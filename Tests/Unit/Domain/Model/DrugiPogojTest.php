<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  			Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  			Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  			Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_A3Ekt_Domain_Model_DrugiPogoj.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage EKT
 *
 * @author Bogdan Štelcar <bstelcar@aeris3.si>
 * @author Jernej Zorec <jzorec@aeris3.si>
 * @author Robert Ferencek <rferencek@aeris3.si>
 * @author Gregor Kirbiš <gkirbis@aeris3.si>
 */
class Tx_A3Ekt_Domain_Model_DrugiPogojTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_A3Ekt_Domain_Model_DrugiPogoj
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_A3Ekt_Domain_Model_DrugiPogoj();
	}

	public function tearDown() {
		unset($this->fixture);
	}

	/**
	 * @test
	 */
	public function getNazivReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setNazivForStringSetsNaziv() { 
		$this->fixture->setNaziv('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getNaziv()
		);
	}
	
	/**
	 * @test
	 */
	public function getOznakaReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setOznakaForStringSetsOznaka() { 
		$this->fixture->setOznaka('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getOznaka()
		);
	}
	
	/**
	 * @test
	 */
	public function getOpisReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setOpisForStringSetsOpis() { 
		$this->fixture->setOpis('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getOpis()
		);
	}
	
	/**
	 * @test
	 */
	public function getDokaziloReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setDokaziloForStringSetsDokazilo() { 
		$this->fixture->setDokazilo('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getDokazilo()
		);
	}
	
	/**
	 * @test
	 */
	public function getOpombaReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setOpombaForStringSetsOpomba() { 
		$this->fixture->setOpomba('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getOpomba()
		);
	}
	
	/**
	 * @test
	 */
	public function getZakonodajniOrganReturnsInitialValueForTx_A3Ekt_Domain_Model_Organ() { }

	/**
	 * @test
	 */
	public function setZakonodajniOrganForTx_A3Ekt_Domain_Model_OrganSetsZakonodajniOrgan() { }
	
	/**
	 * @test
	 */
	public function getZakonodajniOrganKontaktReturnsInitialValueForTx_A3Ekt_Domain_Model_KontaktnaOseba() { }

	/**
	 * @test
	 */
	public function setZakonodajniOrganKontaktForTx_A3Ekt_Domain_Model_KontaktnaOsebaSetsZakonodajniOrganKontakt() { }
	
	/**
	 * @test
	 */
	public function getPravnePodlageReturnsInitialValueForObjectStorageContainingTx_A3Ekt_Domain_Model_PravnaPodlaga() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getPravnePodlage()
		);
	}

	/**
	 * @test
	 */
	public function setPravnePodlageForObjectStorageContainingTx_A3Ekt_Domain_Model_PravnaPodlagaSetsPravnePodlage() { 
		$pravnePodlage = new Tx_A3Ekt_Domain_Model_PravnaPodlaga();
		$objectStorageHoldingExactlyOnePravnePodlage = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOnePravnePodlage->attach($pravnePodlage);
		$this->fixture->setPravnePodlage($objectStorageHoldingExactlyOnePravnePodlage);

		$this->assertSame(
			$objectStorageHoldingExactlyOnePravnePodlage,
			$this->fixture->getPravnePodlage()
		);
	}
	
	/**
	 * @test
	 */
	public function addPravnePodlageToObjectStorageHoldingPravnePodlage() {
		$pravnePodlage = new Tx_A3Ekt_Domain_Model_PravnaPodlaga();
		$objectStorageHoldingExactlyOnePravnePodlage = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOnePravnePodlage->attach($pravnePodlage);
		$this->fixture->addPravnePodlage($pravnePodlage);

		$this->assertEquals(
			$objectStorageHoldingExactlyOnePravnePodlage,
			$this->fixture->getPravnePodlage()
		);
	}

	/**
	 * @test
	 */
	public function removePravnePodlageFromObjectStorageHoldingPravnePodlage() {
		$pravnePodlage = new Tx_A3Ekt_Domain_Model_PravnaPodlaga();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($pravnePodlage);
		$localObjectStorage->detach($pravnePodlage);
		$this->fixture->addPravnePodlage($pravnePodlage);
		$this->fixture->removePravnePodlage($pravnePodlage);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getPravnePodlage()
		);
	}
	
	/**
	 * @test
	 */
	public function getPristojniOrganReturnsInitialValueForTx_A3Ekt_Domain_Model_Organ() { }

	/**
	 * @test
	 */
	public function setPristojniOrganForTx_A3Ekt_Domain_Model_OrganSetsPristojniOrgan() { }
	
	/**
	 * @test
	 */
	public function getPristojniOrganKontaktReturnsInitialValueForTx_A3Ekt_Domain_Model_KontaktnaOseba() { }

	/**
	 * @test
	 */
	public function setPristojniOrganKontaktForTx_A3Ekt_Domain_Model_KontaktnaOsebaSetsPristojniOrganKontakt() { }
	
	/**
	 * @test
	 */
	public function getSorodnePovezaveReturnsInitialValueForObjectStorageContainingTx_A3Ekt_Domain_Model_SorodnePovezave() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getSorodnePovezave()
		);
	}

	/**
	 * @test
	 */
	public function setSorodnePovezaveForObjectStorageContainingTx_A3Ekt_Domain_Model_SorodnePovezaveSetsSorodnePovezave() { 
		$sorodnePovezave = new Tx_A3Ekt_Domain_Model_SorodnePovezave();
		$objectStorageHoldingExactlyOneSorodnePovezave = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneSorodnePovezave->attach($sorodnePovezave);
		$this->fixture->setSorodnePovezave($objectStorageHoldingExactlyOneSorodnePovezave);

		$this->assertSame(
			$objectStorageHoldingExactlyOneSorodnePovezave,
			$this->fixture->getSorodnePovezave()
		);
	}
	
	/**
	 * @test
	 */
	public function addSorodnePovezaveToObjectStorageHoldingSorodnePovezave() {
		$sorodnePovezave = new Tx_A3Ekt_Domain_Model_SorodnePovezave();
		$objectStorageHoldingExactlyOneSorodnePovezave = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneSorodnePovezave->attach($sorodnePovezave);
		$this->fixture->addSorodnePovezave($sorodnePovezave);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneSorodnePovezave,
			$this->fixture->getSorodnePovezave()
		);
	}

	/**
	 * @test
	 */
	public function removeSorodnePovezaveFromObjectStorageHoldingSorodnePovezave() {
		$sorodnePovezave = new Tx_A3Ekt_Domain_Model_SorodnePovezave();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($sorodnePovezave);
		$localObjectStorage->detach($sorodnePovezave);
		$this->fixture->addSorodnePovezave($sorodnePovezave);
		$this->fixture->removeSorodnePovezave($sorodnePovezave);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getSorodnePovezave()
		);
	}
	
}
?>