<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  			Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  			Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  			Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_A3Ekt_Domain_Model_EKTPovezavePogoj.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage EKT
 *
 * @author Bogdan Štelcar <bstelcar@aeris3.si>
 * @author Jernej Zorec <jzorec@aeris3.si>
 * @author Robert Ferencek <rferencek@aeris3.si>
 * @author Gregor Kirbiš <gkirbis@aeris3.si>
 */
class Tx_A3Ekt_Domain_Model_EKTPovezavePogojTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_A3Ekt_Domain_Model_EKTPovezavePogoj
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_A3Ekt_Domain_Model_EKTPovezavePogoj();
	}

	public function tearDown() {
		unset($this->fixture);
	}

	/**
	 * @test
	 */
	public function getOpombaReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setOpombaForStringSetsOpomba() { 
		$this->fixture->setOpomba('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getOpomba()
		);
	}
	
	/**
	 * @test
	 */
	public function getPogojReturnsInitialValueForObjectStorageContainingTx_A3Ekt_Domain_Model_PovezavePogojev() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getPogoj()
		);
	}

	/**
	 * @test
	 */
	public function setPogojForObjectStorageContainingTx_A3Ekt_Domain_Model_PovezavePogojevSetsPogoj() { 
		$pogoj = new Tx_A3Ekt_Domain_Model_PovezavePogojev();
		$objectStorageHoldingExactlyOnePogoj = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOnePogoj->attach($pogoj);
		$this->fixture->setPogoj($objectStorageHoldingExactlyOnePogoj);

		$this->assertSame(
			$objectStorageHoldingExactlyOnePogoj,
			$this->fixture->getPogoj()
		);
	}
	
	/**
	 * @test
	 */
	public function addPogojToObjectStorageHoldingPogoj() {
		$pogoj = new Tx_A3Ekt_Domain_Model_PovezavePogojev();
		$objectStorageHoldingExactlyOnePogoj = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOnePogoj->attach($pogoj);
		$this->fixture->addPogoj($pogoj);

		$this->assertEquals(
			$objectStorageHoldingExactlyOnePogoj,
			$this->fixture->getPogoj()
		);
	}

	/**
	 * @test
	 */
	public function removePogojFromObjectStorageHoldingPogoj() {
		$pogoj = new Tx_A3Ekt_Domain_Model_PovezavePogojev();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($pogoj);
		$localObjectStorage->detach($pogoj);
		$this->fixture->addPogoj($pogoj);
		$this->fixture->removePogoj($pogoj);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getPogoj()
		);
	}
	
	/**
	 * @test
	 */
	public function getDejavnostReturnsInitialValueForObjectStorageContainingTx_A3Ekt_Domain_Model_Dejavnost() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getDejavnost()
		);
	}

	/**
	 * @test
	 */
	public function setDejavnostForObjectStorageContainingTx_A3Ekt_Domain_Model_DejavnostSetsDejavnost() { 
		$dejavnost = new Tx_A3Ekt_Domain_Model_Dejavnost();
		$objectStorageHoldingExactlyOneDejavnost = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneDejavnost->attach($dejavnost);
		$this->fixture->setDejavnost($objectStorageHoldingExactlyOneDejavnost);

		$this->assertSame(
			$objectStorageHoldingExactlyOneDejavnost,
			$this->fixture->getDejavnost()
		);
	}
	
	/**
	 * @test
	 */
	public function addDejavnostToObjectStorageHoldingDejavnost() {
		$dejavnost = new Tx_A3Ekt_Domain_Model_Dejavnost();
		$objectStorageHoldingExactlyOneDejavnost = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneDejavnost->attach($dejavnost);
		$this->fixture->addDejavnost($dejavnost);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneDejavnost,
			$this->fixture->getDejavnost()
		);
	}

	/**
	 * @test
	 */
	public function removeDejavnostFromObjectStorageHoldingDejavnost() {
		$dejavnost = new Tx_A3Ekt_Domain_Model_Dejavnost();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($dejavnost);
		$localObjectStorage->detach($dejavnost);
		$this->fixture->addDejavnost($dejavnost);
		$this->fixture->removeDejavnost($dejavnost);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getDejavnost()
		);
	}
	
}
?>