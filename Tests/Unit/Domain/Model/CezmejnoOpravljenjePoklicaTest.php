<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  			Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  			Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  			Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_A3Ekt_Domain_Model_CezmejnoOpravljenjePoklica.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage EKT
 *
 * @author Bogdan Štelcar <bstelcar@aeris3.si>
 * @author Jernej Zorec <jzorec@aeris3.si>
 * @author Robert Ferencek <rferencek@aeris3.si>
 * @author Gregor Kirbiš <gkirbis@aeris3.si>
 */
class Tx_A3Ekt_Domain_Model_CezmejnoOpravljenjePoklicaTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_A3Ekt_Domain_Model_CezmejnoOpravljenjePoklica
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_A3Ekt_Domain_Model_CezmejnoOpravljenjePoklica();
	}

	public function tearDown() {
		unset($this->fixture);
	}

	/**
	 * @test
	 */
	public function getNazivReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setNazivForStringSetsNaziv() { 
		$this->fixture->setNaziv('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getNaziv()
		);
	}
	
	/**
	 * @test
	 */
	public function getOznakaReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setOznakaForStringSetsOznaka() { 
		$this->fixture->setOznaka('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getOznaka()
		);
	}
	
	/**
	 * @test
	 */
	public function getPravnoSredstvoReturnsInitialValueForInteger() { 
		$this->assertSame(
			null,
			$this->fixture->getPravnoSredstvo()
		);
	}

	/**
	 * @test
	 */
	public function setPravnoSredstvoForIntegerSetsPravnoSredstvo() { 
		$this->fixture->setPravnoSredstvo(12);

		$this->assertSame(
			12,
			$this->fixture->getPravnoSredstvo()
		);
	}
	
	/**
	 * @test
	 */
	public function getMolkOrganaReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setMolkOrganaForStringSetsMolkOrgana() { 
		$this->fixture->setMolkOrgana('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getMolkOrgana()
		);
	}
	
	/**
	 * @test
	 */
	public function getOpombaReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setOpombaForStringSetsOpomba() { 
		$this->fixture->setOpomba('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getOpomba()
		);
	}
	
	/**
	 * @test
	 */
	public function getPristojniOrganReturnsInitialValueForTx_A3Ekt_Domain_Model_Organ() { }

	/**
	 * @test
	 */
	public function setPristojniOrganForTx_A3Ekt_Domain_Model_OrganSetsPristojniOrgan() { }
	
	/**
	 * @test
	 */
	public function getPristojniOrganKontaktReturnsInitialValueForTx_A3Ekt_Domain_Model_KontaktnaOseba() { }

	/**
	 * @test
	 */
	public function setPristojniOrganKontaktForTx_A3Ekt_Domain_Model_KontaktnaOsebaSetsPristojniOrganKontakt() { }
	
	/**
	 * @test
	 */
	public function getVlogeReturnsInitialValueForObjectStorageContainingTx_A3Ekt_Domain_Model_Vloga() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getVloge()
		);
	}

	/**
	 * @test
	 */
	public function setVlogeForObjectStorageContainingTx_A3Ekt_Domain_Model_VlogaSetsVloge() { 
		$vloge = new Tx_A3Ekt_Domain_Model_Vloga();
		$objectStorageHoldingExactlyOneVloge = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneVloge->attach($vloge);
		$this->fixture->setVloge($objectStorageHoldingExactlyOneVloge);

		$this->assertSame(
			$objectStorageHoldingExactlyOneVloge,
			$this->fixture->getVloge()
		);
	}
	
	/**
	 * @test
	 */
	public function addVlogeToObjectStorageHoldingVloge() {
		$vloge = new Tx_A3Ekt_Domain_Model_Vloga();
		$objectStorageHoldingExactlyOneVloge = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneVloge->attach($vloge);
		$this->fixture->addVloge($vloge);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneVloge,
			$this->fixture->getVloge()
		);
	}

	/**
	 * @test
	 */
	public function removeVlogeFromObjectStorageHoldingVloge() {
		$vloge = new Tx_A3Ekt_Domain_Model_Vloga();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($vloge);
		$localObjectStorage->detach($vloge);
		$this->fixture->addVloge($vloge);
		$this->fixture->removeVloge($vloge);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getVloge()
		);
	}
	
	/**
	 * @test
	 */
	public function getPrilogeKVlogiReturnsInitialValueForObjectStorageContainingTx_A3Ekt_Domain_Model_VlogaPriloga() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getPrilogeKVlogi()
		);
	}

	/**
	 * @test
	 */
	public function setPrilogeKVlogiForObjectStorageContainingTx_A3Ekt_Domain_Model_VlogaPrilogaSetsPrilogeKVlogi() { 
		$prilogeKVlogi = new Tx_A3Ekt_Domain_Model_VlogaPriloga();
		$objectStorageHoldingExactlyOnePrilogeKVlogi = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOnePrilogeKVlogi->attach($prilogeKVlogi);
		$this->fixture->setPrilogeKVlogi($objectStorageHoldingExactlyOnePrilogeKVlogi);

		$this->assertSame(
			$objectStorageHoldingExactlyOnePrilogeKVlogi,
			$this->fixture->getPrilogeKVlogi()
		);
	}
	
	/**
	 * @test
	 */
	public function addPrilogeKVlogiToObjectStorageHoldingPrilogeKVlogi() {
		$prilogeKVlogi = new Tx_A3Ekt_Domain_Model_VlogaPriloga();
		$objectStorageHoldingExactlyOnePrilogeKVlogi = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOnePrilogeKVlogi->attach($prilogeKVlogi);
		$this->fixture->addPrilogeKVlogi($prilogeKVlogi);

		$this->assertEquals(
			$objectStorageHoldingExactlyOnePrilogeKVlogi,
			$this->fixture->getPrilogeKVlogi()
		);
	}

	/**
	 * @test
	 */
	public function removePrilogeKVlogiFromObjectStorageHoldingPrilogeKVlogi() {
		$prilogeKVlogi = new Tx_A3Ekt_Domain_Model_VlogaPriloga();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($prilogeKVlogi);
		$localObjectStorage->detach($prilogeKVlogi);
		$this->fixture->addPrilogeKVlogi($prilogeKVlogi);
		$this->fixture->removePrilogeKVlogi($prilogeKVlogi);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getPrilogeKVlogi()
		);
	}
	
	/**
	 * @test
	 */
	public function getStroskiPostopkaReturnsInitialValueForObjectStorageContainingTx_A3Ekt_Domain_Model_StroskiPostopka() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getStroskiPostopka()
		);
	}

	/**
	 * @test
	 */
	public function setStroskiPostopkaForObjectStorageContainingTx_A3Ekt_Domain_Model_StroskiPostopkaSetsStroskiPostopka() { 
		$stroskiPostopka = new Tx_A3Ekt_Domain_Model_StroskiPostopka();
		$objectStorageHoldingExactlyOneStroskiPostopka = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneStroskiPostopka->attach($stroskiPostopka);
		$this->fixture->setStroskiPostopka($objectStorageHoldingExactlyOneStroskiPostopka);

		$this->assertSame(
			$objectStorageHoldingExactlyOneStroskiPostopka,
			$this->fixture->getStroskiPostopka()
		);
	}
	
	/**
	 * @test
	 */
	public function addStroskiPostopkaToObjectStorageHoldingStroskiPostopka() {
		$stroskiPostopka = new Tx_A3Ekt_Domain_Model_StroskiPostopka();
		$objectStorageHoldingExactlyOneStroskiPostopka = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneStroskiPostopka->attach($stroskiPostopka);
		$this->fixture->addStroskiPostopka($stroskiPostopka);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneStroskiPostopka,
			$this->fixture->getStroskiPostopka()
		);
	}

	/**
	 * @test
	 */
	public function removeStroskiPostopkaFromObjectStorageHoldingStroskiPostopka() {
		$stroskiPostopka = new Tx_A3Ekt_Domain_Model_StroskiPostopka();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($stroskiPostopka);
		$localObjectStorage->detach($stroskiPostopka);
		$this->fixture->addStroskiPostopka($stroskiPostopka);
		$this->fixture->removeStroskiPostopka($stroskiPostopka);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getStroskiPostopka()
		);
	}
	
	/**
	 * @test
	 */
	public function getKorakiPostopkaReturnsInitialValueForObjectStorageContainingTx_A3Ekt_Domain_Model_KorakiPostopka() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getKorakiPostopka()
		);
	}

	/**
	 * @test
	 */
	public function setKorakiPostopkaForObjectStorageContainingTx_A3Ekt_Domain_Model_KorakiPostopkaSetsKorakiPostopka() { 
		$korakiPostopka = new Tx_A3Ekt_Domain_Model_KorakiPostopka();
		$objectStorageHoldingExactlyOneKorakiPostopka = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneKorakiPostopka->attach($korakiPostopka);
		$this->fixture->setKorakiPostopka($objectStorageHoldingExactlyOneKorakiPostopka);

		$this->assertSame(
			$objectStorageHoldingExactlyOneKorakiPostopka,
			$this->fixture->getKorakiPostopka()
		);
	}
	
	/**
	 * @test
	 */
	public function addKorakiPostopkaToObjectStorageHoldingKorakiPostopka() {
		$korakiPostopka = new Tx_A3Ekt_Domain_Model_KorakiPostopka();
		$objectStorageHoldingExactlyOneKorakiPostopka = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneKorakiPostopka->attach($korakiPostopka);
		$this->fixture->addKorakiPostopka($korakiPostopka);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneKorakiPostopka,
			$this->fixture->getKorakiPostopka()
		);
	}

	/**
	 * @test
	 */
	public function removeKorakiPostopkaFromObjectStorageHoldingKorakiPostopka() {
		$korakiPostopka = new Tx_A3Ekt_Domain_Model_KorakiPostopka();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($korakiPostopka);
		$localObjectStorage->detach($korakiPostopka);
		$this->fixture->addKorakiPostopka($korakiPostopka);
		$this->fixture->removeKorakiPostopka($korakiPostopka);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getKorakiPostopka()
		);
	}
	
	/**
	 * @test
	 */
	public function getPritozbeniOrganReturnsInitialValueForTx_A3Ekt_Domain_Model_Organ() { }

	/**
	 * @test
	 */
	public function setPritozbeniOrganForTx_A3Ekt_Domain_Model_OrganSetsPritozbeniOrgan() { }
	
	/**
	 * @test
	 */
	public function getPravnaPodlagaReturnsInitialValueForObjectStorageContainingTx_A3Ekt_Domain_Model_PravnaPodlaga() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getPravnaPodlaga()
		);
	}

	/**
	 * @test
	 */
	public function setPravnaPodlagaForObjectStorageContainingTx_A3Ekt_Domain_Model_PravnaPodlagaSetsPravnaPodlaga() { 
		$pravnaPodlaga = new Tx_A3Ekt_Domain_Model_PravnaPodlaga();
		$objectStorageHoldingExactlyOnePravnaPodlaga = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOnePravnaPodlaga->attach($pravnaPodlaga);
		$this->fixture->setPravnaPodlaga($objectStorageHoldingExactlyOnePravnaPodlaga);

		$this->assertSame(
			$objectStorageHoldingExactlyOnePravnaPodlaga,
			$this->fixture->getPravnaPodlaga()
		);
	}
	
	/**
	 * @test
	 */
	public function addPravnaPodlagaToObjectStorageHoldingPravnaPodlaga() {
		$pravnaPodlaga = new Tx_A3Ekt_Domain_Model_PravnaPodlaga();
		$objectStorageHoldingExactlyOnePravnaPodlaga = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOnePravnaPodlaga->attach($pravnaPodlaga);
		$this->fixture->addPravnaPodlaga($pravnaPodlaga);

		$this->assertEquals(
			$objectStorageHoldingExactlyOnePravnaPodlaga,
			$this->fixture->getPravnaPodlaga()
		);
	}

	/**
	 * @test
	 */
	public function removePravnaPodlagaFromObjectStorageHoldingPravnaPodlaga() {
		$pravnaPodlaga = new Tx_A3Ekt_Domain_Model_PravnaPodlaga();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($pravnaPodlaga);
		$localObjectStorage->detach($pravnaPodlaga);
		$this->fixture->addPravnaPodlaga($pravnaPodlaga);
		$this->fixture->removePravnaPodlaga($pravnaPodlaga);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getPravnaPodlaga()
		);
	}
	
}
?>