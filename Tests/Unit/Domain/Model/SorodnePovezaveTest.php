<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  			Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  			Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  			Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_A3Ekt_Domain_Model_SorodnePovezave.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage EKT
 *
 * @author Bogdan Štelcar <bstelcar@aeris3.si>
 * @author Jernej Zorec <jzorec@aeris3.si>
 * @author Robert Ferencek <rferencek@aeris3.si>
 * @author Gregor Kirbiš <gkirbis@aeris3.si>
 */
class Tx_A3Ekt_Domain_Model_SorodnePovezaveTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_A3Ekt_Domain_Model_SorodnePovezave
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_A3Ekt_Domain_Model_SorodnePovezave();
	}

	public function tearDown() {
		unset($this->fixture);
	}

	/**
	 * @test
	 */
	public function getNazivReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setNazivForStringSetsNaziv() { 
		$this->fixture->setNaziv('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getNaziv()
		);
	}
	
	/**
	 * @test
	 */
	public function getUrlReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setUrlForStringSetsUrl() { 
		$this->fixture->setUrl('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getUrl()
		);
	}
	
}
?>