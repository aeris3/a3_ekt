<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  			Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  			Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  			Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_A3Ekt_Domain_Model_PovezavePoklicev.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage EKT
 *
 * @author Bogdan Štelcar <bstelcar@aeris3.si>
 * @author Jernej Zorec <jzorec@aeris3.si>
 * @author Robert Ferencek <rferencek@aeris3.si>
 * @author Gregor Kirbiš <gkirbis@aeris3.si>
 */
class Tx_A3Ekt_Domain_Model_PovezavePoklicevTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_A3Ekt_Domain_Model_PovezavePoklicev
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_A3Ekt_Domain_Model_PovezavePoklicev();
	}

	public function tearDown() {
		unset($this->fixture);
	}

	/**
	 * @test
	 */
	public function getTipReturnsInitialValueForInteger() { 
		$this->assertSame(
			null,
			$this->fixture->getTip()
		);
	}

	/**
	 * @test
	 */
	public function setTipForIntegerSetsTip() { 
		$this->fixture->setTip(12);

		$this->assertSame(
			12,
			$this->fixture->getTip()
		);
	}
	
	/**
	 * @test
	 */
	public function getDovoljenjeReturnsInitialValueForTx_A3Ekt_Domain_Model_Dovoljenje() { }

	/**
	 * @test
	 */
	public function setDovoljenjeForTx_A3Ekt_Domain_Model_DovoljenjeSetsDovoljenje() { }
	
	/**
	 * @test
	 */
	public function getCezmejnoDovoljenjeReturnsInitialValueForTx_A3Ekt_Domain_Model_CezmejnoDovoljenje() { }

	/**
	 * @test
	 */
	public function setCezmejnoDovoljenjeForTx_A3Ekt_Domain_Model_CezmejnoDovoljenjeSetsCezmejnoDovoljenje() { }
	
	/**
	 * @test
	 */
	public function getDrugiPogojReturnsInitialValueForTx_A3Ekt_Domain_Model_DrugiPogoj() { }

	/**
	 * @test
	 */
	public function setDrugiPogojForTx_A3Ekt_Domain_Model_DrugiPogojSetsDrugiPogoj() { }
	
	/**
	 * @test
	 */
	public function getPogojReturnsInitialValueForObjectStorageContainingTx_A3Ekt_Domain_Model_PovezavePoklicev() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getPogoj()
		);
	}

	/**
	 * @test
	 */
	public function setPogojForObjectStorageContainingTx_A3Ekt_Domain_Model_PovezavePoklicevSetsPogoj() { 
		$pogoj = new Tx_A3Ekt_Domain_Model_PovezavePoklicev();
		$objectStorageHoldingExactlyOnePogoj = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOnePogoj->attach($pogoj);
		$this->fixture->setPogoj($objectStorageHoldingExactlyOnePogoj);

		$this->assertSame(
			$objectStorageHoldingExactlyOnePogoj,
			$this->fixture->getPogoj()
		);
	}
	
	/**
	 * @test
	 */
	public function addPogojToObjectStorageHoldingPogoj() {
		$pogoj = new Tx_A3Ekt_Domain_Model_PovezavePoklicev();
		$objectStorageHoldingExactlyOnePogoj = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOnePogoj->attach($pogoj);
		$this->fixture->addPogoj($pogoj);

		$this->assertEquals(
			$objectStorageHoldingExactlyOnePogoj,
			$this->fixture->getPogoj()
		);
	}

	/**
	 * @test
	 */
	public function removePogojFromObjectStorageHoldingPogoj() {
		$pogoj = new Tx_A3Ekt_Domain_Model_PovezavePoklicev();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($pogoj);
		$localObjectStorage->detach($pogoj);
		$this->fixture->addPogoj($pogoj);
		$this->fixture->removePogoj($pogoj);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getPogoj()
		);
	}
	
	/**
	 * @test
	 */
	public function getDovoljenjeVTujiniReturnsInitialValueForTx_A3Ekt_Domain_Model_DovoljenjeVTujini() { }

	/**
	 * @test
	 */
	public function setDovoljenjeVTujiniForTx_A3Ekt_Domain_Model_DovoljenjeVTujiniSetsDovoljenjeVTujini() { }
	
	/**
	 * @test
	 */
	public function getPoklicReturnsInitialValueForTx_A3Ekt_Domain_Model_Poklic() { }

	/**
	 * @test
	 */
	public function setPoklicForTx_A3Ekt_Domain_Model_PoklicSetsPoklic() { }
	
}
?>