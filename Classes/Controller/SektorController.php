<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Controller_SektorController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * sektorRepository
	 *
	 * @var Tx_A3Ekt_Domain_Repository_SektorRepository
	 */
	protected $sektorRepository;

	/**
	 * injectSektorRepository
	 *
	 * @param Tx_A3Ekt_Domain_Repository_SektorRepository $sektorRepository
	 * @return void
	 */
	public function injectSektorRepository(Tx_A3Ekt_Domain_Repository_SektorRepository $sektorRepository) {
		$this->sektorRepository = $sektorRepository;
	}
    
    private function getDovoljenjaForSektor($sektorId, $limit=5) {
		
		
		// if cezmejnoDelovanje and sektor OR only sektor
		$sektorDejavnostiRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTDejavnostRepository');
	    $sektorDejavnostiQuery = $sektorDejavnostiRepository->createQuery();
	    $sektorDejavnosti = $sektorDejavnostiQuery->matching($sektorDejavnostiQuery->equals("sektor_dejavnosti", $sektorId))->execute();
		
	    $EKTDejavnostiId=array();
	    foreach($sektorDejavnosti as $sektorDejavnost) {
            $EKTDejavnostiId[] = $sektorDejavnost->getUid();
	    }

			// ni obrodilo sadov
		if ( count($EKTDejavnostiId) == 0 )
			return null;
			
		$dejavnostRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_DejavnostRepository');
	    $dejavnostQuery = $dejavnostRepository->createQuery();
			
	    $dejavnosti = $dejavnostQuery->matching($dejavnostQuery->in("dejavnost_po_e_k_t", $EKTDejavnostiId))->execute();
			

        $dejavnostiId=array();
        foreach($dejavnosti as $dejavnost) {
            $dejavnostiId[] = $dejavnost->getUid();
        }
		
		// ni obrodilo sadov
		if ( count($dejavnostiId) == 0 )
			return null;
		
        $EKTPovezavePogojRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTPovezavePogojRepository');
        $EKTPovezavePogojRepositoryQuery = $EKTPovezavePogojRepository->createQuery();
		$EKTPovezavePogoj = $EKTPovezavePogojRepositoryQuery->matching(
			$EKTPovezavePogojRepositoryQuery->logicalAnd(
				$EKTPovezavePogojRepositoryQuery->in("dejavnost", $dejavnostiId)
			)
		)->execute();
		
		foreach($EKTPovezavePogoj as $povezavaPogoja) {
			$nekej = $povezavaPogoja->getPogoj();
			foreach($nekej as $nekej2) {
				$pogojiId[] = $nekej2->getUid();
			}
        }

		// ni obrodilo sadov
		if ( count($pogojiId) == 0 )
			return null;
		
		$povezavePogojevRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository');
        $povezavePogojevRepositoryQuery = $povezavePogojevRepository->createQuery();
		$povezavePogojev = $povezavePogojevRepositoryQuery->matching(
			$povezavePogojevRepositoryQuery->logicalAnd(
				$povezavePogojevRepositoryQuery->in("uid", $pogojiId),
				$povezavePogojevRepositoryQuery->equals("tip", 2)
			)
		)->execute();
		
		foreach($povezavePogojev as $povezavaPogoja) {
				$tmp_dovoljenje = $povezavaPogoja->getDovoljenje();
				if ( $tmp_dovoljenje != null )
					$dovoljenjaId[] = $tmp_dovoljenje->getUid();
        }
		
		$dovoljenjaId = array_unique($dovoljenjaId);
		
		// ni obrodilo sadov
		if ( count($dovoljenjaId) == 0 )
			return null;
        
        $dovoljenjeRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_DovoljenjeRepository');
		
		$dovoljenjeQuery = $dovoljenjeRepository->createQuery();
		//$dovoljenjeQuery->setLimit($limit);
        
		// if only sektor OR sektor and cezmejnoDelovanje
		$dovoljenja = $dovoljenjeQuery->matching($dovoljenjeQuery->in("uid", $dovoljenjaId))->execute();
		
		return $dovoljenja;
	}

    
    private function getPokliciForSektor($sektorId, $limit=5) {
		
		
		$sektorDejavnostiRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTDejavnostRepository');
        $sektorDejavnostiQuery = $sektorDejavnostiRepository->createQuery();
        $sektorDejavnosti = $sektorDejavnostiQuery->matching($sektorDejavnostiQuery->equals("sektor_dejavnosti", $sektorId))->execute();
		
		$EKTDejavnostiId=array();
        foreach($sektorDejavnosti as $sektorDejavnost) {
            $EKTDejavnostiId[] = $sektorDejavnost->getUid();
        }
        
        $dejavnostRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_DejavnostRepository');
        $dejavnostQuery = $dejavnostRepository->createQuery();
        $dejavnosti = $dejavnostQuery->matching($dejavnostQuery->in("dejavnost_po_e_k_t", $EKTDejavnostiId))->execute();

        $dejavnostiId=array();
        foreach($dejavnosti as $dejavnost) {
            $dejavnostiId[] = $dejavnost->getUid();
        }

		// ni obrodilo sadov
		if ( count($dejavnostiId) == 0 )
			return null;

        $EKTPovezavePogojRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTPovezavePogojRepository');
        $EKTPovezavePogojRepositoryQuery = $EKTPovezavePogojRepository->createQuery();
        //$povezavePogojev = $povezavePogojevRepositoryQuery->matching($povezavePogojevRepositoryQuery->in("poklic", $dejavnostiId))->execute();
		$EKTPovezavePogoj = $EKTPovezavePogojRepositoryQuery->matching(
			$EKTPovezavePogojRepositoryQuery->logicalAnd(
				$EKTPovezavePogojRepositoryQuery->in("dejavnost", $dejavnostiId)
			)
		)->execute();
		
		foreach($EKTPovezavePogoj as $povezavaPogoja) {
			$nekej = $povezavaPogoja->getPogoj();
			foreach($nekej as $nekej2) {
				$pogojiId[] = $nekej2->getUid();
			}
        }

		// ni obrodilo sadov
		if ( count($pogojiId) == 0 )
			return null;
		
		$povezavePogojevRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository');
        $povezavePogojevRepositoryQuery = $povezavePogojevRepository->createQuery();
		$povezavePogojev = $povezavePogojevRepositoryQuery->matching(
			$povezavePogojevRepositoryQuery->logicalAnd(
				$povezavePogojevRepositoryQuery->in("uid", $pogojiId),
				$povezavePogojevRepositoryQuery->equals("tip", 1)
			)
		)->execute();
		
		foreach($povezavePogojev as $povezavaPogoja) {
				$tmp_poklic = $povezavaPogoja->getPoklic();
				if ( $tmp_poklic != null )
					$pokliciId[] = $tmp_poklic->getUid();
        }
		
		$pokliciId = array_unique($pokliciId);
        
        // ni obrodilo sadov
		if ( count($pokliciId) == 0 )
			return null;
        
        $povezavePoklicevRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTPovezavePoklicevRepository');
        
		$povezavePoklicevQuery = $povezavePoklicevRepository->createQuery();
		$povezavePoklicev = $povezavePoklicevQuery->matching($povezavePoklicevQuery->in("uid", $pokliciId))->execute();
        
        foreach($povezavePoklicev as $povezavaPoklicev) {
            $poklici = $povezavaPoklicev->getPoklic();
            foreach($poklici as $poklic) {
                $pokliciId2[] = $poklic->getUid();
            }
        }
		
        $poklicRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PoklicRepository');
        
		$poklicQuery = $poklicRepository->createQuery();
        //$poklicQuery->setLimit($limit);
		
		$poklici = $poklicQuery->matching($poklicQuery->in("uid", $pokliciId2))->execute();
			
		return $poklici;
	}


	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$sektors = $this->sektorRepository->findAll();
		
		$firstHalfWithDejavnosti = null;
		$secondHalfWithDejavnosti = null;
		$firstHalfWithoutDejavnosti = null;
		$secondHalfWithoutDejavnosti = null;
		
		// getting ekt dejavnosti for sektors
		$i = 0;
		$j = 0;
		foreach ($sektors as $sektor) {
			
			$EKTDejavnostRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTDejavnostRepository');
			$DejavnostRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_DejavnostRepository');
			
			$query = $EKTDejavnostRepository->createQuery();
			$EKTDejavnostData = $query->matching(
				$query->equals('sektor_dejavnosti', $sektor)
			)->execute();
			
			$k = 0;
			$vseDejavnosti = array();
			foreach ($EKTDejavnostData as $ektDejavnost) {
				$query = $DejavnostRepository->createQuery();
				$DejavnostData = $query->matching(
					$query->equals('dejavnost_po_e_k_t', $ektDejavnost)
				)->execute();
				if ( count($DejavnostData[0]) )
					$vseDejavnosti[] = $DejavnostData[0];
				$k++;
			}
			
			$sektor->dejavnosti = $vseDejavnosti;
			$sektor->steviloDejavnosti = count($vseDejavnosti);
			
			if ( count($EKTDejavnostData) == 0 ) {
				$i++;
				
				if ( $i % 2 != 0 )
					$firstHalfWithoutDejavnosti[] = $sektor;
				else
					$secondHalfWithoutDejavnosti[] = $sektor;
			} else if ( count($EKTDejavnostData) != 0 ) {
				$j++;
				
				if ( $j % 2 != 0 )
					$firstHalfWithDejavnosti[] = $sektor;
				else
					$secondHalfWithDejavnosti[] = $sektor;  
			}
		}
		
		//$this->view->assign('sektors', $sektors);
		$this->view->assign('firstHalfWithoutDejavnosti', $firstHalfWithoutDejavnosti);
		$this->view->assign('secondHalfWithoutDejavnosti', $secondHalfWithoutDejavnosti);
		$this->view->assign('firstHalfWithDejavnosti', $firstHalfWithDejavnosti);
		$this->view->assign('secondHalfWithDejavnosti', $secondHalfWithDejavnosti);
	}

	/**
	 * action show
	 *
	 * @param $sektor
	 * @return void
	 */
	public function showAction(Tx_A3Ekt_Domain_Model_Sektor $sektor) {
        
        $limit=5;
        
        $EKTDejavnostRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTDejavnostRepository');
        $EKTDejavnostQuery = $EKTDejavnostRepository->createQuery();
        $EKTDejavnosti = $EKTDejavnostQuery->matching($EKTDejavnostQuery->equals("sektor_dejavnosti", $sektor))->execute();
        
        $EKTDejavnostId=array();
        foreach($EKTDejavnosti as $EKTDejavnost) {
            $EKTDejavnostId[] = $EKTDejavnost->getUid();
        }
        
        $dejavnostRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_DejavnostRepository');
        $dejavnostQuery = $dejavnostRepository->createQuery();
        //$dejavnostQuery->setLimit($limit);
        $dejavnosti = $dejavnostQuery->matching($dejavnostQuery->in("dejavnost_po_e_k_t", $EKTDejavnostId))->execute();
        
        $stSlik=0;
        $stDejavnosti=0;
        $stPoklicev=0;
        $stDovoljenj=0;
        
        foreach($dejavnosti as $dejavnost) {
            $stDejavnosti++;
            
            if($dejavnost->getSlika()!= null && $dejavnost->getSlika()!= "")
                $stSlik++;
        }
        
        $poklici = $this->getPokliciForSektor($sektor, $limit);
        $dovoljenja = $this->getDovoljenjaForSektor($sektor, $limit);
        
        foreach($poklici as $poklic) {
            $stPoklicev++;
        }
        
        foreach($dovoljenja as $dovoljenje) {
            $stDovoljenj++;
        }
        
        $this->view->assign('stDejavnosti', $stDejavnosti);
        $this->view->assign('stPoklicev', $stPoklicev);
        $this->view->assign('stDovoljenj', $stDovoljenj);
        
        $this->view->assign('stSlik', $stSlik);
        $this->view->assign('limit', $limit);
        $this->view->assign('dovoljenja', $dovoljenja);
        $this->view->assign('poklici', $poklici);
        $this->view->assign('dejavnosti', $dejavnosti);
		$this->view->assign('sektor', $sektor);
	}

}
?>