<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Controller_EKTDejavnostController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * eKTDejavnostRepository
	 *
	 * @var Tx_A3Ekt_Domain_Repository_EKTDejavnostRepository
	 */
	protected $eKTDejavnostRepository;

	/**
	 * injectEKTDejavnostRepository
	 *
	 * @param Tx_A3Ekt_Domain_Repository_EKTDejavnostRepository $eKTDejavnostRepository
	 * @return void
	 */
	public function injectEKTDejavnostRepository(Tx_A3Ekt_Domain_Repository_EKTDejavnostRepository $eKTDejavnostRepository) {
		$this->eKTDejavnostRepository = $eKTDejavnostRepository;
	}

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$eKTDejavnosts = $this->eKTDejavnostRepository->findAll();
		$this->view->assign('eKTDejavnosts', $eKTDejavnosts);
	}

	/**
	 * action show
	 *
	 * @param $eKTDejavnost
	 * @return void
	 */
	public function showAction(Tx_A3Ekt_Domain_Model_EKTDejavnost $eKTDejavnost) {
		$this->view->assign('nekej', array('kekec in mojca'));
		$this->view->assign('ts', $this->configurationManager->getConfiguration(Tx_Extbase_Configuration_ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK));
		$this->view->assign('eKTDejavnost', $eKTDejavnost);
	}

}
?>