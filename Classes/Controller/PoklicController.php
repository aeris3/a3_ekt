<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Controller_PoklicController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * poklicRepository
	 *
	 * @var Tx_A3Ekt_Domain_Repository_PoklicRepository
	 */
	protected $poklicRepository;
    
    /**
	 * filter
	 *
	 * @var $filter
	 */
    private $filter;

	/**
	 * injectPoklicRepository
	 *
	 * @param Tx_A3Ekt_Domain_Repository_PoklicRepository $poklicRepository
	 * @return void
	 */
	public function injectPoklicRepository(Tx_A3Ekt_Domain_Repository_PoklicRepository $poklicRepository) {
		$this->poklicRepository = $poklicRepository;
	}
		
	private function getPokliciForFilter($cezmejnoDelovanjeId, $sektorId) {
		
		// only cezmejnoDelovanje
		if ( $sektorId == -1 || $sektorId == null ) {
			$query = $this->poklicRepository->createQuery();
			$poklici = $query->matching($query->equals("cezmejno_priznavanje_poklica", $cezmejnoDelovanjeId))->execute();
			return $poklici;
		}
		
		// if cezmejnoDelovanje and sektor OR only sektor
		$sektorDejavnostiRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTDejavnostRepository');
        $sektorDejavnostiQuery = $sektorDejavnostiRepository->createQuery();
        $sektorDejavnosti = $sektorDejavnostiQuery->matching($sektorDejavnostiQuery->equals("sektor_dejavnosti", $sektorId))->execute();
		
		$EKTDejavnostiId=array();
        foreach($sektorDejavnosti as $sektorDejavnost) {
            $EKTDejavnostiId[] = $sektorDejavnost->getUid();
        }
        
        $dejavnostRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_DejavnostRepository');
        $dejavnostQuery = $dejavnostRepository->createQuery();
        $dejavnosti = $dejavnostQuery->matching($dejavnostQuery->in("dejavnost_po_e_k_t", $EKTDejavnostiId))->execute();

        $dejavnostiId=array();
        foreach($dejavnosti as $dejavnost) {
            $dejavnostiId[] = $dejavnost->getUid();
        }

		// ni obrodilo sadov
		if ( count($dejavnostiId) == 0 )
			return null;

        $EKTPovezavePogojRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTPovezavePogojRepository');
        $EKTPovezavePogojRepositoryQuery = $EKTPovezavePogojRepository->createQuery();
        //$povezavePogojev = $povezavePogojevRepositoryQuery->matching($povezavePogojevRepositoryQuery->in("poklic", $dejavnostiId))->execute();
		$EKTPovezavePogoj = $EKTPovezavePogojRepositoryQuery->matching(
			$EKTPovezavePogojRepositoryQuery->logicalAnd(
				$EKTPovezavePogojRepositoryQuery->in("dejavnost", $dejavnostiId)
			)
		)->execute();
		
		foreach($EKTPovezavePogoj as $povezavaPogoja) {
			$nekej = $povezavaPogoja->getPogoj();
			foreach($nekej as $nekej2) {
				$pogojiId[] = $nekej2->getUid();
			}
        }

		// ni obrodilo sadov
		if ( count($pogojiId) == 0 )
			return null;
		
		$povezavePogojevRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository');
        $povezavePogojevRepositoryQuery = $povezavePogojevRepository->createQuery();
        //$povezavePogojev = $povezavePogojevRepositoryQuery->matching($povezavePogojevRepositoryQuery->in("poklic", $dejavnostiId))->execute();
		$povezavePogojev = $povezavePogojevRepositoryQuery->matching(
			$povezavePogojevRepositoryQuery->logicalAnd(
				$povezavePogojevRepositoryQuery->in("uid", $pogojiId),
				$povezavePogojevRepositoryQuery->equals("tip", 1)
			)
		)->execute();
		
		foreach($povezavePogojev as $povezavaPogoja) {
				$pokliciId[] = $povezavaPogoja->getPoklic()->getUid();
        }
		
		$pokliciId = array_unique($pokliciId);
		
		// ni obrodilo sadov
		if ( count($pokliciId) == 0 )
			return null;
        
        $povezavePoklicevRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTPovezavePoklicevRepository');
        
		$povezavePoklicevQuery = $povezavePoklicevRepository->createQuery();
		$povezavePoklicev = $povezavePoklicevQuery->matching($povezavePoklicevQuery->in("uid", $pokliciId))->execute();
        
        foreach($povezavePoklicev as $povezavaPoklicev) {
            $poklici = $povezavaPoklicev->getPoklic();
            foreach($poklici as $poklic) {
                $pokliciId2[] = $poklic->getUid();
            }
        }
		
		$poklicQuery = $this->poklicRepository->createQuery();
		
		// if only sektor OR sektor and cezmejnoDelovanje
		if ( $cezmejnoDelovanjeId == -1 || $cezmejnoDelovanjeId == null ) {
			$poklici = $poklicQuery->matching($poklicQuery->in("uid", $pokliciId2))->execute();
		} else {
			$poklici = $poklicQuery->matching(
				$poklicQuery->logicalAnd(
					$poklicQuery->in("uid", $pokliciId2),
					$poklicQuery->equals("cezmejno_priznavanje_poklica", $cezmejnoDelovanjeId)
				)
			)->execute();
		}
		
		return $poklici;
	}

	/**
	 * action list
	 *
     * @param int $page page number
	 * @return void
	 */
	public function listAction($page = 1) {
		//$poklics = $this->poklicRepository->findAll();
		//$this->view->assign('poklics', $poklics);
        
        $this->setFilter();
        
        $pageCount = 0;
        $itemsPerPage = 10;
        
        $this->poklicRepository->setDefaultOrderings(array('naziv'=>'ASC'));
        
        $cezmejnoDelovanje = $this->getFilter('cezmejnoDelovanje');
        $sektor = $this->getFilter('sektor');
        
        //if cezmejno dovoljenje
        if($cezmejnoDelovanje!="" && $cezmejnoDelovanje!="-1") {
            
            //if cezmejno dovoljenje and sektor
            if($sektor!="" && $sektor!="-1") {
                
				$poklici = $this->getPokliciForFilter($cezmejnoDelovanje, $sektor);
				$allItems = count($poklici);
				
            // only cezmejno dovoljenje set in filter
            } else {
				$poklici = $this->getPokliciForFilter($cezmejnoDelovanje);
				//$allItems = count($poklici);
            }
            
        } else {
			
            // if only sector in filter is chosen
            if($sektor!="" && $sektor!=-1) {
                
				$poklici = $this->getPokliciForFilter(-1, $sektor);
				//$allItems = count($poklici);
            	
			// if no filters
            } else {
				$query = $this->poklicRepository->createQuery();
                $poklici = $query->execute();
                //$allItems = count($poklici);
            }
            
        }
        
        /*try {
            $pageCount = Tx_ExtbasePager_Utility_Pager::prepareQuery($query, $page, $itemsPerPage);
        } catch(Exception $e) {
            $noResults="Iskanje ni obrodilo sadov!";
        }*/
        
        $data=array();
        foreach($poklici as $poklic) {
            $data[] =  array(
                "uid"   => $poklic->getUid(),
                "naziv" => $poklic->getNaziv(),
            );
        }
        
        //sektorji/področja
        $sektorRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_SektorRepository');
        $sektorji = $sektorRepository->createQuery()->execute();
        
        
        //if($itemsPerPage>$allItems)
            //$itemsPerPage=$allItems;
        
        $this->view->assign('noResults', $noResults);
        $this->view->assign('sektorji', $sektorji);
        $this->view->assign('pokliciFrom', $itemsPerPage);
        //$this->view->assign('pokliciCount', $allItems);
        $this->view->assign('page', $page);
        $this->view->assign('pageCount', $pageCount);
		$this->view->assign('poklici', $data);
        $this->view->assign('cezmejnoDelovanjeChecked', $this->getFilter('cezmejnoDelovanje'));
        $this->view->assign('sektorSelected', $this->getFilter('sektor'));
	}
    
    private function setFilter() {
        //$this->filter = $GLOBALS['TSFE']->fe_user->getKey('ses', 'tx_a3ekt_template');
        
        if($this->request->hasArgument('cezmejnoDelovanje'))
            $this->filter['cezmejnoDelovanje'] = $this->request->getArgument('cezmejnoDelovanje');
        
        if($this->request->hasArgument('sektor'))
            $this->filter['sektor'] = $this->request->getArgument('sektor');
		
        //$GLOBALS['TSFE']->fe_user->setKey('ses', 'tx_a3ekt_template', $this->filter);
        //$GLOBALS['TSFE']->fe_user->storeSessionData();
    }
    
    private function getFilter($argument) {
        return ($this->filter[$argument]=="") ? "-1" : $this->filter[$argument];
    }
    
    private function clearFilter() {
        $GLOBALS['TSFE']->fe_user->setKey('ses', 'tx_a3ekt_template', null);
        $GLOBALS['TSFE']->fe_user->storeSessionData();
    }

	/**
	 * action show
	 *
	 * @param $poklic
	 * @return void
	 */
	public function showAction(Tx_A3Ekt_Domain_Model_Poklic $poklic) {
		$this->view->assign('poklic', $poklic);
		
		if ( $this->request->hasArgument('poklic') ) {
			
			$poklicUid = $this->request->getArgument('poklic');
			
			// getting ektpovezave pogoj from poklic
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EktPovezavePoklicevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->logicalAnd(
					$query->equals('poklic', $poklicUid)
			))->execute();
			
			if ( $data->getFirst() != null ) {
				$this->view->assign('EKTPovezavePoklicev', $data->getFirst());

				$ektpovezavepoklicUid = $data->getFirst()->getUid();
				
				// getting povezave pogojev
				$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository');
				$query = $nekej->createQuery();
				$data = $query->matching(
					$query->logicalAnd(
	//					$query->equals('poklic', $poklic),
						$query->equals('ektpovezavepoklicev', $ektpovezavepoklicUid)
				))->execute();
			
				// getting the povezave pogojev records in correct order
				$query = $nekej->createQuery();
				$pogoji = $query->matching(
					$query->logicalAnd(
						$query->equals('povezavepoklicev', $data[0]->getUid())
				))->setOrderings(array('sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING))->execute();
			
			}
			
		} elseif ( $this->request->hasArgument('povezavePogojev') ) {
			
			$poklicUid = $this->request->getArgument('poklic');

			// getting ektpovezave pogoj from poklic
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->logicalAnd(
					$query->equals('ektpovezavepoklicev', $poklicUid)
			))->execute();

			$ektpovezavepoklicUid = $data->getFirst()->getUid();

			// getting povezave pogojev
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->logicalAnd(
//					$query->equals('poklic', $poklic),
					$query->equals('povezavepoklicev', $ektpovezavepoklicUid)
			))->execute();
			
			// getting the povezave pogojev records in correct order
			$query = $nekej->createQuery();
			$pogoji = $query->matching(
				$query->logicalAnd(
					$query->equals('povezavepogojev', $data[0]->getUid())
			))->setOrderings(array('sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING))->execute();
			
		}

		$this->view->assign('povezavePogojev', $data);
		
		$this->view->assign('pogoji', $pogoji);
		
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('pravne_podlage','tx_a3ekt_domain_model_poklic', 'uid = "'.$poklic->getUid().'"');
		$data = array();
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) $data[] = $row['pravne_podlage'];
		$podlage = explode(',', $data[0]);

		$pravnePodlage = array();
		foreach($poklic->getPravnePodlage() as $pravnaPodlaga) {
			$pravnePodlage[$pravnaPodlaga->getUid()] = $pravnaPodlaga;
		}
		$pravnePodlageSort = array();
		foreach($podlage as $podlaga) {
			$pravnePodlageSort[] = $pravnePodlage[$podlaga];
		}

		$this->view->assign('pravnePodlage', $pravnePodlageSort);
		
		$this->view->assign('povezavepogojevs', $povezavepogojevs);
	}

}
?>