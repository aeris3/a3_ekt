<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Controller_CezmejnoDovoljenjeController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * cezmejnoDovoljenjeRepository
	 *
	 * @var Tx_A3Ekt_Domain_Repository_CezmejnoDovoljenjeRepository
	 */
	protected $cezmejnoDovoljenjeRepository;

	/**
	 * injectCezmejnoDovoljenjeRepository
	 *
	 * @param Tx_A3Ekt_Domain_Repository_CezmejnoDovoljenjeRepository $cezmejnoDovoljenjeRepository
	 * @return void
	 */
	public function injectCezmejnoDovoljenjeRepository(Tx_A3Ekt_Domain_Repository_CezmejnoDovoljenjeRepository $cezmejnoDovoljenjeRepository) {
		$this->cezmejnoDovoljenjeRepository = $cezmejnoDovoljenjeRepository;
	}

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$cezmejnoDovoljenjes = $this->cezmejnoDovoljenjeRepository->findAll();
		$this->view->assign('cezmejnoDovoljenjes', $cezmejnoDovoljenjes);
	}

	/**
	 * action show
	 *
	 * @param $cezmejnoDovoljenje
	 * @return void
	 */
	public function showAction(Tx_A3Ekt_Domain_Model_CezmejnoDovoljenje $cezmejnoDovoljenje) {
		$this->view->assign('cezmejnoDovoljenje', $cezmejnoDovoljenje);
		
		if ( $this->request->hasArgument('poklic') ) {
			$poklicUid = $this->request->getArgument('poklic');
			$this->view->assign('poklic', true);
			
			// getting ektpovezave pogoj from dejavnost
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EktPovezavePoklicevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->logicalAnd(
					$query->equals('poklic', $poklicUid)
			))->execute();
			$ektpovezavepoklicUid = $data->getFirst()->getUid();
			
			// getting povezave pogojev
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->logicalAnd(
#					$query->equals('poklic', $poklicUid),
					$query->equals('tip', 2),
					$query->equals('ektpovezavepoklicev', $ektpovezavepoklicUid)
			))->execute();
			
			// getting the povezave pogojev records in correct order
			$query = $nekej->createQuery();
			$pogoji = $query->matching(
				$query->logicalAnd(
					$query->equals('povezavepoklicev', $data[0]->getUid())
			))->setOrderings(array('sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING))->execute();
			
		} elseif ( $this->request->hasArgument('dejavnost') ) {
			$dejavnostUid = $this->request->getArgument('dejavnost');
			$this->view->assign('dejavnost', true);

			// getting ektpovezave pogoj from dejavnost
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EktPovezavePogojRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->logicalAnd(
					$query->equals('dejavnost', $dejavnostUid)
			))->execute();
			$ektpovezavepogojevUid = $data->getFirst()->getUid();

			// getting povezave pogojev
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->logicalAnd(
#					$query->equals('dejavnost', $dejavnostUid),
					$query->equals('tip', 3),
					$query->equals('ektpovezavepogoj', $ektpovezavepogojevUid)
			))->execute();
			
			// getting the povezave pogojev records in correct order
			$query = $nekej->createQuery();
			
			if ( $data[0] != null ) {
				$pogoji = $query->matching(
					$query->logicalAnd(
						$query->equals('povezavepogojev', $data[0]->getUid())
				))->setOrderings(array('sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING))->execute();	
			}
		}
		
		$this->view->assign('pogoji', $pogoji);
	}

}
?>