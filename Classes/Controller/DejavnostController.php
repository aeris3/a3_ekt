<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Controller_DejavnostController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * dejavnostRepository
	 *
	 * @var Tx_A3Ekt_Domain_Repository_DejavnostRepository
	 */
	protected $dejavnostRepository;
    
    /**
	 * filter
	 *
	 * @var $filter
	 */
    private $filter;
    

	/**
	 * injectDejavnostRepository
	 *
	 * @param Tx_A3Ekt_Domain_Repository_DejavnostRepository $dejavnostRepository
	 * @return void
	 */
	public function injectDejavnostRepository(Tx_A3Ekt_Domain_Repository_DejavnostRepository $dejavnostRepository) {
		$this->dejavnostRepository = $dejavnostRepository;
	}

	/**
	 * action list
	 *
     * @param int $page page number
	 * @return void
	 */
	public function listAction($page=1) {
        
        $this->setFilter();
        
        $pageCount = 0;
        $itemsPerPage = 10;
        
        $this->dejavnostRepository->setDefaultOrderings(array('naziv'=>'ASC'));
        $query = $this->dejavnostRepository->createQuery();
        
        $cezmejnoDelovanje = $this->getFilter('cezmejnoDelovanje');
        $sektor = $this->getFilter('sektor');
        
        //if cezmejno dovoljenje
        if($cezmejnoDelovanje!="" && $cezmejnoDelovanje!="-1") {
            
            //if cezmejno dovoljenje and sektor
            if($sektor!="" && $sektor!="-1") {
                
                $sektorDejavnostiRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTDejavnostRepository');
                $sektorDejavnostiQuery = $sektorDejavnostiRepository->createQuery();
                $sektorDejavnosti = $sektorDejavnostiQuery->matching($sektorDejavnostiQuery->equals("sektor_dejavnosti", $sektor))->execute();
                
                $sektorDejavnostiId=array();
                foreach($sektorDejavnosti as $sektorDejavnost) {
                    $sektorDejavnostiId[] = $sektorDejavnost->getUid();
                }
                
                $dejavnosti = $query->matching(
                        $query->logicalAnd(
                                $query->equals("cezmejno_opravljanje", $this->getFilter('cezmejnoDelovanje')),
                                $query->in("dejavnost_po_e_k_t", $sektorDejavnostiId)
                        )
                )->execute();
                
                /*$allItems = $query->matching(
                        $query->logicalAnd(
                                $query->equals("cezmejno_opravljanje", $this->getFilter('cezmejnoDelovanje')),
                                $query->in("dejavnost_po_e_k_t", $sektorDejavnostiId)
                        )
                )->execute()->count();*/
            
            //only področje    
            } else { 
                $dejavnosti = $query->matching($query->equals("cezmejno_opravljanje", $this->getFilter('cezmejnoDelovanje')))->execute();
                //$allItems = $this->dejavnostRepository->createQuery()->matching($query->equals("cezmejno_opravljanje", $this->getFilter('cezmejnoDelovanje')))->execute()->count();
            }
            
        } else {
            
            //if sector
            if($sektor!="" && $sektor!=-1) {
                
                $sektorDejavnostiRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTDejavnostRepository');
                $sektorDejavnostiQuery = $sektorDejavnostiRepository->createQuery();
                $sektorDejavnosti = $sektorDejavnostiQuery->matching($sektorDejavnostiQuery->equals("sektor_dejavnosti", $sektor))->execute();
                
                $sektorDejavnostiId=array();
                foreach($sektorDejavnosti as $sektorDejavnost) {
                    $sektorDejavnostiId[] = $sektorDejavnost->getUid();
                }
                
                $dejavnosti = $query->matching(
                    $query->in("dejavnost_po_e_k_t", $sektorDejavnostiId)
                )->execute();
                
                /*$allItems = $query->matching(
                    $query->in("dejavnost_po_e_k_t", $sektorDejavnostiId)
                )->execute()->count();*/
            
            //no filters
            } else {
                $dejavnosti = $query->execute();
                //$allItems = $dejavnosti->count();
            }
            
        }
        
        /*try {
            $pageCount = Tx_ExtbasePager_Utility_Pager::prepareQuery($query, $page, $itemsPerPage);
        } catch(Exception $e) {
            $noResults="Iskanje ni obrodilo sadov!";
        }*/
        
        $data=array();
        foreach($dejavnosti as $dejavnost) {
            $data[] =  array(
            	"uid"   => $dejavnost->getUid(),
            	"naziv" => $dejavnost->getNaziv(),
        	);
        }
        
        //sektorji/področja
        $sektorRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_SektorRepository');
        $sektorji = $sektorRepository->createQuery()->execute();
        
        /*if($itemsPerPage>$allItems)
            $itemsPerPage=$allItems;
        */
        
        $this->view->assign('noResults', $noResults);
        $this->view->assign('sektorji', $sektorji);
        $this->view->assign('dejavnostiFrom', $itemsPerPage);
        //$this->view->assign('dejavnostiCount', $allItems);
        $this->view->assign('page', $page);
        $this->view->assign('pageCount', $pageCount);
		$this->view->assign('dejavnosti', $data);
        $this->view->assign('cezmejnoDelovanjeChecked', $this->getFilter('cezmejnoDelovanje'));
        $this->view->assign('sektorSelected', $this->getFilter('sektor'));
	}
    
    private function setFilter() {
        //$this->filter = $GLOBALS['TSFE']->fe_user->getKey('ses', 'tx_a3ekt_template');
        
        if($this->request->hasArgument('cezmejnoDelovanje'))
            $this->filter['cezmejnoDelovanje'] = $this->request->getArgument('cezmejnoDelovanje');
        
        if($this->request->hasArgument('sektor'))
            $this->filter['sektor'] = $this->request->getArgument('sektor');
        
        //$GLOBALS['TSFE']->fe_user->setKey('ses', 'tx_a3ekt_template', $this->filter);
        //$GLOBALS['TSFE']->fe_user->storeSessionData();
    }
    
    private function getFilter($argument) {
        return ($this->filter[$argument]=="") ? "-1" : $this->filter[$argument];
    }
    
    private function clearFilter() {
        $GLOBALS['TSFE']->fe_user->setKey('ses', 'tx_a3ekt_template', null);
        $GLOBALS['TSFE']->fe_user->storeSessionData();
    }

	/**
	 * action show
	 *
	 * @param $dejavnost
	 * @return void
	 */
	public function showAction(Tx_A3Ekt_Domain_Model_Dejavnost $dejavnost) {
		
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('pravne_podlage','tx_a3ekt_domain_model_dejavnost', 'uid = "'.$dejavnost->getUid().'"');
		$data = array();
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) $data[] = $row['pravne_podlage'];
		$podlage = explode(',', $data[0]);
		
		$pravnePodlage = array();
		foreach($dejavnost->getPravnePodlage() as $pravnaPodlaga) {
			$pravnePodlage[$pravnaPodlaga->getUid()] = $pravnaPodlaga;
		}
		$pravnePodlageSort = array();
		foreach($podlage as $podlaga) {
			$pravnePodlageSort[] = $pravnePodlage[$podlaga];
		}
		
		$this->view->assign('pravnePodlage', $pravnePodlageSort);
		$this->view->assign('dejavnost', $dejavnost);
	}

}
?>