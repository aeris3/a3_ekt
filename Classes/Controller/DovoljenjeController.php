<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Controller_DovoljenjeController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * dovoljenjeRepository
	 *
	 * @var Tx_A3Ekt_Domain_Repository_DovoljenjeRepository
	 */
	protected $dovoljenjeRepository;
    
    /**
	 * filter
	 *
	 * @var $filter
	 */
    private $filter;

	/**
	 * injectDovoljenjeRepository
	 *
	 * @param Tx_A3Ekt_Domain_Repository_DovoljenjeRepository $dovoljenjeRepository
	 * @return void
	 */
	public function injectDovoljenjeRepository(Tx_A3Ekt_Domain_Repository_DovoljenjeRepository $dovoljenjeRepository) {
		$this->dovoljenjeRepository = $dovoljenjeRepository;
	}
	
	private function getDovoljenjaForFilter($cezmejnoDelovanjeId, $sektorId) {
		
		// only cezmejnoDelovanje
		if ( $sektorId == -1 || $sektorId == null ) {
			
			$dejavnostRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_DejavnostRepository');
	        $dejavnostQuery = $dejavnostRepository->createQuery();
			if ( $cezmejnoDelovanjeId == -1 || $cezmejnoDelovanjeId == null ) {
	        	$dejavnosti = $dejavnostQuery->matching($dejavnostQuery->in("dejavnost_po_e_k_t", $EKTDejavnostiId))->execute();
			} else {
				$dejavnosti = $dejavnostQuery->matching(
					$dejavnostQuery->logicalAnd(
						$dejavnostQuery->equals("cezmejno_opravljanje", $cezmejnoDelovanjeId)
					)
				)->execute();
			}
			
		} else {
		
			// if cezmejnoDelovanje and sektor OR only sektor
			$sektorDejavnostiRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTDejavnostRepository');
	        $sektorDejavnostiQuery = $sektorDejavnostiRepository->createQuery();
	        $sektorDejavnosti = $sektorDejavnostiQuery->matching($sektorDejavnostiQuery->equals("sektor_dejavnosti", $sektorId))->execute();
		
	        $EKTDejavnostiId=array();
	        foreach($sektorDejavnosti as $sektorDejavnost) {
	            $EKTDejavnostiId[] = $sektorDejavnost->getUid();
	        }

			// ni obrodilo sadov
			if ( count($EKTDejavnostiId) == 0 )
				return null;
			
			$dejavnostRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_DejavnostRepository');
	        $dejavnostQuery = $dejavnostRepository->createQuery();
			if ( $cezmejnoDelovanjeId == -1 || $cezmejnoDelovanjeId == null ) {
	        	$dejavnosti = $dejavnostQuery->matching($dejavnostQuery->in("dejavnost_po_e_k_t", $EKTDejavnostiId))->execute();
			} else {
				$dejavnosti = $dejavnostQuery->matching(
					$dejavnostQuery->logicalAnd(
						$dejavnostQuery->in("dejavnost_po_e_k_t", $EKTDejavnostiId),
						$dejavnostQuery->equals("cezmejno_opravljanje", $cezmejnoDelovanjeId)
					)
				)->execute();
			}
		
		}

        $dejavnostiId=array();
        foreach($dejavnosti as $dejavnost) {
            $dejavnostiId[] = $dejavnost->getUid();
        }
		
		// ni obrodilo sadov
		if ( count($dejavnostiId) == 0 )
			return null;
		
        $EKTPovezavePogojRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTPovezavePogojRepository');
        $EKTPovezavePogojRepositoryQuery = $EKTPovezavePogojRepository->createQuery();
		$EKTPovezavePogoj = $EKTPovezavePogojRepositoryQuery->matching(
			$EKTPovezavePogojRepositoryQuery->logicalAnd(
				$EKTPovezavePogojRepositoryQuery->in("dejavnost", $dejavnostiId)
			)
		)->execute();
		
		foreach($EKTPovezavePogoj as $povezavaPogoja) {
			$nekej = $povezavaPogoja->getPogoj();
			foreach($nekej as $nekej2) {
				$pogojiId[] = $nekej2->getUid();
			}
        }

		// ni obrodilo sadov
		if ( count($pogojiId) == 0 )
			return null;
		
		$povezavePogojevRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository');
        $povezavePogojevRepositoryQuery = $povezavePogojevRepository->createQuery();
		$povezavePogojev = $povezavePogojevRepositoryQuery->matching(
			$povezavePogojevRepositoryQuery->logicalAnd(
				$povezavePogojevRepositoryQuery->in("uid", $pogojiId),
				$povezavePogojevRepositoryQuery->equals("tip", 2)
			)
		)->execute();
		
		foreach($povezavePogojev as $povezavaPogoja) {
				$dovoljenjaId[] = $povezavaPogoja->getDovoljenje()->getUid();
        }
		
		$dovoljenjaId = array_unique($dovoljenjaId);
		
		// ni obrodilo sadov
		if ( count($dovoljenjaId) == 0 )
			return null;
		
		$dovoljenjeQuery = $this->dovoljenjeRepository->createQuery();
		
		// if only sektor OR sektor and cezmejnoDelovanje
		$dovoljenja = $dovoljenjeQuery->matching($dovoljenjeQuery->in("uid", $dovoljenjaId))->execute();
		
		return $dovoljenja;
	}

	/**
	 * action list
	 *
     * @param int $page page number
	 * @return void
	 */
	public function listAction($page=1) {
		//$dovoljenjes = $this->dovoljenjeRepository->findAll();
		//$this->view->assign('dovoljenjes', $dovoljenjes);
        $this->setFilter();
        
        $pageCount = 0;
        $itemsPerPage = 10;
        
        $this->dovoljenjeRepository->setDefaultOrderings(array('naziv'=>'ASC'));
		
		$cezmejnoDelovanje = $this->getFilter('cezmejnoDelovanje');
        $sektor = $this->getFilter('sektor');
		
        //$query = $this->dovoljenjeRepository->createQuery();
        
       // $cezmejnoDelovanje = $this->getFilter('cezmejnoDelovanje');
        //$sektor = $this->getFilter('sektor');
        
        //if cezmejno dovoljenje
        //if($cezmejnoDelovanje!="" && $cezmejnoDelovanje!="-1") {
            
            //if cezmejno dovoljenje and sektor
            /*if($sektor!="" && $sektor!="-1") {
                
                $sektorDejavnostiRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTDejavnostRepository');
                $sektorDejavnostiQuery = $sektorDejavnostiRepository->createQuery();
                $sektorDejavnosti = $sektorDejavnostiQuery->matching($sektorDejavnostiQuery->equals("sektor_dejavnosti", $sektor))->execute();
                
                $sektorDejavnostiId=array();
                foreach($sektorDejavnosti as $sektorDejavnost) {
                    $sektorDejavnostiId[] = $sektorDejavnost->getUid();
                }
                
                $dejavnosti = $query->matching(
                        $query->logicalAnd(
                                $query->equals("cezmejno_opravljanje", $this->getFilter('cezmejnoDelovanje')),
                                $query->in("dejavnost_po_e_k_t", $sektorDejavnostiId)
                        )
                )->execute();
                
                $allItems = $query->matching(
                        $query->logicalAnd(
                                $query->equals("cezmejno_opravljanje", $this->getFilter('cezmejnoDelovanje')),
                                $query->in("dejavnost_po_e_k_t", $sektorDejavnostiId)
                        )
                )->execute()->count();
            
            //only področje    
            } else { */
              //  $poklici = $query->matching($query->equals("cezmejno_opravljanje_poklica", $this->getFilter('cezmejnoDelovanje')))->execute();
               // $allItems = $this->poklicRepository->createQuery()->matching($query->equals("cezmejno_opravljanje_poklica", $this->getFilter('cezmejnoDelovanje')))->execute()->count();
            //}
            
       // } else {
            
            //if sector
            /*if($sektor!="" && $sektor!=-1) {
                
                $sektorDejavnostiRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTDejavnostRepository');
                $sektorDejavnostiQuery = $sektorDejavnostiRepository->createQuery();
                $sektorDejavnosti = $sektorDejavnostiQuery->matching($sektorDejavnostiQuery->equals("sektor_dejavnosti", $sektor))->execute();
                
                $sektorDejavnostiId=array();
                foreach($sektorDejavnosti as $sektorDejavnost) {
                    $sektorDejavnostiId[] = $sektorDejavnost->getUid();
                }
                
                $dejavnosti = $query->matching(
                    $query->in("dejavnost_po_e_k_t", $sektorDejavnostiId)
                )->execute();
                
                $allItems = $query->matching(
                    $query->in("dejavnost_po_e_k_t", $sektorDejavnostiId)
                )->execute()->count();
            
            //no filters
            } else {
                $dovoljenja = $query->execute();
                $allItems = $this->dovoljenjeRepository->findAll()->count();
            //}
            
        //}*/

		//if cezmejno dovoljenje
        if($cezmejnoDelovanje!="" && $cezmejnoDelovanje!="-1") {
            
            //if cezmejno dovoljenje and sektor
            if($sektor!="" && $sektor!="-1") {
                
				$dovoljenja = $this->getDovoljenjaForFilter($cezmejnoDelovanje, $sektor);
				//$allItems = count($dovoljenja);
				
            // only cezmejno dovoljenje set in filter
            } else {
				$dovoljenja = $this->getDovoljenjaForFilter($cezmejnoDelovanje);
				//$allItems = count($dovoljenja);
            }
            
        } else {
			
            // if only sector in filter is chosen
            if($sektor!="" && $sektor!=-1) {
                
				$dovoljenja = $this->getDovoljenjaForFilter(-1, $sektor);
				//$allItems = count($dovoljenja);
            	
			// if no filters
            } else {
				$query = $this->dovoljenjeRepository->createQuery();
                $dovoljenja = $query->execute();
                //$allItems = count($dovoljenja);
            }
            
        }
        
        /*try {
            $pageCount = Tx_ExtbasePager_Utility_Pager::prepareQuery($query, $page, $itemsPerPage);
        } catch(Exception $e) {
            $noResults="Iskanje ni obrodilo sadov!";
        }*/
        
        $data=array();
        foreach($dovoljenja as $dovoljenje) {
            $data[] =  array(
                "uid"   => $dovoljenje->getUid(),
                "naziv" => $dovoljenje->getNaziv(),
            );
        }
        
        //sektorji/področja
        $sektorRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_SektorRepository');
        $sektorji = $sektorRepository->createQuery()->execute();
        
        
        //if($itemsPerPage>$allItems)
            //$itemsPerPage=$allItems;
        
        $this->view->assign('noResults', $noResults);
        $this->view->assign('sektorji', $sektorji);
        $this->view->assign('dovoljenjaFrom', $itemsPerPage);
        //$this->view->assign('dovoljenjaCount', $allItems);
        $this->view->assign('page', $page);
        $this->view->assign('pageCount', $pageCount);
		$this->view->assign('dovoljenja', $data);
        $this->view->assign('cezmejnoDelovanjeChecked', $this->getFilter('cezmejnoDelovanje'));
        $this->view->assign('sektorSelected', $this->getFilter('sektor'));
	}
    
    private function setFilter() {
        //$this->filter = $GLOBALS['TSFE']->fe_user->getKey('ses', 'tx_a3ekt_template');
        
        if($this->request->hasArgument('cezmejnoDelovanje'))
            $this->filter['cezmejnoDelovanje'] = $this->request->getArgument('cezmejnoDelovanje');
        
        if($this->request->hasArgument('sektor'))
            $this->filter['sektor'] = $this->request->getArgument('sektor');
        
        //$GLOBALS['TSFE']->fe_user->setKey('ses', 'tx_a3ekt_template', $this->filter);
        //$GLOBALS['TSFE']->fe_user->storeSessionData();
    }
    
    private function getFilter($argument) {
        return ($this->filter[$argument]=="") ? "-1" : $this->filter[$argument];
    }
    
    private function clearFilter() {
        $GLOBALS['TSFE']->fe_user->setKey('ses', 'tx_a3ekt_template', null);
        $GLOBALS['TSFE']->fe_user->storeSessionData();
    }

	/**
	 * action show
	 *
	 * @param $dovoljenje
	 * @return void
	 */
	public function showAction(Tx_A3Ekt_Domain_Model_Dovoljenje $dovoljenje) {
		$this->view->assign('dovoljenje', $dovoljenje);
		
		if ( $this->request->hasArgument('dejavnost') ) {
			
			$dejavnostUid = $this->request->getArgument('dejavnost');
			$this->view->assign('dejavnost', true);
			
			// getting ektpovezave pogoj from dejavnost
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EktPovezavePogojRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->logicalAnd(
					$query->equals('dejavnost', $dejavnostUid)
			))->execute();
			
			$ektpovezavepogojUid = $data->getFirst()->getUid();
			
			// getting the linking povezave pogojev record
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->logicalAnd(
					$query->equals('dovoljenje', $dovoljenje),
					$query->equals('ektpovezavepogoj', $ektpovezavepogojUid)
			))->execute();
			
			// getting the povezave pogojev records in correct order
			$query = $nekej->createQuery();
			$pogoji = $query->matching(
				$query->logicalAnd(
					$query->equals('povezavepogojev', $data[0]->getUid())
			))->setOrderings(array('sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING))->execute();
			
		} elseif ( $this->request->hasArgument('povezavePogojev') ) {
			
			$povezavePogojevUid = $this->request->getArgument('povezavePogojev');
			$this->view->assign('povezavePogojev', $povezavePogojevUid);
			
			// getting the linking povezave pogojev record
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->equals('uid', $povezavePogojevUid)
			)->execute();
			
			// getting the povezave pogojev records in correct order
			$query = $nekej->createQuery();
			$pogoji = $query->matching(
				$query->logicalAnd(
					$query->equals('povezavepogojev', $data[0]->getUid())
			))->setOrderings(array('sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING))->execute();
			
		} elseif ( $this->request->hasArgument('poklic') ) {
			
			$poklicUid = $this->request->getArgument('poklic');
			$this->view->assign('poklic', true);
			
			// getting ektpovezave pogoj from dejavnost
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EktPovezavePoklicevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->logicalAnd(
					$query->equals('poklic', $poklicUid)
			))->execute();
			$ektpovezavepoklicUid = $data->getFirst()->getUid();
			
			// getting the linking povezave pogojev record
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->logicalAnd(
					$query->equals('dovoljenje', $dovoljenje),
					$query->equals('ektpovezavepoklicev', $ektpovezavepoklicUid)
			))->execute();
			
			// getting the povezave pogojev records in correct order
			$query = $nekej->createQuery();
			$pogoji = $query->matching(
				$query->logicalAnd(
					$query->equals('povezavepoklicev', $data[0]->getUid())
			))->setOrderings(array('sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING))->execute();
			
		} elseif ( $this->request->hasArgument('povezavePoklicev') ) {
			
			$povezavePoklicevUid = $this->request->getArgument('povezavePoklicev');
			$this->view->assign('povezavePoklicev', $povezavePogojevUid);
			
			// getting the linking povezave pogojev record
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->equals('uid', $povezavePoklicevUid)
			)->execute();
			
			// getting the povezave pogojev records in correct order
			$query = $nekej->createQuery();
			$pogoji = $query->matching(
				$query->logicalAnd(
					$query->equals('povezavepoklicev', $data[0]->getUid())
			))->setOrderings(array('sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING))->execute();
			
		}
		
		// getting all poklici if there is no poklic
		// tak tak ... ni še najbolje kul - start
		if ( !$this->request->hasArgument('poklic') ) {
			
			$povezavePoklicevRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository');
	        $povezavePoklicevRepositoryQuery = $povezavePoklicevRepository->createQuery();
			$povezavePoklicev = $povezavePoklicevRepositoryQuery->matching(
				$povezavePoklicevRepositoryQuery->equals("dovoljenje", $dovoljenje->getUid())
			)->execute();
			
			$ektPovezavePoklicevId = array();
			foreach($povezavePoklicev as $povezavaPoklica) {
				$tmp_ektpovezave = $povezavaPoklica->getEKTPovezavePoklicev();
				foreach($tmp_ektpovezave->getPoklic() as $poklic) {
					$pokliciId[] = $poklic->getUid();
				}
	        }
			
			$pokliciId = array_unique($pokliciId);
			
			// ni obrodilo sadov
			if ( count($pokliciId) != 0 ) {
				$poklicRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PoklicRepository');
				$poklicQuery = $poklicRepository->createQuery();
				$poklici = $poklicQuery->matching($poklicQuery->equals("uid", $pokliciId))->execute();
				
				$this->view->assign('poklici', $poklici);
				$this->view->assign('stPoklicev', count($poklici));
				$this->view->assign('limit', 5);
			}
		}
		
		// getting all dejavnosti if there is no dejavnost
		if ( !$this->request->hasArgument('dejavnost') ) {
		
			$povezavePogojevRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository');
	        $povezavePogojevRepositoryQuery = $povezavePogojevRepository->createQuery();
			$povezavePogojev = $povezavePogojevRepositoryQuery->matching(
				$povezavePogojevRepositoryQuery->equals("dovoljenje", $dovoljenje->getUid())
			)->execute();
			
			$dejavnostiId = array();
			foreach($povezavePogojev as $povezavaPogoja) {
				$tmp_ektpovezave = $povezavaPogoja->getEKTPovezavePogoj();
				if ($tmp_ektpovezave != null) {
					foreach($tmp_ektpovezave->getDejavnost() as $dejavnost) {
						$dejavnostiId[] = $dejavnost->getUid();
					}
				}
	        }
			
			$dejavnostiId = array_unique($dejavnostiId);
			
			// ni obrodilo sadov
			if ( count($dejavnostiId) != 0 ) {
				$dejavnostRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_DejavnostRepository');
				$dejavnostQuery = $dejavnostRepository->createQuery();
				$dejavnosti = $dejavnostQuery->matching($dejavnostQuery->equals("uid", $dejavnostiId))->execute();

				$this->view->assign('dejavnosti', $dejavnosti);
				$this->view->assign('stDejavnosti', count($dejavnosti));
				$this->view->assign('limit', 5);
			}
		}
		// tak tak ... ni še najbolje kul - end
		
		$this->view->assign('pogoji', $pogoji);
		
		$this->view->assign('povezavepogojevs', $povezavepogojevs);
	}

}
?>