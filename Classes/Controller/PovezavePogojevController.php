<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Controller_PovezavePogojevController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * povezavePogojevRepository
	 *
	 * @var Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository
	 */
	protected $povezavePogojevRepository;

	/**
	 * injectPovezavePogojevRepository
	 *
	 * @param Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository $povezavePogojevRepository
	 * @return void
	 */
	public function injectPovezavePogojevRepository(Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository $povezavePogojevRepository) {
		$this->povezavePogojevRepository = $povezavePogojevRepository;
	}

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$povezavePogojevs = $this->povezavePogojevRepository->findAll();
		$this->view->assign('povezavePogojevs', $povezavePogojevs);
	}

	/**
	 * action show
	 *
	 * @param $povezavePogojev
	 * @return void
	 */
	public function showAction(Tx_A3Ekt_Domain_Model_PovezavePogojev $povezavePogojev) {
		$this->view->assign('povezavePogojev', $povezavePogojev);
	}

	/**
	 * action new
	 *
	 * @param $newPovezavePogojev
	 * @dontvalidate $newPovezavePogojev
	 * @return void
	 */
	public function newAction(Tx_A3Ekt_Domain_Model_PovezavePogojev $newPovezavePogojev = NULL) {
		$this->view->assign('newPovezavePogojev', $newPovezavePogojev);
	}

	/**
	 * action create
	 *
	 * @param $newPovezavePogojev
	 * @return void
	 */
	public function createAction(Tx_A3Ekt_Domain_Model_PovezavePogojev $newPovezavePogojev) {
		$this->povezavePogojevRepository->add($newPovezavePogojev);
		$this->flashMessageContainer->add('Your new PovezavePogojev was created.');
		$this->redirect('list');
	}

	/**
	 * action edit
	 *
	 * @param $povezavePogojev
	 * @return void
	 */
	public function editAction(Tx_A3Ekt_Domain_Model_PovezavePogojev $povezavePogojev) {
		$this->view->assign('povezavePogojev', $povezavePogojev);
	}

	/**
	 * action update
	 *
	 * @param $povezavePogojev
	 * @return void
	 */
	public function updateAction(Tx_A3Ekt_Domain_Model_PovezavePogojev $povezavePogojev) {
		$this->povezavePogojevRepository->update($povezavePogojev);
		$this->flashMessageContainer->add('Your PovezavePogojev was updated.');
		$this->redirect('list');
	}

	/**
	 * action delete
	 *
	 * @param $povezavePogojev
	 * @return void
	 */
	public function deleteAction(Tx_A3Ekt_Domain_Model_PovezavePogojev $povezavePogojev) {
		$this->povezavePogojevRepository->remove($povezavePogojev);
		$this->flashMessageContainer->add('Your PovezavePogojev was removed.');
		$this->redirect('list');
	}

}
?>