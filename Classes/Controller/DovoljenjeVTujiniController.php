<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Controller_DovoljenjeVTujiniController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * dovoljenjeVTujiniRepository
	 *
	 * @var Tx_A3Ekt_Domain_Repository_DovoljenjeVTujiniRepository
	 */
	protected $dovoljenjeVTujiniRepository;

	/**
	 * injectDovoljenjeVTujiniRepository
	 *
	 * @param Tx_A3Ekt_Domain_Repository_DovoljenjeVTujiniRepository $dovoljenjeVTujiniRepository
	 * @return void
	 */
	public function injectDovoljenjeVTujiniRepository(Tx_A3Ekt_Domain_Repository_DovoljenjeVTujiniRepository $dovoljenjeVTujiniRepository) {
		$this->dovoljenjeVTujiniRepository = $dovoljenjeVTujiniRepository;
	}

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$dovoljenjeVTujinis = $this->dovoljenjeVTujiniRepository->findAll();
		$this->view->assign('dovoljenjeVTujinis', $dovoljenjeVTujinis);
	}

	/**
	 * action show
	 *
	 * @param $dovoljenjeVTujini
	 * @return void
	 */
	public function showAction(Tx_A3Ekt_Domain_Model_DovoljenjeVTujini $dovoljenjeVTujini) {
		$this->view->assign('dovoljenjeVTujini', $dovoljenjeVTujini);
		
		if ( $this->request->hasArgument('dejavnost') ) {
			
			$dejavnostUid = $this->request->getArgument('dejavnost');
			$this->view->assign('dejavnost', true);
			
			// getting ektpovezave pogoj from dejavnost
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EktPovezavePogojRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->logicalAnd(
					$query->equals('dejavnost', $dejavnostUid)
			))->execute();
			$ektpovezavepogojUid = $data->getFirst()->getUid();
			
			// getting povezave pogojev
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->logicalAnd(
					$query->equals('dovoljenjeVTujini', $dovoljenjeVTujini),
					$query->equals('ektpovezavepogoj', $ektpovezavepogojUid)
			))->execute();
			
			// getting the povezave pogojev records in correct order
			$query = $nekej->createQuery();
			$pogoji = $query->matching(
				$query->logicalAnd(
					$query->equals('povezavepogojev', $data[0]->getUid())
			))->setOrderings(array('sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING))->execute();
			
		} elseif ( $this->request->hasArgument('povezavePogojev') ) {
			
			$povezavePogojevUid = $this->request->getArgument('povezavePogojev');
			$this->view->assign('povezavePogojev', $povezavePogojevUid);
			
			// getting povezave pogojev
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->equals('uid', $povezavePogojevUid)
			)->execute();
			
			// getting the povezave pogojev records in correct order
			$query = $nekej->createQuery();
			$pogoji = $query->matching(
				$query->logicalAnd(
					$query->equals('povezavepogojev', $data[0]->getUid())
			))->setOrderings(array('sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING))->execute();
			
		} elseif ( $this->request->hasArgument('poklic') ) {
			$poklicUid = $this->request->getArgument('poklic');
			$this->view->assign('poklic', true);
			
			// getting ektpovezave pogoj from dejavnost
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EktPovezavePoklicevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->logicalAnd(
					$query->equals('poklic', $poklicUid)
			))->execute();
			$ektpovezavepoklicUid = $data->getFirst()->getUid();
			
			// getting povezave pogojev
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->logicalAnd(
					$query->equals('dovoljenjeVTujini', $dovoljenjeVTujini),
					$query->equals('ektpovezavepoklicev', $ektpovezavepoklicUid)
			))->execute();
			
			if ( $data[0] != null ) {
				// getting the povezave pogojev records in correct order
				$query = $nekej->createQuery();
				$pogoji = $query->matching(
					$query->logicalAnd(
						$query->equals('povezavepoklicev', $data[0]->getUid())
				))->setOrderings(array('sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING))->execute();
			}
			
		} elseif ( $this->request->hasArgument('povezavePoklicev') ) {
			
			$povezavePoklicevUid = $this->request->getArgument('povezavePoklicev');
			$this->view->assign('povezavePoklicev', $povezavePogojevUid);
			
			// getting povezave pogojev
			$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository');
			$query = $nekej->createQuery();
			$data = $query->matching(
				$query->equals('uid', $povezavePoklicevUid)
			)->execute();
			
			if ( $data[0] != null ) {
				// getting the povezave pogojev records in correct order
				$query = $nekej->createQuery();
				$pogoji = $query->matching(
					$query->logicalAnd(
						$query->equals('povezavepoklicev', $data[0]->getUid())
			))->setOrderings(array('sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING))->execute();
			}
			
		}

		$this->view->assign('pogoji', $pogoji);
		
		$this->view->assign('povezavepogojevs', $povezavepogojevs);
	}

}
?>