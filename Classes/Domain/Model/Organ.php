<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Model_Organ extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * Naziv
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $naziv;

	/**
	 * Naslov
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $naslov;

	/**
	 * Spletni naslov
	 *
	 * @var string
	 */
	protected $spletniNaslov;

	/**
	 * Vrsta organa
	 *
	 * @var integer
	 */
	protected $vrstaOrgana;

	/**
	 * Pošta
	 *
	 * @var Tx_A3Ekt_Domain_Model_Posta
	 */
	protected $posta;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {

	}

	/**
	 * Returns the naziv
	 *
	 * @return string $naziv
	 */
	public function getNaziv() {
		return $this->naziv;
	}

	/**
	 * Sets the naziv
	 *
	 * @param string $naziv
	 * @return void
	 */
	public function setNaziv($naziv) {
		$this->naziv = $naziv;
	}

	/**
	 * Returns the naslov
	 *
	 * @return string $naslov
	 */
	public function getNaslov() {
		return $this->naslov;
	}

	/**
	 * Sets the naslov
	 *
	 * @param string $naslov
	 * @return void
	 */
	public function setNaslov($naslov) {
		$this->naslov = $naslov;
	}

	/**
	 * Returns the spletniNaslov
	 *
	 * @return string $spletniNaslov
	 */
	public function getSpletniNaslov() {
		return $this->spletniNaslov;
	}

	/**
	 * Sets the spletniNaslov
	 *
	 * @param string $spletniNaslov
	 * @return void
	 */
	public function setSpletniNaslov($spletniNaslov) {
		$this->spletniNaslov = $spletniNaslov;
	}

	/**
	 * Returns the vrstaOrgana
	 *
	 * @return integer $vrstaOrgana
	 */
	public function getVrstaOrgana() {
		return $this->vrstaOrgana;
	}

	/**
	 * Sets the vrstaOrgana
	 *
	 * @param integer $vrstaOrgana
	 * @return void
	 */
	public function setVrstaOrgana($vrstaOrgana) {
		$this->vrstaOrgana = $vrstaOrgana;
	}

	/**
	 * Returns the posta
	 *
	 * @return Tx_A3Ekt_Domain_Model_Posta $posta
	 */
	public function getPosta() {
		return $this->posta;
	}

	/**
	 * Sets the posta
	 *
	 * @param Tx_A3Ekt_Domain_Model_Posta $posta
	 * @return void
	 */
	public function setPosta(Tx_A3Ekt_Domain_Model_Posta $posta) {
		$this->posta = $posta;
	}

}
?>