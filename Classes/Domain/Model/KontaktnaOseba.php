<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Model_KontaktnaOseba extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * Ime in priimek
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $imePriimek;

	/**
	 * Telefon
	 *
	 * @var string
	 */
	protected $telefon;

	/**
	 * Elektronska pošta
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $email;

	/**
	 * Elektronska pošta namestnika
	 *
	 * @var string
	 */
	protected $emailNamestnika;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {

	}

	/**
	 * Returns the imePriimek
	 *
	 * @return string $imePriimek
	 */
	public function getImePriimek() {
		return $this->imePriimek;
	}

	/**
	 * Sets the imePriimek
	 *
	 * @param string $imePriimek
	 * @return void
	 */
	public function setImePriimek($imePriimek) {
		$this->imePriimek = $imePriimek;
	}

	/**
	 * Returns the telefon
	 *
	 * @return string $telefon
	 */
	public function getTelefon() {
		return $this->telefon;
	}

	/**
	 * Sets the telefon
	 *
	 * @param string $telefon
	 * @return void
	 */
	public function setTelefon($telefon) {
		$this->telefon = $telefon;
	}

	/**
	 * Returns the email
	 *
	 * @return string $email
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Sets the email
	 *
	 * @param string $email
	 * @return void
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	 * Returns the emailNamestnika
	 *
	 * @return string $emailNamestnika
	 */
	public function getEmailNamestnika() {
		return $this->emailNamestnika;
	}

	/**
	 * Sets the emailNamestnika
	 *
	 * @param string $emailNamestnika
	 * @return void
	 */
	public function setEmailNamestnika($emailNamestnika) {
		$this->emailNamestnika = $emailNamestnika;
	}

}
?>