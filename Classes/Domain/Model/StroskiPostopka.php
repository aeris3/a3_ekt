<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Model_StroskiPostopka extends Tx_Extbase_DomainObject_AbstractValueObject {

	/**
	 * Naziv stroška
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $nazivStroska;

	/**
	 * Znesek
	 *
	 * @var float
	 * @validate NotEmpty
	 */
	protected $znesek;

	/**
	 * Naziv predpisa
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $nazivPredpisa;

	/**
	 * Uradni list / drugo (url pov.)
	 *
	 * @var string
	 */
	protected $drugo;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {

	}

	/**
	 * Returns the nazivStroska
	 *
	 * @return string $nazivStroska
	 */
	public function getNazivStroska() {
		return $this->nazivStroska;
	}

	/**
	 * Sets the nazivStroska
	 *
	 * @param string $nazivStroska
	 * @return void
	 */
	public function setNazivStroska($nazivStroska) {
		$this->nazivStroska = $nazivStroska;
	}

	/**
	 * Returns the znesek
	 *
	 * @return float $znesek
	 */
	public function getZnesek() {
		return $this->znesek;
	}

	/**
	 * Sets the znesek
	 *
	 * @param float $znesek
	 * @return void
	 */
	public function setZnesek($znesek) {
		$this->znesek = $znesek;
	}

	/**
	 * Returns the nazivPredpisa
	 *
	 * @return string $nazivPredpisa
	 */
	public function getNazivPredpisa() {
		return $this->nazivPredpisa;
	}

	/**
	 * Sets the nazivPredpisa
	 *
	 * @param string $nazivPredpisa
	 * @return void
	 */
	public function setNazivPredpisa($nazivPredpisa) {
		$this->nazivPredpisa = $nazivPredpisa;
	}

	/**
	 * Returns the drugo
	 *
	 * @return string $drugo
	 */
	public function getDrugo() {
		return $this->drugo;
	}

	/**
	 * Sets the drugo
	 *
	 * @param string $drugo
	 * @return void
	 */
	public function setDrugo($drugo) {
		$this->drugo = $drugo;
	}

}
?>