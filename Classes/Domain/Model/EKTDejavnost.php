<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Model_EKTDejavnost extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * Naziv dejavnosti po EKT
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $naziv;

	/**
	 * EKT šifra
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $sifra;

	/**
	 * SKD dejavnosti
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_SKDDejavnost>
	 */
	protected $skdDejavnosti;

	/**
	 * Sektor dejavnosti
	 *
	 * @var Tx_A3Ekt_Domain_Model_Sektor
	 */
	protected $sektorDejavnosti;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->skdDejavnosti = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the naziv
	 *
	 * @return string $naziv
	 */
	public function getNaziv() {
		return $this->naziv;
	}

	/**
	 * Sets the naziv
	 *
	 * @param string $naziv
	 * @return void
	 */
	public function setNaziv($naziv) {
		$this->naziv = $naziv;
	}

	/**
	 * Returns the sifra
	 *
	 * @return string $sifra
	 */
	public function getSifra() {
		return $this->sifra;
	}

	/**
	 * Sets the sifra
	 *
	 * @param string $sifra
	 * @return void
	 */
	public function setSifra($sifra) {
		$this->sifra = $sifra;
	}

	/**
	 * Adds a SKDDejavnost
	 *
	 * @param Tx_A3Ekt_Domain_Model_SKDDejavnost $skdDejavnosti
	 * @return void
	 */
	public function addSkdDejavnosti(Tx_A3Ekt_Domain_Model_SKDDejavnost $skdDejavnosti) {
		$this->skdDejavnosti->attach($skdDejavnosti);
	}

	/**
	 * Removes a SKDDejavnost
	 *
	 * @param Tx_A3Ekt_Domain_Model_SKDDejavnost $skdDejavnostiToRemove The SKDDejavnost to be removed
	 * @return void
	 */
	public function removeSkdDejavnosti(Tx_A3Ekt_Domain_Model_SKDDejavnost $skdDejavnostiToRemove) {
		$this->skdDejavnosti->detach($skdDejavnostiToRemove);
	}

	/**
	 * Returns the skdDejavnosti
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_SKDDejavnost> $skdDejavnosti
	 */
	public function getSkdDejavnosti() {
		return $this->skdDejavnosti;
	}

	/**
	 * Sets the skdDejavnosti
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_SKDDejavnost> $skdDejavnosti
	 * @return void
	 */
	public function setSkdDejavnosti(Tx_Extbase_Persistence_ObjectStorage $skdDejavnosti) {
		$this->skdDejavnosti = $skdDejavnosti;
	}

	/**
	 * Returns the sektorDejavnosti
	 *
	 * @return Tx_A3Ekt_Domain_Model_Sektor $sektorDejavnosti
	 */
	public function getSektorDejavnosti() {
		return $this->sektorDejavnosti;
	}

	/**
	 * Sets the sektorDejavnosti
	 *
	 * @param Tx_A3Ekt_Domain_Model_Sektor $sektorDejavnosti
	 * @return void
	 */
	public function setSektorDejavnosti(Tx_A3Ekt_Domain_Model_Sektor $sektorDejavnosti) {
		$this->sektorDejavnosti = $sektorDejavnosti;
	}

	/**
	 * Returns the dejavnosti
	 *
	 * @return Tx_A3Ekt_Domain_Repository_DejavnostRepository<Tx_A3Ekt_Domain_Model_Dejavnost> $dejavnosti
	 */
	public function getDejavnosti() {
		        $dejavnosti = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_DejavnostRepository')->getDejavnostiForEKTDejavnost($this);
		        return $dejavnosti;
	}

}
?>