<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Model_Dovoljenje extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * Naziv dovoljenja
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $naziv;

	/**
	 * Interna oznaka
	 *
	 * @var string
	 */
	protected $oznaka;

	/**
	 * Opis dovoljenja
	 *
	 * @var string
	 */
	protected $opis;

	/**
	 * Označite v primeru obveznega članstva
	 *
	 * @var integer
	 */
	protected $obveznoClanstvo;

	/**
	 * Označite v primeru obnove dovoljenja
	 *
	 * @var integer
	 */
	protected $obnova;

	/**
	 * Termin obnove
	 *
	 * @var string
	 */
	protected $terminObnove;

	/**
	 * Opišite postopek obnove dovoljenja
	 *
	 * @var string
	 */
	protected $opisObnove;

	/**
	 * Drugo
	 *
	 * @var string
	 */
	protected $drugo;

	/**
	 * Navedite naziv pravnega sredstva (npr. pritožba / upravni spor)
	 *
	 * @var integer
	 */
	protected $pravnoSredstvo;

	/**
	 * Molk organa
	 *
	 * @var string
	 */
	protected $molkOrgana;

	/**
	 * Opomba
	 *
	 * @var string
	 */
	protected $opomba;
    
    /**
	 * Opombe
	 *
	 * @var string
	 */
	protected $opombe;

	/**
	 * Pristojna organ
	 *
	 * @var Tx_A3Ekt_Domain_Model_Organ
	 */
	protected $pristojniOrgan;
    
    /**
	 * Pristojna organ
	 *
	 * @var Tx_A3Ekt_Domain_Model_Organ
	 */
	protected $vir;

	/**
	 * Kontaktna oseba
	 *
	 * @var Tx_A3Ekt_Domain_Model_KontaktnaOseba
	 */
	protected $kontaktnaOseba;

	/**
	 * Vloge
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_Vloga>
	 */
	protected $vloge;

	/**
	 * Priloge k vlogi
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_VlogaPriloga>
	 */
	protected $prilogeKVlogi;

	/**
	 * Stroški postopka
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_StroskiPostopka>
	 */
	protected $stroskiPostopka;

	/**
	 * Koraki postopka
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_KorakiPostopka>
	 */
	protected $korakiPostopka;

	/**
	 * Pritožbena organ
	 *
	 * @var Tx_A3Ekt_Domain_Model_Organ
	 */
	protected $pritozbeniOrgan;

	/**
	 * Pravna podlaga
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PravnaPodlaga>
	 */
	protected $pravnaPodlaga;

	/**
	 * Register dovoljenj
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_RegisterDovoljenj>
	 */
	protected $registerDovoljenj;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->vloge = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->prilogeKVlogi = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->stroskiPostopka = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->korakiPostopka = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->pravnaPodlaga = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->registerDovoljenj = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the naziv
	 *
	 * @return string $naziv
	 */
	public function getNaziv() {
		return $this->naziv;
	}

	/**
	 * Sets the naziv
	 *
	 * @param string $naziv
	 * @return void
	 */
	public function setNaziv($naziv) {
		$this->naziv = $naziv;
	}

	/**
	 * Returns the oznaka
	 *
	 * @return string $oznaka
	 */
	public function getOznaka() {
		return $this->oznaka;
	}

	/**
	 * Sets the oznaka
	 *
	 * @param string $oznaka
	 * @return void
	 */
	public function setOznaka($oznaka) {
		$this->oznaka = $oznaka;
	}

	/**
	 * Returns the opis
	 *
	 * @return string $opis
	 */
	public function getOpis() {
		return $this->opis;
	}

	/**
	 * Sets the opis
	 *
	 * @param string $opis
	 * @return void
	 */
	public function setOpis($opis) {
		$this->opis = $opis;
	}

	/**
	 * Returns the obveznoClanstvo
	 *
	 * @return integer $obveznoClanstvo
	 */
	public function getObveznoClanstvo() {
		return $this->obveznoClanstvo;
	}

	/**
	 * Sets the obveznoClanstvo
	 *
	 * @param integer $obveznoClanstvo
	 * @return void
	 */
	public function setObveznoClanstvo($obveznoClanstvo) {
		$this->obveznoClanstvo = $obveznoClanstvo;
	}

	/**
	 * Returns the obnova
	 *
	 * @return integer $obnova
	 */
	public function getObnova() {
		return $this->obnova;
	}

	/**
	 * Sets the obnova
	 *
	 * @param integer $obnova
	 * @return void
	 */
	public function setObnova($obnova) {
		$this->obnova = $obnova;
	}

	/**
	 * Returns the terminObnove
	 *
	 * @return string $terminObnove
	 */
	public function getTerminObnove() {
		return $this->terminObnove;
	}

	/**
	 * Sets the terminObnove
	 *
	 * @param string $terminObnove
	 * @return void
	 */
	public function setTerminObnove($terminObnove) {
		$this->terminObnove = $terminObnove;
	}

	/**
	 * Returns the opisObnove
	 *
	 * @return string $opisObnove
	 */
	public function getOpisObnove() {
		return $this->opisObnove;
	}

	/**
	 * Sets the opisObnove
	 *
	 * @param string $opisObnove
	 * @return void
	 */
	public function setOpisObnove($opisObnove) {
		$this->opisObnove = $opisObnove;
	}

	/**
	 * Returns the drugo
	 *
	 * @return string $drugo
	 */
	public function getDrugo() {
		return $this->drugo;
	}

	/**
	 * Sets the drugo
	 *
	 * @param string $drugo
	 * @return void
	 */
	public function setDrugo($drugo) {
		$this->drugo = $drugo;
	}

	/**
	 * Returns the pravnoSredstvo
	 *
	 * @return integer $pravnoSredstvo
	 */
	public function getPravnoSredstvo() {
		return $this->pravnoSredstvo;
	}

	/**
	 * Sets the pravnoSredstvo
	 *
	 * @param integer $pravnoSredstvo
	 * @return void
	 */
	public function setPravnoSredstvo($pravnoSredstvo) {
		$this->pravnoSredstvo = $pravnoSredstvo;
	}

	/**
	 * Returns the molkOrgana
	 *
	 * @return string $molkOrgana
	 */
	public function getMolkOrgana() {
		return $this->molkOrgana;
	}

	/**
	 * Sets the molkOrgana
	 *
	 * @param string $molkOrgana
	 * @return void
	 */
	public function setMolkOrgana($molkOrgana) {
		$this->molkOrgana = $molkOrgana;
	}

	/**
	 * Returns the opomba
	 *
	 * @return string $opomba
	 */
	public function getOpomba() {
		return $this->opomba;
	}

	/**
	 * Sets the opomba
	 *
	 * @param string $opomba
	 * @return void
	 */
	public function setOpomba($opomba) {
		$this->opomba = $opomba;
	}
    
    /**
	 * Returns the opomba
	 *
	 * @return string $opombe
	 */
	public function getOpombe() {
		return $this->opombe;
	}

	/**
	 * Sets the opombe
	 *
	 * @param string $opombe
	 * @return void
	 */
	public function setOpombe($opombe) {
		$this->opombe = $opombe;
	}

	/**
	 * Returns the kontaktnaOseba
	 *
	 * @return Tx_A3Ekt_Domain_Model_KontaktnaOseba $kontaktnaOseba
	 */
	public function getKontaktnaOseba() {
		return $this->kontaktnaOseba;
	}

	/**
	 * Sets the kontaktnaOseba
	 *
	 * @param Tx_A3Ekt_Domain_Model_KontaktnaOseba $kontaktnaOseba
	 * @return void
	 */
	public function setKontaktnaOseba(Tx_A3Ekt_Domain_Model_KontaktnaOseba $kontaktnaOseba) {
		$this->kontaktnaOseba = $kontaktnaOseba;
	}

	/**
	 * Adds a VlogaPriloga
	 *
	 * @param Tx_A3Ekt_Domain_Model_VlogaPriloga $prilogeKVlogi
	 * @return void
	 */
	public function addPrilogeKVlogi(Tx_A3Ekt_Domain_Model_VlogaPriloga $prilogeKVlogi) {
		$this->prilogeKVlogi->attach($prilogeKVlogi);
	}

	/**
	 * Removes a VlogaPriloga
	 *
	 * @param Tx_A3Ekt_Domain_Model_VlogaPriloga $prilogeKVlogiToRemove The VlogaPriloga to be removed
	 * @return void
	 */
	public function removePrilogeKVlogi(Tx_A3Ekt_Domain_Model_VlogaPriloga $prilogeKVlogiToRemove) {
		$this->prilogeKVlogi->detach($prilogeKVlogiToRemove);
	}

	/**
	 * Returns the prilogeKVlogi
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_VlogaPriloga> $prilogeKVlogi
	 */
	public function getPrilogeKVlogi() {
		return $this->prilogeKVlogi;
	}

	/**
	 * Sets the prilogeKVlogi
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_VlogaPriloga> $prilogeKVlogi
	 * @return void
	 */
	public function setPrilogeKVlogi(Tx_Extbase_Persistence_ObjectStorage $prilogeKVlogi) {
		$this->prilogeKVlogi = $prilogeKVlogi;
	}

	/**
	 * Adds a StroskiPostopka
	 *
	 * @param Tx_A3Ekt_Domain_Model_StroskiPostopka $stroskiPostopka
	 * @return void
	 */
	public function addStroskiPostopka(Tx_A3Ekt_Domain_Model_StroskiPostopka $stroskiPostopka) {
		$this->stroskiPostopka->attach($stroskiPostopka);
	}

	/**
	 * Removes a StroskiPostopka
	 *
	 * @param Tx_A3Ekt_Domain_Model_StroskiPostopka $stroskiPostopkaToRemove The StroskiPostopka to be removed
	 * @return void
	 */
	public function removeStroskiPostopka(Tx_A3Ekt_Domain_Model_StroskiPostopka $stroskiPostopkaToRemove) {
		$this->stroskiPostopka->detach($stroskiPostopkaToRemove);
	}

	/**
	 * Returns the stroskiPostopka
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_StroskiPostopka> $stroskiPostopka
	 */
	public function getStroskiPostopka() {
		return $this->stroskiPostopka;
	}

	/**
	 * Sets the stroskiPostopka
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_StroskiPostopka> $stroskiPostopka
	 * @return void
	 */
	public function setStroskiPostopka(Tx_Extbase_Persistence_ObjectStorage $stroskiPostopka) {
		$this->stroskiPostopka = $stroskiPostopka;
	}

	/**
	 * Adds a KorakiPostopka
	 *
	 * @param Tx_A3Ekt_Domain_Model_KorakiPostopka $korakiPostopka
	 * @return void
	 */
	public function addKorakiPostopka(Tx_A3Ekt_Domain_Model_KorakiPostopka $korakiPostopka) {
		$this->korakiPostopka->attach($korakiPostopka);
	}

	/**
	 * Removes a KorakiPostopka
	 *
	 * @param Tx_A3Ekt_Domain_Model_KorakiPostopka $korakiPostopkaToRemove The KorakiPostopka to be removed
	 * @return void
	 */
	public function removeKorakiPostopka(Tx_A3Ekt_Domain_Model_KorakiPostopka $korakiPostopkaToRemove) {
		$this->korakiPostopka->detach($korakiPostopkaToRemove);
	}

	/**
	 * Returns the korakiPostopka
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_KorakiPostopka> $korakiPostopka
	 */
	public function getKorakiPostopka() {
		        return $this->korakiPostopka;
	}

	/**
	 * Sets the korakiPostopka
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_KorakiPostopka> $korakiPostopka
	 * @return void
	 */
	public function setKorakiPostopka(Tx_Extbase_Persistence_ObjectStorage $korakiPostopka) {
		$this->korakiPostopka = $korakiPostopka;
	}

	/**
	 * Returns the pritozbeniOrgan
	 *
	 * @return Tx_A3Ekt_Domain_Model_Organ $pritozbeniOrgan
	 */
	public function getPritozbeniOrgan() {
		return $this->pritozbeniOrgan;
	}

	/**
	 * Sets the pritozbeniOrgan
	 *
	 * @param Tx_A3Ekt_Domain_Model_Organ $pritozbeniOrgan
	 * @return void
	 */
	public function setPritozbeniOrgan(Tx_A3Ekt_Domain_Model_Organ $pritozbeniOrgan) {
		$this->pritozbeniOrgan = $pritozbeniOrgan;
	}

	/**
	 * Adds a PravnaPodlaga
	 *
	 * @param Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnaPodlaga
	 * @return void
	 */
	public function addPravnaPodlaga(Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnaPodlaga) {
		$this->pravnaPodlaga->attach($pravnaPodlaga);
	}

	/**
	 * Removes a PravnaPodlaga
	 *
	 * @param Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnaPodlagaToRemove The PravnaPodlaga to be removed
	 * @return void
	 */
	public function removePravnaPodlaga(Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnaPodlagaToRemove) {
		$this->pravnaPodlaga->detach($pravnaPodlagaToRemove);
	}

	/**
	 * Returns the pravnaPodlaga
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PravnaPodlaga> $pravnaPodlaga
	 */
	public function getPravnaPodlaga() {
		return $this->pravnaPodlaga;
	}

	/**
	 * Sets the pravnaPodlaga
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PravnaPodlaga> $pravnaPodlaga
	 * @return void
	 */
	public function setPravnaPodlaga(Tx_Extbase_Persistence_ObjectStorage $pravnaPodlaga) {
		$this->pravnaPodlaga = $pravnaPodlaga;
	}

	/**
	 * Adds a RegisterDovoljenj
	 *
	 * @param Tx_A3Ekt_Domain_Model_RegisterDovoljenj $registerDovoljenj
	 * @return void
	 */
	public function addRegisterDovoljenj(Tx_A3Ekt_Domain_Model_RegisterDovoljenj $registerDovoljenj) {
		$this->registerDovoljenj->attach($registerDovoljenj);
	}

	/**
	 * Removes a RegisterDovoljenj
	 *
	 * @param Tx_A3Ekt_Domain_Model_RegisterDovoljenj $registerDovoljenjToRemove The RegisterDovoljenj to be removed
	 * @return void
	 */
	public function removeRegisterDovoljenj(Tx_A3Ekt_Domain_Model_RegisterDovoljenj $registerDovoljenjToRemove) {
		$this->registerDovoljenj->detach($registerDovoljenjToRemove);
	}

	/**
	 * Returns the registerDovoljenj
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_RegisterDovoljenj> $registerDovoljenj
	 */
	public function getRegisterDovoljenj() {
		return $this->registerDovoljenj;
	}

	/**
	 * Sets the registerDovoljenj
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_RegisterDovoljenj> $registerDovoljenj
	 * @return void
	 */
	public function setRegisterDovoljenj(Tx_Extbase_Persistence_ObjectStorage $registerDovoljenj) {
		$this->registerDovoljenj = $registerDovoljenj;
	}

	/**
	 * Adds a Vloga
	 *
	 * @param Tx_A3Ekt_Domain_Model_Vloga $vloge
	 * @return void
	 */
	public function addVloge(Tx_A3Ekt_Domain_Model_Vloga $vloge) {
		$this->vloge->attach($vloge);
	}

	/**
	 * Removes a Vloga
	 *
	 * @param Tx_A3Ekt_Domain_Model_Vloga $vlogeToRemove The Vloga to be removed
	 * @return void
	 */
	public function removeVloge(Tx_A3Ekt_Domain_Model_Vloga $vlogeToRemove) {
		$this->vloge->detach($vlogeToRemove);
	}

	/**
	 * Returns the vloge
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_Vloga> $vloge
	 */
	public function getVloge() {
		return $this->vloge;
	}

	/**
	 * Sets the vloge
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_Vloga> $vloge
	 * @return void
	 */
	public function setVloge(Tx_Extbase_Persistence_ObjectStorage $vloge) {
		$this->vloge = $vloge;
	}

	/**
	 * Returns the pristojniOrgan
	 *
	 * @return Tx_A3Ekt_Domain_Model_Organ $pristojniOrgan
	 */
	public function getPristojniOrgan() {
		return $this->pristojniOrgan;
	}

	/**
	 * Sets the pristojniOrgan
	 *
	 * @param Tx_A3Ekt_Domain_Model_Organ $pristojniOrgan
	 * @return void
	 */
	public function setPristojniOrgan(Tx_A3Ekt_Domain_Model_Organ $pristojniOrgan) {
		$this->pristojniOrgan = $pristojniOrgan;
	}
    
    /**
	 * Returns the pristojniOrgan
	 *
	 * @return Tx_A3Ekt_Domain_Model_Organ $vir
	 */
	public function getVir() {
		return $this->vir;
	}

}
?>