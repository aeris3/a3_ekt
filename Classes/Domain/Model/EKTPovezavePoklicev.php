<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Model_EKTPovezavePoklicev extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * Opomba
	 *
	 * @var string
	 */
	protected $opomba;

	/**
	 * Poklicna kvalifikacija
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_Poklic>
	 */
	protected $poklic;

	/**
	 * Pogoj
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PovezavePoklicev>
	 */
	protected $pogoj;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->poklic = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->pogoj = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the opomba
	 *
	 * @return string $opomba
	 */
	public function getOpomba() {
		return $this->opomba;
	}

	/**
	 * Sets the opomba
	 *
	 * @param string $opomba
	 * @return void
	 */
	public function setOpomba($opomba) {
		$this->opomba = $opomba;
	}

	/**
	 * Adds a PovezavePoklicev
	 *
	 * @param Tx_A3Ekt_Domain_Model_PovezavePoklicev $pogoj
	 * @return void
	 */
	public function addPogoj(Tx_A3Ekt_Domain_Model_PovezavePoklicev $pogoj) {
		$this->pogoj->attach($pogoj);
	}

	/**
	 * Removes a PovezavePoklicev
	 *
	 * @param Tx_A3Ekt_Domain_Model_PovezavePoklicev $pogojToRemove The PovezavePoklicev to be removed
	 * @return void
	 */
	public function removePogoj(Tx_A3Ekt_Domain_Model_PovezavePoklicev $pogojToRemove) {
		$this->pogoj->detach($pogojToRemove);
	}

	/**
	 * Returns the pogoj
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PovezavePoklicev> $pogoj
	 */
	public function getPogoj() {
		return $this->pogoj;
	}

	/**
	 * Sets the pogoj
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PovezavePoklicev> $pogoj
	 * @return void
	 */
	public function setPogoj(Tx_Extbase_Persistence_ObjectStorage $pogoj) {
		$this->pogoj = $pogoj;
	}

	/**
	 * Adds a Poklic
	 *
	 * @param Tx_A3Ekt_Domain_Model_Poklic $poklic
	 * @return void
	 */
	public function addPoklic(Tx_A3Ekt_Domain_Model_Poklic $poklic) {
		$this->poklic->attach($poklic);
	}

	/**
	 * Removes a Poklic
	 *
	 * @param Tx_A3Ekt_Domain_Model_Poklic $poklicToRemove The Poklic to be removed
	 * @return void
	 */
	public function removePoklic(Tx_A3Ekt_Domain_Model_Poklic $poklicToRemove) {
		$this->poklic->detach($poklicToRemove);
	}

	/**
	 * Returns the poklic
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_Poklic> $poklic
	 */
	public function getPoklic() {
		return $this->poklic;
	}

	/**
	 * Sets the poklic
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_Poklic> $poklic
	 * @return void
	 */
	public function setPoklic(Tx_Extbase_Persistence_ObjectStorage $poklic) {
		$this->poklic = $poklic;
	}

}
?>