<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Model_EKTPovezavePogoj extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * Opombe za vnos podatkov za tujce
	 *
	 * @var string
	 */
	protected $opomba;

	/**
	 * Pogoj
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PovezavePogojev>
	 */
	protected $pogoj;

	/**
	 * Dejavnost
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_Dejavnost>
	 */
	protected $dejavnost;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->pogoj = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->dejavnost = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the opomba
	 *
	 * @return string $opomba
	 */
	public function getOpomba() {
		return $this->opomba;
	}

	/**
	 * Sets the opomba
	 *
	 * @param string $opomba
	 * @return void
	 */
	public function setOpomba($opomba) {
		$this->opomba = $opomba;
	}

	/**
	 * Adds a PovezavePogojev
	 *
	 * @param Tx_A3Ekt_Domain_Model_PovezavePogojev $pogoj
	 * @return void
	 */
	public function addPogoj(Tx_A3Ekt_Domain_Model_PovezavePogojev $pogoj) {
		$this->pogoj->attach($pogoj);
	}

	/**
	 * Removes a PovezavePogojev
	 *
	 * @param Tx_A3Ekt_Domain_Model_PovezavePogojev $pogojToRemove The PovezavePogojev to be removed
	 * @return void
	 */
	public function removePogoj(Tx_A3Ekt_Domain_Model_PovezavePogojev $pogojToRemove) {
		$this->pogoj->detach($pogojToRemove);
	}

	/**
	 * Returns the pogoj
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PovezavePogojev> $pogoj
	 */
	public function getPogoj() {
		return $this->pogoj;
	}

	/**
	 * Sets the pogoj
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PovezavePogojev> $pogoj
	 * @return void
	 */
	public function setPogoj(Tx_Extbase_Persistence_ObjectStorage $pogoj) {
		$this->pogoj = $pogoj;
	}

	/**
	 * Adds a Dejavnost
	 *
	 * @param Tx_A3Ekt_Domain_Model_Dejavnost $dejavnost
	 * @return void
	 */
	public function addDejavnost(Tx_A3Ekt_Domain_Model_Dejavnost $dejavnost) {
		$this->dejavnost->attach($dejavnost);
	}

	/**
	 * Removes a Dejavnost
	 *
	 * @param Tx_A3Ekt_Domain_Model_Dejavnost $dejavnostToRemove The Dejavnost to be removed
	 * @return void
	 */
	public function removeDejavnost(Tx_A3Ekt_Domain_Model_Dejavnost $dejavnostToRemove) {
		$this->dejavnost->detach($dejavnostToRemove);
	}

	/**
	 * Returns the dejavnost
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_Dejavnost> $dejavnost
	 */
	public function getDejavnost() {
		return $this->dejavnost;
	}

	/**
	 * Sets the dejavnost
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_Dejavnost> $dejavnost
	 * @return void
	 */
	public function setDejavnost(Tx_Extbase_Persistence_ObjectStorage $dejavnost) {
		$this->dejavnost = $dejavnost;
	}

}
?>