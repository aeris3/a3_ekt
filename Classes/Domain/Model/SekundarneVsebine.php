<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Robert Feencek <rferencek@aeris3.si>, Aeris3 d.o.o.
 *  Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3 d.o.o.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_activa_nakupi
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Model_SekundarneVsebine extends Tx_Extbase_DomainObject_AbstractEntity {

    /**
     * @var string
     */
    protected $header;
    
    /**
     * @var string
     */
    protected $content;
    
    /**
     * @var string
     */
    protected $headerLayout;
    
    /**
     * @var string
     */
    protected $contentType;

    /**
     * Returns the header data
     *
     * @return string
     */
    public function getHeader() {
        return $this->header;
    }
 

    /**
     * Returns the content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }
    
    /**
     * Returns the content type
     *
     * @return string
     */
    public function getContentType() {
        return $this->contentType;
    }
}
?>