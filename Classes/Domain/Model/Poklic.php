<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Model_Poklic extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * Sekundarne vsebine
	 *
	 * @var string
	 */
	protected $sekundarneVsebine;

	/**
	 * Naziv poklica
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $naziv;

	/**
	 * Interna oznaka
	 *
	 * @var string
	 */
	protected $oznaka;

	/**
	 * Opis poklica
	 *
	 * @var string
	 */
	protected $opis;

	/**
	 * Ključne besede in sorodni pojmi
	 *
	 * @var string
	 */
	protected $kljucneBesede;

	/**
	 * Opombe za vnos podatkov za tujce
	 *
	 * @var string
	 */
	protected $opomba;

	/**
	 * Čezmejno priznavanje poklica
	 *
	 * @var integer
	 */
	protected $cezmejnoPriznavanjePoklica;

	/**
	 * Drugo
	 *
	 * @var string
	 */
	protected $cezmejnoPriznavanjeObrazlozitev;

	/**
	 * Zakonodajna organ
	 *
	 * @var Tx_A3Ekt_Domain_Model_Organ
	 */
	protected $zakonodajniOrgan;
    
    /**
	 * Zakonodajna organ
	 *
	 * @var Tx_A3Ekt_Domain_Model_Organ
	 */
	protected $vir;

	/**
	 * Kontakt pri zakonodajnem organu
	 *
	 * @var Tx_A3Ekt_Domain_Model_KontaktnaOseba
	 */
	protected $zakonodajniOrganKontakt;
	
	/**
	 * Sorodne povezave
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_SorodnePovezave>
	 */
	protected $sorodnePovezave;

	/**
	 * Pravne podlage
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PravnaPodlaga>
	 */
	protected $pravnePodlage;
	
	/**
	 * Čezmejno opravljanje poklica
	 *
	 * @var Tx_A3Ekt_Domain_Model_CezmejnoOpravljenjePoklica
	 */
	protected $prijavaSPostopkomPridobitveDovoljenja;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->sorodnePovezave = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->pravnePodlage = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the naziv
	 *
	 * @return string $naziv
	 */
	public function getNaziv() {
		return $this->naziv;
	}

	/**
	 * Sets the naziv
	 *
	 * @param string $naziv
	 * @return void
	 */
	public function setNaziv($naziv) {
		$this->naziv = $naziv;
	}

	/**
	 * Returns the oznaka
	 *
	 * @return string $oznaka
	 */
	public function getOznaka() {
		return $this->oznaka;
	}

	/**
	 * Sets the oznaka
	 *
	 * @param string $oznaka
	 * @return void
	 */
	public function setOznaka($oznaka) {
		$this->oznaka = $oznaka;
	}

	/**
	 * Returns the opis
	 *
	 * @return string $opis
	 */
	public function getOpis() {
		return $this->opis;
	}

	/**
	 * Sets the opis
	 *
	 * @param string $opis
	 * @return void
	 */
	public function setOpis($opis) {
		$this->opis = $opis;
	}

	/**
	 * Returns the kljucneBesede
	 *
	 * @return string $kljucneBesede
	 */
	public function getKljucneBesede() {
		return $this->kljucneBesede;
	}

	/**
	 * Sets the kljucneBesede
	 *
	 * @param string $kljucneBesede
	 * @return void
	 */
	public function setKljucneBesede($kljucneBesede) {
		$this->kljucneBesede = $kljucneBesede;
	}

	/**
	 * Returns the opomba
	 *
	 * @return string $opomba
	 */
	public function getOpomba() {
		return $this->opomba;
	}

	/**
	 * Sets the opomba
	 *
	 * @param string $opomba
	 * @return void
	 */
	public function setOpomba($opomba) {
		$this->opomba = $opomba;
	}

	/**
	 * Returns the zakonodajniOrganKontakt
	 *
	 * @return Tx_A3Ekt_Domain_Model_KontaktnaOseba $zakonodajniOrganKontakt
	 */
	public function getZakonodajniOrganKontakt() {
		return $this->zakonodajniOrganKontakt;
	}

	/**
	 * Sets the zakonodajniOrganKontakt
	 *
	 * @param Tx_A3Ekt_Domain_Model_KontaktnaOseba $zakonodajniOrganKontakt
	 * @return void
	 */
	public function setZakonodajniOrganKontakt(Tx_A3Ekt_Domain_Model_KontaktnaOseba $zakonodajniOrganKontakt) {
		$this->zakonodajniOrganKontakt = $zakonodajniOrganKontakt;
	}

	/**
	 * Adds a SorodnePovezave
	 *
	 * @param Tx_A3Ekt_Domain_Model_SorodnePovezave $sorodnePovezave
	 * @return void
	 */
	public function addSorodnePovezave(Tx_A3Ekt_Domain_Model_SorodnePovezave $sorodnePovezave) {
		$this->sorodnePovezave->attach($sorodnePovezave);
	}

	/**
	 * Removes a SorodnePovezave
	 *
	 * @param Tx_A3Ekt_Domain_Model_SorodnePovezave $sorodnePovezaveToRemove The SorodnePovezave to be removed
	 * @return void
	 */
	public function removeSorodnePovezave(Tx_A3Ekt_Domain_Model_SorodnePovezave $sorodnePovezaveToRemove) {
		$this->sorodnePovezave->detach($sorodnePovezaveToRemove);
	}

	/**
	 * Returns the sorodnePovezave
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_SorodnePovezave> $sorodnePovezave
	 */
	public function getSorodnePovezave() {
		return $this->sorodnePovezave;
	}

	/**
	 * Sets the sorodnePovezave
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_SorodnePovezave> $sorodnePovezave
	 * @return void
	 */
	public function setSorodnePovezave(Tx_Extbase_Persistence_ObjectStorage $sorodnePovezave) {
		$this->sorodnePovezave = $sorodnePovezave;
	}

	/**
	 * Adds a PravnaPodlaga
	 *
	 * @param Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnePodlage
	 * @return void
	 */
	public function addPravnePodlage(Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnePodlage) {
		$this->pravnePodlage->attach($pravnePodlage);
	}

	/**
	 * Removes a PravnaPodlaga
	 *
	 * @param Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnePodlageToRemove The PravnaPodlaga to be removed
	 * @return void
	 */
	public function removePravnePodlage(Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnePodlageToRemove) {
		$this->pravnePodlage->detach($pravnePodlageToRemove);
	}

	/**
	 * Returns the pravnePodlage
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PravnaPodlaga> $pravnePodlage
	 */
	public function getPravnePodlage() {
		return $this->pravnePodlage;
	}

	/**
	 * Sets the pravnePodlage
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PravnaPodlaga> $pravnePodlage
	 * @return void
	 */
	public function setPravnePodlage(Tx_Extbase_Persistence_ObjectStorage $pravnePodlage) {
		$this->pravnePodlage = $pravnePodlage;
	}

	/**
	 * Returns the zakonodajniOrgan
	 *
	 * @return Tx_A3Ekt_Domain_Model_Organ $zakonodajniOrgan
	 */
	public function getZakonodajniOrgan() {
		return $this->zakonodajniOrgan;
	}

	/**
	 * Sets the zakonodajniOrgan
	 *
	 * @param Tx_A3Ekt_Domain_Model_Organ $zakonodajniOrgan
	 * @return void
	 */
	public function setZakonodajniOrgan(Tx_A3Ekt_Domain_Model_Organ $zakonodajniOrgan) {
		$this->zakonodajniOrgan = $zakonodajniOrgan;
	}
    
    /**
	 * Returns the zakonodajniOrgan
	 *
	 * @return Tx_A3Ekt_Domain_Model_Organ $vir
	 */
	public function getVir() {
		return $this->vir;
	}

	/**
	 * Returns the povezavePoklicev
	 *
	 * @return Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository<Tx_A3Ekt_Domain_Model_Pove> $ekt_dejavnost
	 */
	public function getEKTPovezavePoklicev() {
		        $ektPovezavePoklicev = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EKTPovezavePoklicevRepository')->getEKTPovezavePoklicevForPoklic($this)->getFirst();
		        if (is_object($ektPovezavePoklicev) ) {
		            $povezavePoklicev = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository')->getPovezavePoklicevForEKTPovezavePoklicev($ektPovezavePoklicev);
		            return $povezavePoklicev;
		        }
		        return null;
	}

	/**
	 * Returns the sekundarneVsebine
	 *
	 * @return string $sekundarneVsebine
	 */
	public function getSekundarneVsebine() {
		        return t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_SekundarneVsebineRepository')->getSekundarneVsebine($this->sekundarneVsebine);
	}
	
	/**
	 * Returns the cezmejnoPriznavanjePoklica
	 *
	 * @return integer $cezmejnoPriznavanjePoklica
	 */
	public function getCezmejnoPriznavanjePoklica() {
		return $this->cezmejnoPriznavanjePoklica;
	}

	/**
	 * Sets the cezmejnoPriznavanjePoklica
	 *
	 * @param integer $cezmejnoPriznavanjePoklica
	 * @return void
	 */
	public function setCezmejnoPriznavanjePoklica($cezmejnoPriznavanjePoklica) {
		$this->cezmejnoPriznavanjePoklica = $cezmejnoPriznavanjePoklica;
	}

	/**
	 * Returns the cezmejnoPriznavanjeObrazlozitev
	 *
	 * @return string $cezmejnoPriznavanjeObrazlozitev
	 */
	public function getCezmejnoPriznavanjeObrazlozitev() {
		return $this->cezmejnoPriznavanjeObrazlozitev;
	}

	/**
	 * Sets the cezmejnoPriznavanjeObrazlozitev
	 *
	 * @param string $cezmejnoPriznavanjeObrazlozitev
	 * @return void
	 */
	public function setCezmejnoPriznavanjeObrazlozitev($cezmejnoPriznavanjeObrazlozitev) {
		$this->cezmejnoPriznavanjeObrazlozitev = $cezmejnoPriznavanjeObrazlozitev;
	}
	
	/**
	 * Returns the vrstaOrgana
	 *
	 * @return Tx_A3Ekt_Domain_Model_CezmejnoOpravljenjePoklica $prijavaSPostopkomPridobitveDovoljenja
	 */
	public function getPrijavaSPostopkomPridobitveDovoljenja() {
		return $this->prijavaSPostopkomPridobitveDovoljenja;
	}

	/**
	 * Sets the prijavaSPostopkomPridobitveDovoljenja
	 *
	 * @param Tx_A3Ekt_Domain_Model_CezmejnoOpravljenjePoklica $prijavaSPostopkomPridobitveDovoljenja
	 * @return void
	 */
	public function setPrijavaSPostopkomPridobitveDovoljenja(Tx_A3Ekt_Domain_Model_CezmejnoOpravljenjePoklica $prijavaSPostopkomPridobitveDovoljenja) {
		$this->prijavaSPostopkomPridobitveDovoljenja = $prijavaSPostopkomPridobitveDovoljenja;
	}

}
?>