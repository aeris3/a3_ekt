<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Model_Sektor extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * Naziv
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $naziv;

	/**
	 * Opis
	 *
	 * @var string
	 */
	protected $opis;

	/**
	 * SKD šifra širšega področja
	 *
	 * @var Tx_A3Ekt_Domain_Model_SKDDejavnost
	 */
	protected $skd;
	
	/**
	 * Stran
	 *
	 * @var string
	 */
	protected $stran;
    
    /**
	 * Sekundarne vsebine
	 *
	 * @var string
	 */
	protected $sekundarneVsebine;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {

	}

	/**
	 * Returns the naziv
	 *
	 * @return string $naziv
	 */
	public function getNaziv() {
		return $this->naziv;
	}

	/**
	 * Sets the naziv
	 *
	 * @param string $naziv
	 * @return void
	 */
	public function setNaziv($naziv) {
		$this->naziv = $naziv;
	}

	/**
	 * Returns the opis
	 *
	 * @return string $opis
	 */
	public function getOpis() {
		return $this->opis;
	}

	/**
	 * Sets the opis
	 *
	 * @param string $opis
	 * @return void
	 */
	public function setOpis($opis) {
		$this->opis = $opis;
	}

	/**
	 * Returns the skd
	 *
	 * @return Tx_A3Ekt_Domain_Model_SKDDejavnost $skd
	 */
	public function getSkd() {
		return $this->skd;
	}

	/**
	 * Sets the skd
	 *
	 * @param Tx_A3Ekt_Domain_Model_SKDDejavnost $skd
	 * @return void
	 */
	public function setSkd(Tx_A3Ekt_Domain_Model_SKDDejavnost $skd) {
		$this->skd = $skd;
	}
	
	/**
	 * Returns the url of page
	 *
	 * @return string $stran
	 */
	public function getStran() {
		return $this->stran;
	}

	/**
	 * Sets the url of page
	 *
	 * @param string $stran
	 * @return void
	 */
	public function setStran($stran) {
		$this->stran = $stran;
	}
    
    /**
	 * Returns the sekundarneVsebine
	 *
	 * @return string $sekundarneVsebine
	 */
	public function getSekundarneVsebine() {
        return t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_SekundarneVsebineRepository')->getSekundarneVsebine($this->sekundarneVsebine);
	}

}
?>