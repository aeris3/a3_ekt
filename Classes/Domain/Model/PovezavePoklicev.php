<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Model_PovezavePoklicev extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * Kategorija pogoja
	 *
	 * @var integer
	 * @validate NotEmpty
	 */
	protected $tip;

	/**
	 * Dovoljenje
	 *
	 * @var Tx_A3Ekt_Domain_Model_Dovoljenje
	 */
	protected $dovoljenje;

	/**
	 * Čezmejno dovoljenje
	 *
	 * @var Tx_A3Ekt_Domain_Model_CezmejnoOpravljenjePoklica
	 */
	protected $cezmejnoDovoljenje;

	/**
	 * drugiPogoj
	 *
	 * @var Tx_A3Ekt_Domain_Model_DrugiPogoj
	 */
	protected $drugiPogoj;

	/**
	 * Pogoj
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PovezavePoklicev>
	 */
	protected $pogoj;

	/**
	 * Dovoljenje v tujini
	 *
	 * @var Tx_A3Ekt_Domain_Model_DovoljenjeVTujini
	 */
	protected $dovoljenjeVTujini;

	/**
	 * Poklicna kvalifikacija
	 *
	 * @var Tx_A3Ekt_Domain_Model_Poklic
	 */
	protected $poklic;
	
	/**
	 * EKT PovezavePoklicev
	 *
	 * @var Tx_A3Ekt_Domain_Model_EKTPovezavePoklicev
	 */
	protected $ektpovezavepoklicev;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Returns the dovoljenje
	 *
	 * @return Tx_A3Ekt_Domain_Model_Dovoljenje $dovoljenje
	 */
	public function getDovoljenje() {
		return $this->dovoljenje;
	}

	/**
	 * Sets the dovoljenje
	 *
	 * @param Tx_A3Ekt_Domain_Model_Dovoljenje $dovoljenje
	 * @return void
	 */
	public function setDovoljenje(Tx_A3Ekt_Domain_Model_Dovoljenje $dovoljenje) {
		$this->dovoljenje = $dovoljenje;
	}

	/**
	 * Returns the cezmejnoDovoljenje
	 *
	 * @return Tx_A3Ekt_Domain_Model_CezmejnoOpravljenjePoklica $cezmejnoDovoljenje
	 */
	public function getCezmejnoDovoljenje() {
		return $this->cezmejnoDovoljenje;
	}

	/**
	 * Sets the cezmejnoDovoljenje
	 *
	 * @param Tx_A3Ekt_Domain_Model_CezmejnoOpravljenjePoklica $cezmejnoDovoljenje
	 * @return void
	 */
	public function setCezmejnoDovoljenje(Tx_A3Ekt_Domain_Model_CezmejnoOpravljenjePoklica $cezmejnoDovoljenje) {
		$this->cezmejnoDovoljenje = $cezmejnoDovoljenje;
	}

	/**
	 * Returns the drugiPogoj
	 *
	 * @return Tx_A3Ekt_Domain_Model_DrugiPogoj $drugiPogoj
	 */
	public function getDrugiPogoj() {
		return $this->drugiPogoj;
	}

	/**
	 * Sets the drugiPogoj
	 *
	 * @param Tx_A3Ekt_Domain_Model_DrugiPogoj $drugiPogoj
	 * @return void
	 */
	public function setDrugiPogoj(Tx_A3Ekt_Domain_Model_DrugiPogoj $drugiPogoj) {
		$this->drugiPogoj = $drugiPogoj;
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->pogoj = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Adds a PovezavePoklicev
	 *
	 * @param Tx_A3Ekt_Domain_Model_PovezavePoklicev $pogoj
	 * @return void
	 */
	public function addPogoj(Tx_A3Ekt_Domain_Model_PovezavePoklicev $pogoj) {
		$this->pogoj->attach($pogoj);
	}

	/**
	 * Removes a PovezavePoklicev
	 *
	 * @param Tx_A3Ekt_Domain_Model_PovezavePoklicev $pogojToRemove The PovezavePoklicev to be removed
	 * @return void
	 */
	public function removePogoj(Tx_A3Ekt_Domain_Model_PovezavePoklicev $pogojToRemove) {
		$this->pogoj->detach($pogojToRemove);
	}

	/**
	 * Returns the pogoj
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PovezavePoklicev> $pogoj
	 */
	public function getPogoj() {
		return $this->pogoj;
	}

	/**
	 * Sets the pogoj
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PovezavePoklicev> $pogoj
	 * @return void
	 */
	public function setPogoj(Tx_Extbase_Persistence_ObjectStorage $pogoj) {
		$this->pogoj = $pogoj;
	}

	/**
	 * Returns the tip
	 *
	 * @return integer $tip
	 */
	public function getTip() {
		return $this->tip;
	}

	/**
	 * Sets the tip
	 *
	 * @param integer $tip
	 * @return void
	 */
	public function setTip($tip) {
		$this->tip = $tip;
	}

	/**
	 * Returns the PovezavePoklicev
	 *
	 * @return Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository<Tx_A3Ekt_Domain_Model_PovezavePoklicev> $povezavePoklicev
	 */
	public function getPovezavePoklicevForEKTPovezavePoklicev() {
		        $povezavePoklicev = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository')->getPovezavePoklicevForPovezavePoklicev($this);
		        return $povezavePoklicev;
	}

	/**
	 * Returns the dovoljenjeVTujini
	 *
	 * @return Tx_A3Ekt_Domain_Model_DovoljenjeVTujini $dovoljenjeVTujini
	 */
	public function getDovoljenjeVTujini() {
		return $this->dovoljenjeVTujini;
	}
	
	/**
	 * Sets the dovoljenjeVTujini
	 *
	 * @param Tx_A3Ekt_Domain_Model_DovoljenjeVTujini $dovoljenjeVTujini
	 * @return void
	 */
	public function setDovoljenjeVTujini(Tx_A3Ekt_Domain_Model_DovoljenjeVTujini $dovoljenjeVTujini) {
		$this->dovoljenjeVTujini = $dovoljenjeVTujini;
	}

	/**
	 * Returns the poklic
	 *
	 * @return Tx_A3Ekt_Domain_Model_Poklic $poklic
	 */
	public function getPoklic() {
		return $this->poklic;
	}

	/**
	 * Sets the poklic
	 *
	 * @param Tx_A3Ekt_Domain_Model_Poklic $poklic
	 * @return void
	 */
	public function setPoklic(Tx_A3Ekt_Domain_Model_Poklic $poklic) {
		$this->poklic = $poklic;
	}
	
	/**
	 * Returns the eKTPovezavePoklicev
	 *
	 * @return Tx_A3Ekt_Domain_Model_EKTPovezavePoklicev $ektpovezavezpoklicev
	 */
	public function getEKTPovezavePoklicev() {
		return $this->ektpovezavepoklicev;
	}
	
	/**
	 * Sets the eKTPovezavePoklicev
	 *
	 * @param Tx_A3Ekt_Domain_Model_EKTPovezavePoklicev $ektpovezavezpoklicev
	 * @return void
	 */
	public function setEKTPovezavePoklicev($ektpovezavezpoklicev) {
		$this->ektpovezavepoklicev = $ektpovezavepoklicev;
	}

}
?>