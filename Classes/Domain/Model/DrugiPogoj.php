<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Model_DrugiPogoj extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * Navedite naziv pogoja
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $naziv;

	/**
	 * Interna oznaka
	 *
	 * @var string
	 */
	protected $oznaka;

	/**
	 * Opis pogoja
	 *
	 * @var string
	 */
	protected $opis;

	/**
	 * Dokazilo o izpolnjevanju pogoja
	 *
	 * @var string
	 */
	protected $dokazilo;

	/**
	 * Opomba
	 *
	 * @var string
	 */
	protected $opomba;

	/**
	 * Zakonodajna organ
	 *
	 * @var Tx_A3Ekt_Domain_Model_Organ
	 */
	protected $zakonodajniOrgan;

	/**
	 * Kontakt pri zakonodajnem organu
	 *
	 * @var Tx_A3Ekt_Domain_Model_KontaktnaOseba
	 */
	protected $zakonodajniOrganKontakt;

	/**
	 * Pravna podlaga
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PravnaPodlaga>
	 */
	protected $pravnePodlage;

	/**
	 * Pristojna organ
	 *
	 * @var Tx_A3Ekt_Domain_Model_Organ
	 */
	protected $pristojniOrgan;

	/**
	 * Kontakt pri pristojnem organu
	 *
	 * @var Tx_A3Ekt_Domain_Model_KontaktnaOseba
	 */
	protected $pristojniOrganKontakt;

	/**
	 * Sorodne povezave
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_SorodnePovezave>
	 */
	protected $sorodnePovezave;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->pravnePodlage = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->sorodnePovezave = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the naziv
	 *
	 * @return string $naziv
	 */
	public function getNaziv() {
		return $this->naziv;
	}

	/**
	 * Sets the naziv
	 *
	 * @param string $naziv
	 * @return void
	 */
	public function setNaziv($naziv) {
		$this->naziv = $naziv;
	}

	/**
	 * Returns the oznaka
	 *
	 * @return string $oznaka
	 */
	public function getOznaka() {
		return $this->oznaka;
	}

	/**
	 * Sets the oznaka
	 *
	 * @param string $oznaka
	 * @return void
	 */
	public function setOznaka($oznaka) {
		$this->oznaka = $oznaka;
	}

	/**
	 * Returns the opis
	 *
	 * @return string $opis
	 */
	public function getOpis() {
		return $this->opis;
	}

	/**
	 * Sets the opis
	 *
	 * @param string $opis
	 * @return void
	 */
	public function setOpis($opis) {
		$this->opis = $opis;
	}

	/**
	 * Returns the dokazilo
	 *
	 * @return string $dokazilo
	 */
	public function getDokazilo() {
		return $this->dokazilo;
	}

	/**
	 * Sets the dokazilo
	 *
	 * @param string $dokazilo
	 * @return void
	 */
	public function setDokazilo($dokazilo) {
		$this->dokazilo = $dokazilo;
	}

	/**
	 * Returns the opomba
	 *
	 * @return string $opomba
	 */
	public function getOpomba() {
		return $this->opomba;
	}

	/**
	 * Sets the opomba
	 *
	 * @param string $opomba
	 * @return void
	 */
	public function setOpomba($opomba) {
		$this->opomba = $opomba;
	}

	/**
	 * Returns the zakonodajniOrganKontakt
	 *
	 * @return Tx_A3Ekt_Domain_Model_KontaktnaOseba $zakonodajniOrganKontakt
	 */
	public function getZakonodajniOrganKontakt() {
		return $this->zakonodajniOrganKontakt;
	}

	/**
	 * Sets the zakonodajniOrganKontakt
	 *
	 * @param Tx_A3Ekt_Domain_Model_KontaktnaOseba $zakonodajniOrganKontakt
	 * @return void
	 */
	public function setZakonodajniOrganKontakt(Tx_A3Ekt_Domain_Model_KontaktnaOseba $zakonodajniOrganKontakt) {
		$this->zakonodajniOrganKontakt = $zakonodajniOrganKontakt;
	}

	/**
	 * Adds a PravnaPodlaga
	 *
	 * @param Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnePodlage
	 * @return void
	 */
	public function addPravnePodlage(Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnePodlage) {
		$this->pravnePodlage->attach($pravnePodlage);
	}

	/**
	 * Removes a PravnaPodlaga
	 *
	 * @param Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnePodlageToRemove The PravnaPodlaga to be removed
	 * @return void
	 */
	public function removePravnePodlage(Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnePodlageToRemove) {
		$this->pravnePodlage->detach($pravnePodlageToRemove);
	}

	/**
	 * Returns the pravnePodlage
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PravnaPodlaga> $pravnePodlage
	 */
	public function getPravnePodlage() {
		return $this->pravnePodlage;
	}

	/**
	 * Sets the pravnePodlage
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PravnaPodlaga> $pravnePodlage
	 * @return void
	 */
	public function setPravnePodlage(Tx_Extbase_Persistence_ObjectStorage $pravnePodlage) {
		$this->pravnePodlage = $pravnePodlage;
	}

	/**
	 * Returns the pristojniOrganKontakt
	 *
	 * @return Tx_A3Ekt_Domain_Model_KontaktnaOseba $pristojniOrganKontakt
	 */
	public function getPristojniOrganKontakt() {
		return $this->pristojniOrganKontakt;
	}

	/**
	 * Sets the pristojniOrganKontakt
	 *
	 * @param Tx_A3Ekt_Domain_Model_KontaktnaOseba $pristojniOrganKontakt
	 * @return void
	 */
	public function setPristojniOrganKontakt(Tx_A3Ekt_Domain_Model_KontaktnaOseba $pristojniOrganKontakt) {
		$this->pristojniOrganKontakt = $pristojniOrganKontakt;
	}

	/**
	 * Adds a SorodnePovezave
	 *
	 * @param Tx_A3Ekt_Domain_Model_SorodnePovezave $sorodnePovezave
	 * @return void
	 */
	public function addSorodnePovezave(Tx_A3Ekt_Domain_Model_SorodnePovezave $sorodnePovezave) {
		$this->sorodnePovezave->attach($sorodnePovezave);
	}

	/**
	 * Removes a SorodnePovezave
	 *
	 * @param Tx_A3Ekt_Domain_Model_SorodnePovezave $sorodnePovezaveToRemove The SorodnePovezave to be removed
	 * @return void
	 */
	public function removeSorodnePovezave(Tx_A3Ekt_Domain_Model_SorodnePovezave $sorodnePovezaveToRemove) {
		$this->sorodnePovezave->detach($sorodnePovezaveToRemove);
	}

	/**
	 * Returns the sorodnePovezave
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_SorodnePovezave> $sorodnePovezave
	 */
	public function getSorodnePovezave() {
		return $this->sorodnePovezave;
	}

	/**
	 * Sets the sorodnePovezave
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_SorodnePovezave> $sorodnePovezave
	 * @return void
	 */
	public function setSorodnePovezave(Tx_Extbase_Persistence_ObjectStorage $sorodnePovezave) {
		$this->sorodnePovezave = $sorodnePovezave;
	}

	/**
	 * Returns the zakonodajniOrgan
	 *
	 * @return Tx_A3Ekt_Domain_Model_Organ $zakonodajniOrgan
	 */
	public function getZakonodajniOrgan() {
		return $this->zakonodajniOrgan;
	}

	/**
	 * Sets the zakonodajniOrgan
	 *
	 * @param Tx_A3Ekt_Domain_Model_Organ $zakonodajniOrgan
	 * @return void
	 */
	public function setZakonodajniOrgan(Tx_A3Ekt_Domain_Model_Organ $zakonodajniOrgan) {
		$this->zakonodajniOrgan = $zakonodajniOrgan;
	}
	
	/**
	 * Returns the pristojniOrgan
	 *
	 * @return Tx_A3Ekt_Domain_Model_Organ $pristojniOrgan
	 */
	public function getPristojniOrgan() {
		return $this->pristojniOrgan;
	}

	/**
	 * Sets the pristojniOrgan
	 *
	 * @param Tx_A3Ekt_Domain_Model_Organ $pristojniOrgan
	 * @return void
	 */
	public function setPristojniOrgan(Tx_A3Ekt_Domain_Model_Organ $pristojniOrgan) {
		$this->pristojniOrgan = $pristojniOrgan;
	}

}
?>