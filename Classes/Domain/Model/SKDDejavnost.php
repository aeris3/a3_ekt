<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Model_SKDDejavnost extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * Deskriptor
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $deskriptor;

	/**
	 * Šifra kategorije
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $sifraKategorije;

	/**
	 * Šifra starša
	 *
	 * @var Tx_A3Ekt_Domain_Model_SKDDejavnost
	 */
	protected $parentSKD;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {

	}

	/**
	 * Returns the deskriptor
	 *
	 * @return string $deskriptor
	 */
	public function getDeskriptor() {
		return $this->deskriptor;
	}

	/**
	 * Sets the deskriptor
	 *
	 * @param string $deskriptor
	 * @return void
	 */
	public function setDeskriptor($deskriptor) {
		$this->deskriptor = $deskriptor;
	}

	/**
	 * Returns the sifraKategorije
	 *
	 * @return string $sifraKategorije
	 */
	public function getSifraKategorije() {
		return $this->sifraKategorije;
	}

	/**
	 * Sets the sifraKategorije
	 *
	 * @param string $sifraKategorije
	 * @return void
	 */
	public function setSifraKategorije($sifraKategorije) {
		$this->sifraKategorije = $sifraKategorije;
	}

	/**
	 * Returns the parentSKD
	 *
	 * @return Tx_A3Ekt_Domain_Model_SKDDejavnost $parentSKD
	 */
	public function getParentSKD() {
		return $this->parentSKD;
	}

	/**
	 * Sets the parentSKD
	 *
	 * @param Tx_A3Ekt_Domain_Model_SKDDejavnost $parentSKD
	 * @return void
	 */
	public function setParentSKD(Tx_A3Ekt_Domain_Model_SKDDejavnost $parentSKD) {
		$this->parentSKD = $parentSKD;
	}

}
?>