<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Model_DovoljenjeVTujini extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * Naziv dovoljenja
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $nazivDovoljenja;

	/**
	 * Način priznavanja
	 *
	 * @var integer
	 */
	protected $nacinPriznavanja;

	/**
	 * Označite v primeru obveznega članstva
	 *
	 * @var integer
	 */
	protected $obveznoClanstvo;

	/**
	 * Označite v primeru obnove dovoljenja
	 *
	 * @var integer
	 */
	protected $obnova;

	/**
	 * Termin obnove
	 *
	 * @var string
	 */
	protected $terminObnove;

	/**
	 * Opišite postopek obnove dovoljenja
	 *
	 * @var string
	 */
	protected $opisObnove;
        
        /**
	 * Opis Dovoljenja v tujini
	 *
	 * @var string
	 */
	protected $opis;

	/**
	 * Drugo
	 *
	 * @var string
	 */
	protected $drugo;
    
    /**
	 * Navedite naziv pravnega sredstva (npr. pritožba / upravni spor)
	 *
	 * @var integer
	 */
	protected $pravnoSredstvo;
    
    /**
	 * Molk organa
	 *
	 * @var string
	 */
	protected $molkOrgana;

	/**
	 * Pristojna organ
	 *
	 * @var Tx_A3Ekt_Domain_Model_Organ
	 */
	protected $pristojniOrgan;

	/**
	 * Kontakt pri pristojnem organu
	 *
	 * @var Tx_A3Ekt_Domain_Model_KontaktnaOseba
	 */
	protected $pristojniOrganKontakt;

	/**
	 * Vloge
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_Vloga>
	 */
	protected $vloge;

	/**
	 * Priloge k vlogi
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_VlogaPriloga>
	 */
	protected $prilogeKVlogi;

	/**
	 * Stroški postopka
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_StroskiPostopka>
	 */
	protected $stroskiPostopka;

	/**
	 * Koraki postopka
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_KorakiPostopka>
	 */
	protected $korakiPostopka;

	/**
	 * Pritožbeni organ
	 *
	 * @var Tx_A3Ekt_Domain_Model_Organ
	 */
	protected $pritozbeniOrgan;

	/**
	 * Pravne podlage tujina
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PravnaPodlaga>
	 */
	protected $pravnePodlageTujina;

	/**
	 * Returns the nazivDovoljenja
	 *
	 * @return string $nazivDovoljenja
	 */
	public function getNazivDovoljenja() {
		return $this->nazivDovoljenja;
	}

	/**
	 * Sets the nazivDovoljenja
	 *
	 * @param string $nazivDovoljenja
	 * @return void
	 */
	public function setNazivDovoljenja($nazivDovoljenja) {
		$this->nazivDovoljenja = $nazivDovoljenja;
	}

	/**
	 * Returns the nacinPriznavanja
	 *
	 * @return integer $nacinPriznavanja
	 */
	public function getNacinPriznavanja() {
		return $this->nacinPriznavanja;
	}

	/**
	 * Sets the nacinPriznavanja
	 *
	 * @param integer $nacinPriznavanja
	 * @return void
	 */
	public function setNacinPriznavanja($nacinPriznavanja) {
		$this->nacinPriznavanja = $nacinPriznavanja;
	}

	/**
	 * Returns the obveznoClanstvo
	 *
	 * @return integer $obveznoClanstvo
	 */
	public function getObveznoClanstvo() {
		return $this->obveznoClanstvo;
	}

	/**
	 * Sets the obveznoClanstvo
	 *
	 * @param integer $obveznoClanstvo
	 * @return void
	 */
	public function setObveznoClanstvo($obveznoClanstvo) {
		$this->obveznoClanstvo = $obveznoClanstvo;
	}

	/**
	 * Returns the obnova
	 *
	 * @return integer $obnova
	 */
	public function getObnova() {
		return $this->obnova;
	}

	/**
	 * Sets the obnova
	 *
	 * @param integer $obnova
	 * @return void
	 */
	public function setObnova($obnova) {
		$this->obnova = $obnova;
	}
        
        /**
	 * Returns the opis
	 *
	 * @return string $opis
	 */
	public function getOpis() {
		return $this->opis;
	}

	/**
	 * Sets the opis
	 *
	 * @param string $opis
	 * @return void
	 */
	public function setOpis($opis) {
		$this->opis = $opis;
	}

	/**
	 * Returns the terminObnove
	 *
	 * @return string $terminObnove
	 */
	public function getTerminObnove() {
		return $this->terminObnove;
	}

	/**
	 * Sets the terminObnove
	 *
	 * @param string $terminObnove
	 * @return void
	 */
	public function setTerminObnove($terminObnove) {
		$this->terminObnove = $terminObnove;
	}

	/**
	 * Returns the opisObnove
	 *
	 * @return string $opisObnove
	 */
	public function getOpisObnove() {
		return $this->opisObnove;
	}

	/**
	 * Sets the opisObnove
	 *
	 * @param string $opisObnove
	 * @return void
	 */
	public function setOpisObnove($opisObnove) {
		$this->opisObnove = $opisObnove;
	}

	/**
	 * Returns the drugo
	 *
	 * @return string $drugo
	 */
	public function getDrugo() {
		return $this->drugo;
	}

	/**
	 * Sets the drugo
	 *
	 * @param string $drugo
	 * @return void
	 */
	public function setDrugo($drugo) {
		$this->drugo = $drugo;
	}
    
    /**
	 * Returns the pravnoSredstvo
	 *
	 * @return integer $pravnoSredstvo
	 */
	public function getPravnoSredstvo() {
		return $this->pravnoSredstvo;
	}
    
    /**
	 * Returns the molkOrgana
	 *
	 * @return string $molkOrgana
	 */
	public function getMolkOrgana() {
		return $this->molkOrgana;
	}

	/**
	 * Returns the pristojniOrgan
	 *
	 * @return Tx_A3Ekt_Domain_Model_Organ pristojniOrgan
	 */
	public function getPristojniOrgan() {
		return $this->pristojniOrgan;
	}

	/**
	 * Sets the pristojniOrgan
	 *
	 * @param Tx_A3Ekt_Domain_Model_Organ $pristojniOrgan
	 * @return Tx_A3Ekt_Domain_Model_Organ pristojniOrgan
	 */
	public function setPristojniOrgan($pristojniOrgan) {
		$this->pristojniOrgan = $pristojniOrgan;
	}

	/**
	 * Returns the pristojniOrganKontakt
	 *
	 * @return Tx_A3Ekt_Domain_Model_KontaktnaOseba pristojniOrganKontakt
	 */
	public function getPristojniOrganKontakt() {
		return $this->pristojniOrganKontakt;
	}

	/**
	 * Sets the pristojniOrganKontakt
	 *
	 * @param Tx_A3Ekt_Domain_Model_KontaktnaOseba $pristojniOrganKontakt
	 * @return Tx_A3Ekt_Domain_Model_KontaktnaOseba pristojniOrganKontakt
	 */
	public function setPristojniOrganKontakt($pristojniOrganKontakt) {
		$this->pristojniOrganKontakt = $pristojniOrganKontakt;
	}

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->vloge = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->prilogeKVlogi = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->stroskiPostopka = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->korakiPostopka = new Tx_Extbase_Persistence_ObjectStorage();
		
		$this->pravnePodlageTujina = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Adds a Vloga
	 *
	 * @param Tx_A3Ekt_Domain_Model_Vloga $vloge
	 * @return void
	 */
	public function addVloge(Tx_A3Ekt_Domain_Model_Vloga $vloge) {
		$this->vloge->attach($vloge);
	}

	/**
	 * Removes a Vloga
	 *
	 * @param Tx_A3Ekt_Domain_Model_Vloga $vlogeToRemove The Vloga to be removed
	 * @return void
	 */
	public function removeVloge(Tx_A3Ekt_Domain_Model_Vloga $vlogeToRemove) {
		$this->vloge->detach($vlogeToRemove);
	}

	/**
	 * Returns the vloge
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_Vloga> $vloge
	 */
	public function getVloge() {
		return $this->vloge;
	}

	/**
	 * Sets the vloge
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_Vloga> $vloge
	 * @return void
	 */
	public function setVloge(Tx_Extbase_Persistence_ObjectStorage $vloge) {
		$this->vloge = $vloge;
	}

	/**
	 * Adds a VlogaPriloga
	 *
	 * @param Tx_A3Ekt_Domain_Model_VlogaPriloga $prilogeKVlogi
	 * @return void
	 */
	public function addPrilogeKVlogi(Tx_A3Ekt_Domain_Model_VlogaPriloga $prilogeKVlogi) {
		$this->prilogeKVlogi->attach($prilogeKVlogi);
	}

	/**
	 * Removes a VlogaPriloga
	 *
	 * @param Tx_A3Ekt_Domain_Model_VlogaPriloga $prilogeKVlogiToRemove The VlogaPriloga to be removed
	 * @return void
	 */
	public function removePrilogeKVlogi(Tx_A3Ekt_Domain_Model_VlogaPriloga $prilogeKVlogiToRemove) {
		$this->prilogeKVlogi->detach($prilogeKVlogiToRemove);
	}

	/**
	 * Returns the prilogeKVlogi
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_VlogaPriloga> $prilogeKVlogi
	 */
	public function getPrilogeKVlogi() {
		return $this->prilogeKVlogi;
	}

	/**
	 * Sets the prilogeKVlogi
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_VlogaPriloga> $prilogeKVlogi
	 * @return void
	 */
	public function setPrilogeKVlogi(Tx_Extbase_Persistence_ObjectStorage $prilogeKVlogi) {
		$this->prilogeKVlogi = $prilogeKVlogi;
	}

	/**
	 * Adds a StroskiPostopka
	 *
	 * @param Tx_A3Ekt_Domain_Model_StroskiPostopka $stroskiPostopka
	 * @return void
	 */
	public function addStroskiPostopka(Tx_A3Ekt_Domain_Model_StroskiPostopka $stroskiPostopka) {
		$this->stroskiPostopka->attach($stroskiPostopka);
	}

	/**
	 * Removes a StroskiPostopka
	 *
	 * @param Tx_A3Ekt_Domain_Model_StroskiPostopka $stroskiPostopkaToRemove The StroskiPostopka to be removed
	 * @return void
	 */
	public function removeStroskiPostopka(Tx_A3Ekt_Domain_Model_StroskiPostopka $stroskiPostopkaToRemove) {
		$this->stroskiPostopka->detach($stroskiPostopkaToRemove);
	}

	/**
	 * Returns the stroskiPostopka
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_StroskiPostopka> $stroskiPostopka
	 */
	public function getStroskiPostopka() {
		return $this->stroskiPostopka;
	}

	/**
	 * Sets the stroskiPostopka
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_StroskiPostopka> $stroskiPostopka
	 * @return void
	 */
	public function setStroskiPostopka(Tx_Extbase_Persistence_ObjectStorage $stroskiPostopka) {
		$this->stroskiPostopka = $stroskiPostopka;
	}

	/**
	 * Adds a KorakiPostopka
	 *
	 * @param Tx_A3Ekt_Domain_Model_KorakiPostopka $korakiPostopka
	 * @return void
	 */
	public function addKorakiPostopka(Tx_A3Ekt_Domain_Model_KorakiPostopka $korakiPostopka) {
		$this->korakiPostopka->attach($korakiPostopka);
	}

	/**
	 * Removes a KorakiPostopka
	 *
	 * @param Tx_A3Ekt_Domain_Model_KorakiPostopka $korakiPostopkaToRemove The KorakiPostopka to be removed
	 * @return void
	 */
	public function removeKorakiPostopka(Tx_A3Ekt_Domain_Model_KorakiPostopka $korakiPostopkaToRemove) {
		$this->korakiPostopka->detach($korakiPostopkaToRemove);
	}

	/**
	 * Returns the korakiPostopka
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_KorakiPostopka> $korakiPostopka
	 */
	public function getKorakiPostopka() {
		return $this->korakiPostopka;
	}

	/**
	 * Sets the korakiPostopka
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_KorakiPostopka> $korakiPostopka
	 * @return void
	 */
	public function setKorakiPostopka(Tx_Extbase_Persistence_ObjectStorage $korakiPostopka) {
		$this->korakiPostopka = $korakiPostopka;
	}

	/**
	 * Returns the pritozbeniOrgan
	 *
	 * @return Tx_A3Ekt_Domain_Model_Organ $pritozbeniOrgan
	 */
	public function getPritozbeniOrgan() {
		return $this->pritozbeniOrgan;
	}

	/**
	 * Sets the pritozbeniOrgan
	 *
	 * @param Tx_A3Ekt_Domain_Model_Organ $pritozbeniOrgan
	 * @return void
	 */
	public function setPritozbeniOrgan(Tx_A3Ekt_Domain_Model_Organ $pritozbeniOrgan) {
		$this->pritozbeniOrgan = $pritozbeniOrgan;
	}

	/**
	 * Adds a PravnaPodlaga
	 *
	 * @param Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnePodlageTujina
	 * @return void
	 */
	public function addPravnePodlageTujina(Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnePodlageTujina) {
		$this->pravnePodlageTujina->attach($pravnePodlageTujina);
	}

	/**
	 * Removes a PravnaPodlaga
	 *
	 * @param Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnePodlageTujinaToRemove The PravnaPodlaga to be removed
	 * @return void
	 */
	public function removePravnePodlageTujina(Tx_A3Ekt_Domain_Model_PravnaPodlaga $pravnePodlageTujinaToRemove) {
		$this->pravnePodlageTujina->detach($pravnePodlageTujinaToRemove);
	}

	/**
	 * Returns the pravnePodlageTujina
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PravnaPodlaga> $pravnePodlageTujina
	 */
	public function getPravnePodlageTujina() {
		return $this->pravnePodlageTujina;
	}

	/**
	 * Sets the pravnePodlageTujina
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_A3Ekt_Domain_Model_PravnaPodlaga> $pravnePodlageTujina
	 * @return void
	 */
	public function setPravnePodlageTujina(Tx_Extbase_Persistence_ObjectStorage $pravnePodlageTujina) {
		$this->pravnePodlageTujina = $pravnePodlageTujina;
	}

}
?>