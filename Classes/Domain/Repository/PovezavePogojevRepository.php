<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository extends Tx_Extbase_Persistence_Repository {
    
	protected $defaultOrderings = array(
		'sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING
	);
	
    /**
	 * Returns the povezavePogojev for EKTPovezavePogojev
	 *
	 * @return Tx_A3Ekt_Domain_Model_PovezavePogojev $povezavePogojev
	 */
	public function getPovezavePogojevForEKTPovezavePogojev(Tx_A3Ekt_Domain_Model_EKTPovezavePogoj $eKTPovezavePogoj) {
        $query = $this->createQuery();
        $query->matching($query->equals('ektpovezavepogoj', $eKTPovezavePogoj));
        $query->setOrderings(array('sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING));
        $povezavePogojev = $query->execute();
		return $povezavePogojev;
	}
    
    /**
	 * Returns the povezavePogojev for PovezavePogojev
	 *
	 * @return Tx_A3Ekt_Domain_Model_PovezavePogojev $povezavePogojev
	 */
    public function getPovezavePogojevForPovezavePogojev(Tx_A3Ekt_Domain_Model_PovezavePogojev $povezavePogoj) {
        $query = $this->createQuery();
        $query->matching($query->equals('povezavepogojev', $povezavePogoj));
        $query->setOrderings(array('sorting' => Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING));
        $povezavePogojev = $query->execute();
        return $povezavePogojev;
    }
    
}
?>