<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Robert Feencek <rferencek@aeris3.si>, Aeris3 d.o.o.
 *  Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3 d.o.o.
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_activa_nakupi
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_Domain_Repository_SekundarneVsebineRepository extends Tx_Extbase_Persistence_Repository {
    
    /**
	 * Returns the Sekundarne vsebine
	 *
	 * @param $eKTDejavnost
	 * @return Tx_A3Ekt_Domain_Model_SekundarneVsebine $sekundarneVsebine
	 */
    public function getSekundarneVsebine($ids) {
		if ( $ids != NULL || $ids != 0 ) {
            $query=$this->createQuery();
            $query->getQuerySettings()->setReturnRawQueryResult(TRUE);
        	$data = $query->statement('SELECT header, bodytext, tx_a3ekt_sekundarne_vsebine_tip FROM tt_content WHERE uid IN ('.$ids.');')->execute();
            
            $content=array();
            foreach($data as $dat) {
                $content[]= array(
                    "header"        =>  $dat['header'],
                    "content"       =>  $dat['bodytext'],
                    "contentType"   =>  $dat['tx_a3ekt_sekundarne_vsebine_tip']
                );
            }
        
            return $content;
        }
		else
			return null;
    }
}
?>