<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */

/**
 * View helper which renders a record list as known from the TYPO3 list module
 * Note: This feature is experimental!
 *
 * = Examples =
 *
 * <code title="Minimal">
 * <f:be.tableList tableName="fe_users" />
 * </code>
 * <output>
 * List of all "Website user" records stored in the configured storage PID.
 * Records will be editable, if the current BE user has got edit rights for the table "fe_users".
 * Only the title column (username) will be shown.
 * Context menu is active.
 * </output>
 *
 * <code title="Full">
 * <f:be.tableList tableName="fe_users" fieldList="{0: 'name', 1: 'email'}" storagePid="1" levels="2" filter='foo' recordsPerPage="10" sortField="name" sortDescending="true" readOnly="true" enableClickMenu="false" clickTitleMode="info" alternateBackgroundColors="true" />
 * </code>
 * <output>
 * List of "Website user" records with a text property of "foo" stored on PID 1 and two levels down.
 * Clicking on a username will open the TYPO3 info popup for the respective record
 * </output>
 *
 */
require_once (PATH_typo3 . 'class.db_list.inc');
require_once (PATH_typo3 . 'class.db_list_extra.inc');

class Tx_A3Ekt_ViewHelpers_TableListViewHelper extends Tx_Fluid_ViewHelpers_Be_AbstractBackendViewHelper {

	/**
	 * @var Tx_Extbase_Configuration_ConfigurationManagerInterface
	 */
	protected $configurationManager;

	/**
	 * @param Tx_Extbase_Configuration_ConfigurationManagerInterface $configurationManager
	 * @return void
	 */
	public function injectConfigurationManager(Tx_Extbase_Configuration_ConfigurationManagerInterface $configurationManager) {
		$this->configurationManager = $configurationManager;
		
		$configType = Tx_Extbase_Configuration_ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT;
		$settings = $this->configurationManager->getConfiguration($configType);
		$settings = Tx_Extbase_Utility_TypoScript::convertTypoScriptArrayToPlainArray($settings['plugin.']['tx_a3ekt.']);
		$this->ts = $settings;
	}

	/**
	 * Renders a record list as known from the TYPO3 list module
	 * Note: This feature is experimental!
	 *
	 * @param string $tableName name of the database table
	 * @param integer $storagePid by default, records are fetched from the storage PID configured in persistence.storagePid. With this argument, the storage PID can be overwritten
	 * @param integer $storagePid2 by default, records are fetched from the storage PID configured in persistence.storagePid. With this argument, the storage PID can be overwritten
	 */
	public function render($tableName, $storagePid = NULL, $storagePid2 = NULL) {
		
		if ( $storagePid == null )
			$storagePid = $this->ts['settings']['pidEktStorage'];
		
		if ( $storagePid2 == null )
			$storagePid2 = $this->ts['settings']['pidSbpStorage'];
		
		$count1 = (int)$GLOBALS['TYPO3_DB']->exec_SELECTcountRows('uid', $tableName, 'pid='.$storagePid.' AND NOT deleted');
		$count2 = (int)$GLOBALS['TYPO3_DB']->exec_SELECTcountRows('uid', $tableName, 'pid='.$storagePid2.' AND NOT deleted');

		$nekej = '<tr>';
		$nekej .= '<td align="center" colspan="3" style="font-size: 10px;">'.$tableName.'</td>';
		$nekej .= '</tr>';
		
		$nekej .= '<tr>';
		$nekej .= '<td align="center" colspan="3" style="height: 20px;">&nbsp;</td>';
		$nekej .= '</tr>';
		
		$nekej .= '<tr>';
		$nekej .= '<td align="center" style="font-size: 30px;">'.$count1.'</td>';
		
		$color = '';
		if ( ($count1 - $count2) > 0)
			$color = 'color: red;';
		$nekej .= '<td align="center" style="font-size: 30px; '.$color.'">'.($count1 - $count2).'</td>';
		
		$nekej .= '<td align="center" style="font-size: 30px;">'.$count2.'</td>';
		$nekej .= '</tr>';
		
		$nekej .= '<tr>';
		$nekej .= '<td align="center" colspan="3" style="height: 20px;">&nbsp;</td>';
		$nekej .= '</tr>';
		
		//return $dblist->HTMLcode;
		return $nekej;
	}
}
?>
