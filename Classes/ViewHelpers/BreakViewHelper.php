<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2010 Claus Due <claus@wildside.dk>, Wildside A/S
*
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 *
 * @author Jernej Zorec
 * @package a3_ekt
 */
class Tx_A3Ekt_ViewHelpers_BreakViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractTagBasedViewHelper {

	/**
	 * Initialize arguments
	 */
	public function initializeArguments() {
		$this->registerArgument('as', 'string', 'Which variable to update in the TemplateVariableContainer.');
		$this->registerArgument('title', 'string', 'Which property/field to look by.');
	}

	/**
	 * "Render" method - sorts a target list-type target. Either $array or $objectStorage must be specified. If both are,
	 * ObjectStorage takes precedence.
	 *
	 * @param array $array
	 * @return mixed
	 */
	public function render($array=NULL) {
		
		$prev = '';
		foreach ( $array as &$item ) {
			$frstLetter = mb_substr($item[$this->arguments['title']], 0, 1, 'UTF-8');
			if ( $frstLetter != $prev ) {
				$item['break'] = true;
			} else {
				$item['break'] = false;
			}
			
			$item['uid'] = $item['uid'];
            $item[$this->arguments['title']] = $item[$this->arguments['title']];
            $item['letter'] = $frstLetter;
			
			$prev = $frstLetter;
		}
		
		if ($this->templateVariableContainer->exists($this->arguments['as'])) {
			$this->templateVariableContainer->remove($this->arguments['as']);
		}
		$this->templateVariableContainer->add($this->arguments['as'], $array);
	}

}
