<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_ViewHelpers_OrganViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractViewHelper {
        
        /**
         * Make Organ.
         *
         * @param mixed $organ Organ
         * @param mixed $kontakt Morebitni kontakt
         * @return string
         */
        public function render($organ, $kontakt = null) {
            
                $content = '';
                if (is_object($organ) ) {
                    if ( method_exists($organ, 'getSpletniNaslov') )
                        $content .= '<a href="http://'.$organ->getSpletniNaslov().'" target="_blank">' . $organ->getNaziv() . '</a><br/>';
                    else
                        $content .= $organ->getNaziv() . '<br/>';
                    $content .= $organ->getNaslov() . '<br/>';
                    $content .= $organ->getPosta()->getStevilka() . ' ' . $organ->getPosta()->getNaziv() . '<br/>';
                }
                
                if (is_object($kontakt) ) {
                    $content .= '<br/>';
                    $content .= $kontakt->getImePriimek() . '<br/>';
                    $content .= $kontakt->getEmail() . '<br/>';
                }
                
                /*if ($this->arguments['src']) {
                        $filename = PATH_site . $this->arguments['src'];
                        $template = $this->getTemplate($filename);
                        $variables = $this->templateVariableContainer->getAll();
                        $template->assignMultiple($variables);
                        $rendered = $template->render();
                        $uniqid = md5($rendered);
                        $extension = 'js';
                        $tempfile = $this->documentHead->saveContentToTempFile($rendered, $uniqid, $extension);
                        $this->includeFile($tempfile);  
                }
                // child content passes through as usual, we're only interested in "src"
                return $this->renderChildren();*/
                
                //$content = $organ['naziv'];
                
                return $content;
        }

}

?>
