<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */

/**
 * View helper which gives info about the scheduler task
 */

class Tx_A3Ekt_ViewHelpers_EktSyncViewHelper extends Tx_Fluid_ViewHelpers_Be_AbstractBackendViewHelper {

	/**
	 * @var Tx_Extbase_Configuration_ConfigurationManagerInterface
	 */
	protected $configurationManager;

	/**
	 * @param Tx_Extbase_Configuration_ConfigurationManagerInterface $configurationManager
	 * @return void
	 */
	public function injectConfigurationManager(Tx_Extbase_Configuration_ConfigurationManagerInterface $configurationManager) {
		$this->configurationManager = $configurationManager;
		
		$configType = Tx_Extbase_Configuration_ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT;
		$settings = $this->configurationManager->getConfiguration($configType);
		$settings = Tx_Extbase_Utility_TypoScript::convertTypoScriptArrayToPlainArray($settings['plugin.']['tx_a3ekt.']);
		$this->ts = $settings;
	}

	/**
	 * Renders a record list as known from the TYPO3 list module
	 * Note: This feature is experimental!
	 *
	 * @param string $taskUid uid of the scheduler task
	 */
	public function render($taskUid) {
		
		if ( $taskUid == null )
			$taskUid = $this->ts['settings']['schedulerSyncUid'];
		
		if ( $_REQUEST['sync'] == '1' )
			$this->markForExecutionSync($taskUid);
		
		$dateFormat = $GLOBALS['TYPO3_CONF_VARS']['SYS']['ddmmyy'] . ' ' . $GLOBALS['TYPO3_CONF_VARS']['SYS']['hhmm'];
		
		$scheduler = t3lib_div::makeInstance('tx_scheduler');
		$task = $scheduler->fetchTask($taskUid);
		$taskRecord = $scheduler->fetchTaskRecord($taskUid);
		$lastexecution_time = $taskRecord['lastexecution_time'];
		$nextexecution = $taskRecord['nextexecution'];
		
		if ( $lastexecution_time == 0 )
			$lastsync = 'Not yet synced';
		else {
			$lastsync = 'Last sync: ' . date($dateFormat, $lastexecution_time);
			$lastsync .= '<br/><br/><br/>';
			$lastsync .= '<form action="/typo3/mod.php">';
			$lastsync .= '<input type="hidden" name="M" value="tools_A3EktEktsync" />';
			$lastsync .= '<input type="hidden" name="sync" value="1" />';
			$lastsync .= '<input type="submit" value="Synchronize" /></form>';
		}
			
		if ( $task->isExecutionRunning() || $nextexecution != 0 ) {
			$lastsync = 'Sync in progress<br/><br/><br/><img src="/typo3conf/ext/a3_ekt/Resources/Public/Images/ajax-loader.gif" alt="..." />';
			$lastsync .= '<script type="text/javascript">setTimeout( "window.location.reload();", 30*1000 );</script>';

			if ( $nextexecution != 0 )
				$lastsync .= '<br/><p>(will start at next scheduler run)</p>';
		}
		
		//return $dblist->HTMLcode;
		return $lastsync;
	}
	
	private function markForExecutionSync($taskUid) {
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('disable', 'tx_scheduler_task', 'uid = ' . (int)$taskUid . '');
		while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$disable = $row['disable'];
		}
		$GLOBALS['TYPO3_DB']->sql_free_result($res);

		if ( $disable == '1' ) {
			$updateFields = array(
				'nextexecution' => time(),
				'disable' => '0'
			);
			$res = $GLOBALS['TYPO3_DB']->exec_UPDATEquery('tx_scheduler_task', 'uid = ' . (int)$taskUid . '', $updateFields);
			return true;
		} else {
			return -1;
		}
		return false;
	}
}
?>
