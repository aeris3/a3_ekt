<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 *
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 *
 */
class Tx_A3Ekt_ViewHelpers_PovezavePogojevViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractViewHelper {
        
        /**
         * Povezave pogojev
         *
         * @param mixed $pogoj Pogoj
		 * @param mixed $dejavnost Dejavnost
  		 * @param mixed $poklic Poklic
		 * @param mixed $dovoljenje Dovoljenje
         * @return String
         */
        public function render($pogoj = 0, $dejavnost = 0, $poklic = 0, $dovoljenje = 0) {
			
			$this->setArguments(array('poklic' => 8));
			
			$templateFile = t3lib_div::getFileAbsFileName('EXT:a3_ekt/Resources/Private/Partials/ViewHelpers/PovezavePogojev.html');
	        $view = t3lib_div::makeInstance('Tx_Fluid_View_StandaloneView');
	        $view->setTemplatePathAndFilename($templateFile);
			
			$arguments = $this->prepareArguments();
			
			if ( $this->hasArgument('dejavnost') ) {

				$dejavnostUid = $arguments['dejavnost'];
				$view->assign('dejavnost', true);

				// getting ektpovezave pogoj from dejavnost
				$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EktPovezavePogojRepository');
				$query = $nekej->createQuery();
				$data = $query->matching(
					$query->logicalAnd(
						$query->equals('dejavnost', $dejavnostUid)
				))->execute();
				$ektpovezavepogojUid = $data->getFirst()->getUid();

				// getting povezave pogojev
				$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository');
				$query = $nekej->createQuery();
				$data = $query->matching(
					$query->logicalAnd(
						$query->equals('dovoljenje', $dovoljenje),
						$query->equals('ektpovezavepogoj', $ektpovezavepogojUid)
				))->execute();

			} elseif ( $this->hasArgument('povezavePogojev') ) {

				$povezavePogojevUid = $arguments['povezavePogojev'];
				$view->assign('povezavePogojev', $povezavePogojevUid);

				// getting povezave pogojev
				$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository');
				$query = $nekej->createQuery();
				$data = $query->matching(
					$query->equals('uid', $povezavePogojevUid)
				)->execute();

			} elseif ( $this->hasArgument('poklic') ) {
				
				$poklicUid = $arguments->poklic->getName();
				
				$view->assign('poklic', true);

				// getting ektpovezave pogoj from dejavnost
				$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_EktPovezavePoklicevRepository');
				$query = $nekej->createQuery();
				$data = $query->matching(
					$query->logicalAnd(
						$query->equals('poklic', $poklicUid)
				))->execute();
				$ektpovezavepoklicUid = $data->getFirst()->getUid();

				// getting povezave pogojev
				$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository');
				$query = $nekej->createQuery();
				$data = $query->matching(
					$query->logicalAnd(
						$query->equals('dovoljenje', $dovoljenje),
						$query->equals('ektpovezavepoklicev', $ektpovezavepoklicUid)
				))->execute();

			} elseif ( $this->hasArgument('povezavePoklicev') ) {

				$povezavePoklicevUid = $arguments['povezavePoklicev'];
				$view->assign('povezavePoklicev', $povezavePogojevUid);

				// getting povezave pogojev
				$nekej = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository');
				$query = $nekej->createQuery();
				$data = $query->matching(
					$query->equals('uid', $povezavePoklicevUid)
				)->execute();

			}
						
	        $view->assign('d', $data[0]);
			
	        $content = $view->render();
			
            return $content;
        }

}

?>
