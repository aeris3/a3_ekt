<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
 *  Jernej Zorec <jzorec@aeris3.si>, Aeris3
 *  Robert Ferencek <rferencek@aeris3.si>, Aeris3
 *  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * EKT SOLR Search Widget
 *
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 */

class Tx_A3Ekt_ViewHelpers_Widget_EktSolrViewHelper extends Tx_Fluid_Core_Widget_AbstractWidgetViewHelper implements Tx_Fluid_Core_ViewHelper_Facets_ChildNodeAccessInterface {

	/**
	 * @var Tx_A3Ekt_Controller_EktSolrController
	 */
	protected $controller;

	/**
	 * @var boolean
	 */
	protected $ajaxWidget = TRUE;

	/**
	 * @param Tx_A3Ekt_Controller_EktSolrController $controller
	 */
	public function injectController(Tx_A3Ekt_Controller_EktSolrController $controller) {
		$this->controller = $controller;
	}

	/**
	 * Initialize
	 */
	public function initializeArguments() {
		$this->registerArgument('id', 'string', 'If specified, is used as ID for widget container');
		$this->registerArgument('options', 'array', 'Options to override options configured in TypoScript', FALSE, array());
		$this->registerArgument('templatePathAndFilename', 'string', 'Optional path to a template file containing rendering sections');
		$this->registerArgument('layoutRootPath', 'string', 'Optional Layout root path');
		$this->registerArgument('partialRootPath', 'string', 'Optional Partialroot path');
		$this->registerArgument('addDefaultStylesheet', 'boolean', 'Add default CSS file for the widget', FALSE, TRUE);
	}

	/**
	 * Render
	 */
	public function render() {
		return $this->initiateSubRequest();
	}

}

?>