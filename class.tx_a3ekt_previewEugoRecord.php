<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
*  Jernej Zorec <jzorec@aeris3.si>, Aeris3
*  Robert Ferencek <rferencek@aeris3.si>, Aeris3
*  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

require_once(t3lib_div::getIndpEnv('TYPO3_DOCUMENT_ROOT') . '/typo3/interfaces/interface.localrecordlist_actionsHook.php');

/**
 * A query specialized to get search suggestions
 *
 * @author	Jernej Zorec <jzorec@aeris3.si>, Aeris3
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 */
class tx_a3ekt_previewEugoRecord implements localRecordList_actionsHook {

	protected $configuration = array();

	/**
	 * constructor for class tx_timtabakismet_Akismet
	 */
	public function __construct() {
		if ($GLOBALS['TSFE']) {
				// FE
			$this->configuration = $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_a3ekt.'];
		}
	}

	protected function initConfiguration() {
		$pageSelect = t3lib_div::makeInstance('t3lib_pageSelect');
		$rootLine = $pageSelect->getRootLine(t3lib_div::_GET('id'));

		$tmpl = t3lib_div::makeInstance('t3lib_tsparser_ext');
		$tmpl->tt_track = false; // Do not log time-performance information
		$tmpl->init();
		$tmpl->runThroughTemplates($rootLine); // This generates the constants/config + hierarchy info for the template.
		$tmpl->generateConfig();

		list($a3ektSetup) = $tmpl->ext_getSetup($tmpl->setup, 'plugin.tx_a3ekt');
		$this->configuration = $a3ektSetup;
	}

	/**
	 * modifies Web>List clip icons (copy, cut, paste, etc.) of a displayed row
	 *
	 * @param	string		the current database table
	 * @param	array		the current record row
	 * @param	array		the default clip-icons to get modified
	 * @param	object		Instance of calling object
	 * @return	array		the modified clip-icons
	 */
	public function makeClip($table, $row, $cells, &$parentObject) {
		return $cells;
	}


	/**
	 * modifies Web>List control icons of a displayed row
	 *
	 * @param	string		the current database table
	 * @param	array		the current record row
	 * @param	array		the default control-icons to get modified
	 * @param	object		Instance of calling object
	 * @return	array		the modified control-icons
	 */
	public function makeControl($table, $row, $cells, &$parentObject) {

		$emptyCell = '<span class="t3-icon t3-icon-empty t3-icon-empty-empty t3-icon-empty">&nbsp;</span>';

		if ($table == 'tx_a3ekt_domain_model_sektor') {
			
			if ( empty($this->configuration) ) $this->initConfiguration();

			$id = $this->configuration['settings.']['sektorji'];
			$uid = $this->getUidOfRecord($row);
			$params = '&L='.$row['sys_language_uid'].'&tx_a3ekt_template%5Bsektor%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=Sektor';
			$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
			$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';

			if ( $row['hidden'] == 0 )
				$cells = array_merge( array('tx_a3ekt' => $previewCell), $cells);
			else
				$cells = array_merge( array('tx_a3ekt' => $emptyCell), $cells);
		
		} else if ($table == 'tx_a3ekt_domain_model_dejavnost' || $table == 'tx_a3ekt_domain_model_ektpovezavepogoj' || $table == 'tx_a3ekt_domain_model_povezavepogojev' ) {

			if ( empty($this->configuration) ) $this->initConfiguration();

			$id = $this->configuration['settings.']['dejavnosti'];
			
			if ( $table == 'tx_a3ekt_domain_model_dejavnost' || $table == 'tx_a3ekt_domain_model_povezavepogojev' )
				$uid = $this->getUidOfRecord($row);
			else
				$uid = $this->getUidOfParentRecord($row, 'dejavnost');

			$params = '&L='.$row['sys_language_uid'].'&tx_a3ekt_template%5Bdejavnost%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=Dejavnost';
			$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
			$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';

			if ( $row['hidden'] == 0 )
				$cells = array_merge( array('tx_a3ekt' => $previewCell), $cells);
			else
				$cells = array_merge( array('tx_a3ekt' => $emptyCell), $cells);

		} else if ($table == 'tx_a3ekt_domain_model_poklic' || $table == 'tx_a3ekt_domain_model_ektpovezavepoklicev') {

			if ( empty($this->configuration) ) $this->initConfiguration();

			$id = $this->configuration['settings.']['poklici'];
			if ( $table == 'tx_a3ekt_domain_model_poklic' )
				$uid = $this->getUidOfRecord($row);
			else
				$uid = $this->getUidOfParentRecord($row, 'poklic');

			$params = '&L='.$row['sys_language_uid'].'&tx_a3ekt_template%5Bpoklic%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=Poklic';
			$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
			$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';

			if ( $row['hidden'] == 0 )
				$cells = array_merge( array('tx_a3ekt' => $previewCell), $cells);
			else
				$cells = array_merge( array('tx_a3ekt' => $emptyCell), $cells);
			
		} else if ($table == 'tx_a3ekt_domain_model_dovoljenje') {

			if ( empty($this->configuration) ) $this->initConfiguration();

			$id = $this->configuration['settings.']['dovoljenja'];
			$uid = $this->getUidOfRecord($row);
			$params = '&L='.$row['sys_language_uid'].'&tx_a3ekt_template%5Bdovoljenje%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=Dovoljenje';
			$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
			$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';

			if ( $row['hidden'] == 0 )
				$cells = array_merge( array('tx_a3ekt' => $previewCell), $cells);
			else
				$cells = array_merge( array('tx_a3ekt' => $emptyCell), $cells);
			
		} else if ($table == 'tx_a3ekt_domain_model_dovoljenjevtujini') {

			if ( empty($this->configuration) ) $this->initConfiguration();

			$id = $this->configuration['settings.']['dovoljenjavtujini'];
			$uid = $this->getUidOfRecord($row);
			$params = '&L='.$row['sys_language_uid'].'&tx_a3ekt_template%5BdovoljenjeVTujini%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=DovoljenjeVTujini';
			$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
			$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';

			if ( $row['hidden'] == 0 )
				$cells = array_merge( array('tx_a3ekt' => $previewCell), $cells);
			else
				$cells = array_merge( array('tx_a3ekt' => $emptyCell), $cells);
			
		} else if ($table == 'tx_a3ekt_domain_model_drugipogoj') {

			if ( empty($this->configuration) ) $this->initConfiguration();

			$id = $this->configuration['settings.']['drugipogoji'];
			$uid = $this->getUidOfRecord($row);
			$params = '&L='.$row['sys_language_uid'].'&tx_a3ekt_template%5BdrugiPogoj%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=DrugiPogoj';
			$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
			$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';

			if ( $row['hidden'] == 0 )
				$cells = array_merge( array('tx_a3ekt' => $previewCell), $cells);
			else
				$cells = array_merge( array('tx_a3ekt' => $emptyCell), $cells);
			
		} else if ($table == 'tx_a3ekt_domain_model_drugipogoj') {

			if ( empty($this->configuration) ) $this->initConfiguration();

			$id = $this->configuration['settings.']['drugipogoji'];
			$uid = $this->getUidOfRecord($row);
			$params = '&L='.$row['sys_language_uid'].'&tx_a3ekt_template%5BdrugiPogoj%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=DrugiPogoj';
			$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
			$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';

			if ( $row['hidden'] == 0 )
				$cells = array_merge( array('tx_a3ekt' => $previewCell), $cells);
			else
				$cells = array_merge( array('tx_a3ekt' => $emptyCell), $cells);
			
		} else if ($table == 'tx_a3ekt_domain_model_cezmejnoopravljenjepoklica') {

			if ( empty($this->configuration) ) $this->initConfiguration();

			$id = $this->configuration['settings.']['cezmejnaopravljenjapoklicev'];
			$uid = $this->getUidOfRecord($row);
			$params = '&L='.$row['sys_language_uid'].'&tx_a3ekt_template%5BcezmejnoOpravljenjePoklica%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=CezmejnoOpravljenjePoklica';
			$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
			$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';

			if ( $row['hidden'] == 0 )
				$cells = array_merge( array('tx_a3ekt' => $previewCell), $cells);
			else
				$cells = array_merge( array('tx_a3ekt' => $emptyCell), $cells);
		}

		//$cells = array_reverse($cells);
		/*echo '<pre>';
		var_dump($cells);
		die();*/

		return $cells;
	}


	/**
	 * modifies Web>List header row columns/cells
	 *
	 * @param	string		the current database table
	 * @param	array		Array of the currently displayed uids of the table
	 * @param	array		An array of rendered cells/columns
	 * @param	object		Instance of calling (parent) object
	 * @return	array		Array of modified cells/columns
	 */
	public function renderListHeader($table, $currentIdList, $headerColumns, &$parentObject) {
		return $headerColumns;
	}


	/**
	 * modifies Web>List header row clipboard/action icons
	 *
	 * @param	string		the current database table
	 * @param	array		Array of the currently displayed uids of the table
	 * @param	array		An array of the current clipboard/action icons
	 * @param	object		Instance of calling (parent) object
	 * @return	array		Array of modified clipboard/action icons
	 */
	public function renderListHeaderActions($table, $currentIdList, $cells, &$parentObject) {
		return $cells;
	}

	private function getUidOfRecord($row) {
		if ( $row['l10n_parent'] != 0 && $row['sys_language_uid'] != 0  )
			$uid = $row['l10n_parent'];
		else
			$uid = $row['uid'];
		return $uid;
	}

	private function getUidOfParentRecord($row, $type) {
		if ( $type == 'poklic' )
			$uid= $row['poklic'];
		else if ( $type == 'dejavnost' )
			$uid= $row['dejavnost'];
		return $uid;
	}

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/a3_ekt/class.tx_a3ekt_previewEugoRecord.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/a3_ekt/class.tx_a3ekt_previewEugoRecord.php']);
}

?>