<?php
class tx_a3ekt_tca {
	function descriptionField($PA, $fObj) {
		$label = (isset($PA['parameters']['label'])) ? $PA['parameters']['label']:'';
		$formField  = '<b>'.$label.'</b>';
		if ( isset($PA['parameters']['description']) && $PA['parameters']['description'] != '' )
			$formField .= '<br/><br/>' . $PA['parameters']['description'];
		$formField .= '<br/><br/>';
		return $formField;
	}
	function spacerField($PA, $fObj) {
		return '<br/><br/>';
	}
	function lineField($PA, $fObj) {
		return '';
	}
	
	public function nazivTitle(&$parameters, $parentObject) {
		$limit = 100;
		$title = t3lib_BEfunc::getRecord($parameters['table'], $parameters['row']['uid']);
		if ( strlen($title['naziv']) > $limit  )
			$parameters['title'] = substr($title['naziv'], 0, ($limit-3)) . '...';
		else
			$parameters['title'] = $title['naziv'];
	}
	
	public function povezavePogojevTitle(&$parameters, $parentObject) {
		$record = t3lib_BEfunc::getRecord($parameters['table'], $parameters['row']['uid']);
		$title = '';
		switch ($record['tip']) {
			case 0:
				$title = '--- izberi ---';
				break;
			case 1:
				$povezave_poklicev = t3lib_BEfunc::getRecord('tx_a3ekt_domain_model_ektpovezavepoklicev', $record['poklic']);
				$poklic_uid = $povezave_poklicev['poklic'];
				//$pogoj_uid = $povezave_poklicev['pogoj'];
				
				$poklic = t3lib_BEfunc::getRecord('tx_a3ekt_domain_model_poklic', $poklic_uid);
				//$pogoj = t3lib_BEfunc::getRecord('tx_a3ekt_domain_model_povezavepoklicev', $pogoj_uid);
				
				$title = 'Poklic / ' . $poklic['naziv'];
				
				break;
			case 2:
				$related_record = t3lib_BEfunc::getRecord('tx_a3ekt_domain_model_dovoljenje', $record['dovoljenje']);
				$title = 'Dovoljenje / ' . $related_record['naziv'];
				break;
			case 3:
				$related_record = t3lib_BEfunc::getRecord('tx_a3ekt_domain_model_cezmejnodovoljenje', $record['cezmejno_dovoljenje']);
				$title = 'Čezmejno dovoljenje / ' . $related_record['naziv'];
				break;
			case 4:
				$related_record = t3lib_BEfunc::getRecord('tx_a3ekt_domain_model_drugipogoj', $record['drugi_pogoj']);
				$title = 'Drugi pogoj / ' . $related_record['naziv'];
				break;
			case 5:
				$related_record = t3lib_BEfunc::getRecord('tx_a3ekt_domain_model_dejavnost', $record['dejavnost']);
				$title = 'Dejavnost / ' . $related_record['naziv'];
				break;
			case 98:
				$title = '--- sklop ---';
				break;
			case 99:
				$title = '--- ali ---';
				break;
		}
		
		$parameters['title'] = $title;
	}
	
	public function povezavePoklicevTitle(&$parameters, $parentObject) {
		$record = t3lib_BEfunc::getRecord($parameters['table'], $parameters['row']['uid']);
		$title = '';
		switch ($record['tip']) {
			case 0:
				$title = '--- izberi ---';
				break;
			case 1:
				$related_record = t3lib_BEfunc::getRecord('tx_a3ekt_domain_model_dovoljenje', $record['dovoljenje']);
				$title = 'Dovoljenje / ' . $related_record['naziv'];
				break;
			case 2:
				$related_record = t3lib_BEfunc::getRecord('tx_a3ekt_domain_model_cezmejnoopravljenjepoklica', $record['cezmejno_dovoljenje']);
				$title = 'Čezmejno dovoljenje / ' . $related_record['naziv'];
				break;
			case 3:
				$related_record = t3lib_BEfunc::getRecord('tx_a3ekt_domain_model_drugipogoj', $record['drugi_pogoj']);
				$title = 'Drugi pogoj / ' . $related_record['naziv'];
				break;
			case 4:
				$related_record = t3lib_BEfunc::getRecord('tx_a3ekt_domain_model_dovoljenjevtujini', $record['dovoljenje_v_tujini']);
				$title = 'Dovoljenje v tujini / ' . $related_record['naziv_dovoljenja'];
				break;
			case 5:
				$related_record = t3lib_BEfunc::getRecord('tx_a3ekt_domain_model_poklic', $record['poklic']);
				$title = 'Poklic / ' . $related_record['naziv'];
				break;
			case 98:
				$title = '--- sklop ---';
				break;
			case 99:
				$title = '--- ali ---';
				break;
		}
		
		$parameters['title'] = $title;
	}
}
?>