<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
*  Jernej Zorec <jzorec@aeris3.si>, Aeris3
*  Robert Ferencek <rferencek@aeris3.si>, Aeris3
*  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/


/**
 * A query specialized to get search suggestions
 *
 * @author	Jernej Zorec <jzorec@aeris3.si>, Aeris3
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 */
class tx_a3ekt_EktQuery extends tx_solr_Query {

	protected $configuration;

	/**
	 * constructor for class tx_solr_SuggestQuery
	 */
	public function __construct($query, $config) {
		$this->queryFromRequst = $query;
		$this->solrConfig = $config;
		$this->configuration = $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_solr.'];
		
		parent::__construct('');
		//$this->setAlternativeQuery('*:*');
		//$this->addQueryParameter('q', 'title:aaa');
		$this->setQueryElevation(false, false);
	}
	
	public function setIndex($index) {
		$this->index = $index;
	}
	
	public function getQueryParameters() {
		
		$types = array();
		$fq_string = '';
		foreach($this->index as $key => $type) {
			if ( substr($type['table'], 0, 8) == 'tx_a3ekt' ) {
				$types[] = $type['table'];
				$fq_string .= '(type:"' . $type['table'] . '") OR ';
			}
		}
		$fq_string = rtrim($fq_string, ' OR');
		
		$queryParameters = array(
			'fl'			=> 'title,type,url',
			'q'				=> 'title:*' . $this->queryFromRequst . '*',
			'fq'			=> $fq_string
		);
		
		return array_merge($queryParameters, $this->queryParameters);
	}
}


if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/a3_ekt/class.tx_a3ekt_ektquery.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/a3_ekt/class.tx_a3ekt_ektquery.php']);
}

?>