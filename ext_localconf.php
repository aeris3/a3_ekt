<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::configurePlugin(
	$_EXTKEY,
	'Template',
	array(
		'EKTDejavnost' => 'show',
		'Dejavnost' => 'list, show',
		'Dovoljenje' => 'list, show',
		'Poklic' => 'list, show',
		'Sektor' => 'list, show',
		'DrugiPogoj' => 'show',
		'CezmejnoDovoljenje' => 'show',
		'DovoljenjeVTujini' => 'show',
		'CezmejnoOpravljenjePoklica' => 'show',
		
	),
	// non-cacheable actions
	array(
		'EKTDejavnost' => '',
		'Dejavnost' => '',
		'Dovoljenje' => '',
		'Poklic' => '',
		'Sektor' => '',
		'DrugiPogoj' => '',
		'CezmejnoDovoljenje' => '',
		'DovoljenjeVTujini' => '',
		'CezmejnoOpravljenjePoklica' => '',
		
	)
);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$TYPO3_CONF_VARS['FE']['eID_include']['tx_a3ekt_solr'] = 'EXT:a3_ekt/eid_solr/solr.php';

$TYPO3_CONF_VARS['SC_OPTIONS']['typo3/class.db_list_extra.inc']['actions'][] = 'tx_a3ekt_previewEugoRecord';
$TYPO3_CONF_VARS['SC_OPTIONS']['t3lib/class.t3lib_tceforms_inline.php']['tceformsInlineHook'][] = 'tx_a3ekt_previewInlineEugoRecord';

$TYPO3_CONF_VARS['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][] = 'tx_a3ekt_postBeSave';
$TYPO3_CONF_VARS['SC_OPTIONS']['t3lib/class.t3lib_tceforms.php']['getMainFieldsClass'][] = 'tx_a3ekt_postBeSave';

?>