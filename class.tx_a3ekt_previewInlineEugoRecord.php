<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
*  Jernej Zorec <jzorec@aeris3.si>, Aeris3
*  Robert Ferencek <rferencek@aeris3.si>, Aeris3
*  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

require_once(t3lib_div::getIndpEnv('TYPO3_DOCUMENT_ROOT') . '/t3lib/interfaces/interface.t3lib_tceformsinlinehook.php');
require_once(t3lib_extMgm::extPath('a3_ekt').'class.tx_a3ekt_previewEugoRecord.php');

/**
 * A query specialized to get search suggestions
 *
 * @author	Jernej Zorec <jzorec@aeris3.si>, Aeris3
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 */
class tx_a3ekt_previewInlineEugoRecord implements t3lib_tceformsInlineHook {

	protected $configuration = array();

	/**
     * Initializes this hook object.
     *
     * @param       t3lib_TCEforms_inline           $parentObject: The calling t3lib_TCEforms_inline object.
     * @return      void
     */
	public function init(&$parentObject) {

	}

	/**
	 * Pre-processing to define which control items are enabled or disabled.
	 *
	 * @param	string		$parentUid: The uid of the parent (embedding) record (uid or NEW...)
	 * @param	string		$foreignTable: The table (foreign_table) we create control-icons for
	 * @param	array		$childRecord: The current record of that foreign_table
	 * @param	array		$childConfig: TCA configuration of the current field of the child record
	 * @param	boolean		$isVirtual: Defines whether the current records is only virtually shown and not physically part of the parent record
	 * @param	array		&$enabledControls: (reference) Associative array with the enabled control items
	 * @return	void
	 */
	public function renderForeignRecordHeaderControl_preProcess($parentUid, $foreignTable, array $childRecord, array $childConfig, $isVirtual, array &$enabledControls) {

	}

	/**
	 * Post-processing to define which control items to show. Possibly own icons can be added here.
	 *
	 * @param	string		$parentUid: The uid of the parent (embedding) record (uid or NEW...)
	 * @param	string		$foreignTable: The table (foreign_table) we create control-icons for
	 * @param	array		$childRecord: The current record of that foreign_table
	 * @param	array		$childConfig: TCA configuration of the current field of the child record
	 * @param	boolean		$isVirtual: Defines whether the current records is only virtually shown and not physically part of the parent record
	 * @param	array		&$controlItems: (reference) Associative array with the currently available control items
	 * @return	void
	 */
	public function renderForeignRecordHeaderControl_postProcess($parentUid, $foreignTable, array $childRecord, array $childConfig, $isVirtual, array &$controlItems) {
		
		$this->initConfiguration($childRecord['pid']);

		//$id = $_REQUEST['edit']['tx_a3ekt_domain_model_ektpovezavepoklicev']);
		//die();
		//$previewEugoRecords = t3lib_div::makeInstance('tx_a3ekt_previewEugoRecord');
		//$newCells = $previewEugoRecords->makeControl($foreignTable, $childRecord, $cells);
		
		if ( $foreignTable == 'tx_a3ekt_domain_model_povezavepoklicev' ) {
			$tip = '';
			switch ($childRecord['tip']) {
				case 1:
					$id = $this->configuration['settings.']['dovoljenja'];
					$uid = $childRecord['dovoljenje'];
					$params = '&L='.$childRecord['sys_language_uid'].'&tx_a3ekt_template%5Bdovoljenje%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=Dovoljenje';
					$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
					$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';
					$tip = $previewCell;
					break;
				case 3:
					$id = $this->configuration['settings.']['drugipogoji'];
					$uid = $childRecord['drugi_pogoj'];
					$params = '&L='.$childRecord['sys_language_uid'].'&tx_a3ekt_template%5BdrugiPogoj%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=DrugiPogoj';
					$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
					$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';
					$tip = $previewCell;
					break;
				case 4:
					$id = $this->configuration['settings.']['dovoljenjavtujini'];
					$uid = $childRecord['dovoljenje_v_tujini'];
					$params = '&L='.$childRecord['sys_language_uid'].'&tx_a3ekt_template%5BdovoljenjeVTujini%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=DovoljenjeVTujini';
					$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
					$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';
					$tip = $previewCell;
					break;
				case 5:
					$id = $this->configuration['settings.']['poklici'];
					$uid = $childRecord['poklic'];
					$params = '&L='.$childRecord['sys_language_uid'].'&tx_a3ekt_template%5Bpoklic%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=Poklic';
					$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
					$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';
					$tip = $previewCell;
					break;
			}
		} else if ( $foreignTable == 'tx_a3ekt_domain_model_povezavepogojev' ) {
			$tip = '';
			switch ($childRecord['tip']) {
				case 1:
					$id = $this->configuration['settings.']['poklici'];
					$povezave_poklicev = t3lib_BEfunc::getRecord('tx_a3ekt_domain_model_ektpovezavepoklicev', $childRecord['poklic']);
					$uid = $povezave_poklicev['poklic'];
					$params = '&L='.$childRecord['sys_language_uid'].'&tx_a3ekt_template%5Bpoklic%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=Poklic';
					$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
					$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';
					$tip = $previewCell;
					break;
				case 2:
					$id = $this->configuration['settings.']['dovoljenja'];
					$uid = $childRecord['dovoljenje'];
					$params = '&L='.$childRecord['sys_language_uid'].'&tx_a3ekt_template%5Bdovoljenje%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=Dovoljenje';
					$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
					$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';
					$tip = $previewCell;
					break;
				case 4:
					$id = $this->configuration['settings.']['drugipogoji'];
					$uid = $childRecord['drugi_pogoj'];
					$params = '&L='.$childRecord['sys_language_uid'].'&tx_a3ekt_template%5BdrugiPogoj%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=DrugiPogoj';
					$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
					$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';
					$tip = $previewCell;
					break;
				case 5:
					$id = $this->configuration['settings.']['dejavnosti'];
					$uid = $childRecord['dejavnost'];
					$params = '&L='.$childRecord['sys_language_uid'].'&tx_a3ekt_template%5Bdejavnost%5D='.$uid.'&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=Dejavnost';
					$viewOnClick = t3lib_BEfunc::viewOnClick($id, '', t3lib_BEfunc::BEgetRootLine($id), '', '', $params);
					$previewCell = '<a href="#" onclick="' . htmlspecialchars($viewOnClick) . '" title="Show">' . t3lib_iconWorks::getSpriteIcon('actions-document-view') . '</a>';
					$tip = $previewCell;
					break;
			}
		}

		if ( $uid == null || $uid == 0 )
			$tip = '';

		if ( $childRecord['hidden'] == 0 )
			$controlItems = array_merge( array('tx_a3ekt' => $tip), $controlItems);
		else
			$controlItems = array_merge( array('tx_a3ekt' => '<span class="t3-icon t3-icon-empty t3-icon-empty-empty t3-icon-empty">&nbsp;</span>'), $controlItems);

	}

	protected function initConfiguration($gId) {

		if ( t3lib_div::_GET('id') != null )
			$id = t3lib_div::_GET('id');
		else
			$id = $gId;

		$pageSelect = t3lib_div::makeInstance('t3lib_pageSelect');
		$rootLine = $pageSelect->getRootLine($id);

		$tmpl = t3lib_div::makeInstance('t3lib_tsparser_ext');
		$tmpl->tt_track = false; // Do not log time-performance information
		$tmpl->init();
		$tmpl->runThroughTemplates($rootLine); // This generates the constants/config + hierarchy info for the template.
		$tmpl->generateConfig();

		list($a3ektSetup) = $tmpl->ext_getSetup($tmpl->setup, 'plugin.tx_a3ekt');
		$this->configuration = $a3ektSetup;
		return $a3ektSetup;
	}

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/a3_ekt/class.tx_a3ekt_previewInlineEugoRecord.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/a3_ekt/class.tx_a3ekt_previewInlineEugoRecord.php']);
}

?>