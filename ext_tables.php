<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::registerPlugin(
	$_EXTKEY,
	'Template',
	'Fluid Template Display'
);

$pluginSignature = str_replace('_','',$_EXTKEY) . '_' . template;
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
t3lib_extMgm::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_' .template. '.xml');

t3lib_extMgm::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'EKT');

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_ektdejavnost', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_ektdejavnost.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_ektdejavnost');
			$TCA['tx_a3ekt_domain_model_ektdejavnost'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_ektdejavnost',
					'label' => 'naziv',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/EKTDejavnost.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_ektdejavnost.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_dejavnost', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_dejavnost.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_dejavnost');
			$TCA['tx_a3ekt_domain_model_dejavnost'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dejavnost',
					'label' => 'naziv',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Dejavnost.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_dejavnost.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_dovoljenje', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_dovoljenje.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_dovoljenje');
			$TCA['tx_a3ekt_domain_model_dovoljenje'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje',
					'label' => 'naziv',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Dovoljenje.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_dovoljenje.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_poklic', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_poklic.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_poklic');
			$TCA['tx_a3ekt_domain_model_poklic'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic',
					'label' => 'naziv',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Poklic.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_poklic.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_organ', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_organ.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_organ');
			$TCA['tx_a3ekt_domain_model_organ'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_organ',
					'label' => 'naziv',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Organ.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_organ.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_posta', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_posta.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_posta');
			$TCA['tx_a3ekt_domain_model_posta'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_posta',
					'label' => 'naziv',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Posta.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_posta.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_pravnapodlaga', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_pravnapodlaga.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_pravnapodlaga');
			$TCA['tx_a3ekt_domain_model_pravnapodlaga'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_pravnapodlaga',
					'label' => 'naziv',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/PravnaPodlaga.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_pravnapodlaga.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_sorodnepovezave', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_sorodnepovezave.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_sorodnepovezave');
			$TCA['tx_a3ekt_domain_model_sorodnepovezave'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_sorodnepovezave',
					'label' => 'naziv',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/SorodnePovezave.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_sorodnepovezave.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_skddejavnost', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_skddejavnost.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_skddejavnost');
			$TCA['tx_a3ekt_domain_model_skddejavnost'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_skddejavnost',
					'label' => 'deskriptor',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/SKDDejavnost.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_skddejavnost.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_sektor', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_sektor.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_sektor');
			$TCA['tx_a3ekt_domain_model_sektor'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_sektor',
					'label' => 'naziv',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Sektor.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_sektor.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_kontaktnaoseba', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_kontaktnaoseba.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_kontaktnaoseba');
			$TCA['tx_a3ekt_domain_model_kontaktnaoseba'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_kontaktnaoseba',
					'label' => 'ime_priimek',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/KontaktnaOseba.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_kontaktnaoseba.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_vloga', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_vloga.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_vloga');
			$TCA['tx_a3ekt_domain_model_vloga'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_vloga',
					'label' => 'zaporedna_stevilka',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Vloga.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_vloga.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_vlogapriloga', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_vlogapriloga.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_vlogapriloga');
			$TCA['tx_a3ekt_domain_model_vlogapriloga'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_vlogapriloga',
					'label' => 'stevilka_vloge',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/VlogaPriloga.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_vlogapriloga.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_stroskipostopka', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_stroskipostopka.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_stroskipostopka');
			$TCA['tx_a3ekt_domain_model_stroskipostopka'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_stroskipostopka',
					'label' => 'naziv_stroska',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/StroskiPostopka.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_stroskipostopka.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_korakipostopka', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_korakipostopka.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_korakipostopka');
			$TCA['tx_a3ekt_domain_model_korakipostopka'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_korakipostopka',
					'label' => 'korak',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/KorakiPostopka.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_korakipostopka.gif'
				),
			);
            
            t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_korak', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_korak.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_korak');
			$TCA['tx_a3ekt_domain_model_korak'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_korak',
					'label' => 'naziv',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Korak.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_korak.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_drugipogoj', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_drugipogoj.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_drugipogoj');
			$TCA['tx_a3ekt_domain_model_drugipogoj'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_drugipogoj',
					'label' => 'naziv',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/DrugiPogoj.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_drugipogoj.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_registerdovoljenj', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_registerdovoljenj.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_registerdovoljenj');
			$TCA['tx_a3ekt_domain_model_registerdovoljenj'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_registerdovoljenj',
					'label' => 'naziv',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/RegisterDovoljenj.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_registerdovoljenj.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_cezmejnodovoljenje', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_cezmejnodovoljenje.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_cezmejnodovoljenje');
			$TCA['tx_a3ekt_domain_model_cezmejnodovoljenje'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_cezmejnodovoljenje',
					'label' => 'naziv',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/CezmejnoDovoljenje.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_cezmejnodovoljenje.gif'
				),
			);
			
			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_cezmejnoopravljenjepoklica', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_tx_a3ekt_domain_model_cezmejnoopravljenjepoklica.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_cezmejnoopravljenjepoklica');
			$TCA['tx_a3ekt_domain_model_cezmejnoopravljenjepoklica'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_cezmejnoopravljenjepoklica',
					'label' => 'naziv',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/CezmejnoOpravljenjePoklica.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_cezmejnoopravljenjepoklica.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_povezavepogojev', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_povezavepogojev.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_povezavepogojev');
			$TCA['tx_a3ekt_domain_model_povezavepogojev'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_povezavepogojev',
					'label' => 'tip',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/PovezavePogojev.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_povezavepogojev.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_ektpovezavepogoj', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_ektpovezavepogoj.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_ektpovezavepogoj');
			$TCA['tx_a3ekt_domain_model_ektpovezavepogoj'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_ektpovezavepogoj',
					'label' => 'opomba',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/EKTPovezavePogoj.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_ektpovezavepogoj.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_povezavepoklicev', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_povezavepoklicev.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_povezavepoklicev');
			$TCA['tx_a3ekt_domain_model_povezavepoklicev'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_povezavepoklicev',
					'label' => 'tip',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/PovezavePoklicev.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_povezavepoklicev.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_ektpovezavepoklicev', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_ektpovezavepoklicev.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_ektpovezavepoklicev');
			$TCA['tx_a3ekt_domain_model_ektpovezavepoklicev'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_ektpovezavepoklicev',
					'label' => 'opomba',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/EKTPovezavePoklicev.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_ektpovezavepoklicev.gif'
				),
			);

			t3lib_extMgm::addLLrefForTCAdescr('tx_a3ekt_domain_model_dovoljenjevtujini', 'EXT:a3_ekt/Resources/Private/Language/locallang_csh_tx_a3ekt_domain_model_dovoljenjevtujini.xml');
			t3lib_extMgm::allowTableOnStandardPages('tx_a3ekt_domain_model_dovoljenjevtujini');
			$TCA['tx_a3ekt_domain_model_dovoljenjevtujini'] = array(
				'ctrl' => array(
					'title'	=> 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini',
					'label' => 'naziv_dovoljenja',
					'tstamp' => 'tstamp',
					'crdate' => 'crdate',
					'cruser_id' => 'cruser_id',
					'dividers2tabs' => TRUE,
					'versioningWS' => 2,
					'versioning_followPages' => TRUE,
					'origUid' => 't3_origuid',
					'languageField' => 'sys_language_uid',
					'transOrigPointerField' => 'l10n_parent',
					'transOrigDiffSourceField' => 'l10n_diffsource',
					'delete' => 'deleted',
					'enablecolumns' => array(
						'disabled' => 'hidden',
						'starttime' => 'starttime',
						'endtime' => 'endtime',
					),
					'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/DovoljenjeVTujini.php',
					'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_a3ekt_domain_model_dovoljenjevtujini.gif'
				),
			);
            

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$tempColumns = array (
    'tx_a3ekt_sekundarne_vsebine_tip' => array (        
        'exclude' => 0,        
        'label' => 'Tip prikaza',
        'config' => array (
            'type' => 'select',    
            'minitems' => 0,
            'maxitems' => 1,
			'items' => array(
                array('Standardni okvir', 1),
                array('Modri okvir', 2),
                array('Okvir pomoč', 3),
			),
        )
    ),
);

if (TYPO3_MODE == 'BE' && !(TYPO3_REQUESTTYPE & TYPO3_REQUESTTYPE_INSTALL)) { 
    /**
    * Registers a Backend Module
    */
    Tx_Extbase_Utility_Extension::registerModule(
        $_EXTKEY,
        'tools',    // Make module a submodule of 'web'
        'ektsync',    // Submodule key
        'top', // Position
        array(
                // An array holding the controller-action-combinations that are accessible
            'EktSync'        => 'list'
        ),
        array(
            'access' => 'user,group',
            'icon'   => 'EXT:'.$_EXTKEY.'/Resources/Public/Icons/relation.gif',
            'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_mod.xml',
        )
    );
    
	/**
    * Registers a Backend Module
    */
    Tx_Extbase_Utility_Extension::registerModule(
        $_EXTKEY,
        'help',    // Make module a submodule of 'help'
        'help',    // Submodule key
        'top', // Position
        array(
                // An array holding the controller-action-combinations that are accessible
            'Help'        => 'helpOverview,helpPrimarne,helpSekundarne,helpOsnovno'
        ),
        array(
            'access' => 'user,group',
            'icon'   => 'EXT:a3_ekt/Resources/Public/Images/moduleicon.png',
            'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_mod2.xml',
        )
    );
    
}

t3lib_div::loadTCA('tt_content');
t3lib_extMgm::addTCAcolumns('tt_content',$tempColumns,1);
t3lib_extMgm::addToAllTCAtypes('tt_content','tx_a3ekt_sekundarne_vsebine_tip;;;;1-1-1');

$TCA['tx_a3ekt_domain_model_skddejavnost']['ctrl']['treeParentField'] = 'parent_s_k_d';
$TCA['tx_a3ekt_domain_model_dejavnost']['ctrl']['type'] = 'cezmejno_opravljanje';
$TCA['tx_a3ekt_domain_model_dejavnost']['ctrl']['label_alt'] = 'oznaka';
$TCA['tx_a3ekt_domain_model_dejavnost']['ctrl']['label_alt_force'] = TRUE;
$TCA['tx_a3ekt_domain_model_dovoljenjevtujini']['ctrl']['label_alt'] = 'oznaka';
$TCA['tx_a3ekt_domain_model_dovoljenjevtujini']['ctrl']['label_alt_force'] = TRUE;
$TCA['tx_a3ekt_domain_model_vloga']['ctrl']['label'] = 'naziv';
$TCA['tx_a3ekt_domain_model_vlogapriloga']['ctrl']['label'] = 'naziv';
$TCA['tx_a3ekt_domain_model_povezavepogojev']['ctrl']['sortby'] = 'sorting';
$TCA['tx_a3ekt_domain_model_povezavepoklicev']['ctrl']['sortby'] = 'sorting';
$TCA['tx_a3ekt_domain_model_vloga']['ctrl']['sortby'] = 'sorting';
$TCA['tx_a3ekt_domain_model_povezavepogojev']['ctrl']['type'] = 'tip';

$TCA['tx_a3ekt_domain_model_ektpovezavepogoj']['ctrl']['label'] = 'dejavnost';
//$TCA['tx_a3ekt_domain_model_ektpovezavepogoj']['ctrl']['sortby'] = 'tx_a3ekt_domain_model_dejavnost.naziv';

$TCA['tx_a3ekt_domain_model_ektpovezavepoklicev']['ctrl']['label'] = 'poklic';
//$TCA['tx_a3ekt_domain_model_ektpovezavepoklicev']['ctrl']['label_alt'] = 'pogoj';
//$TCA['tx_a3ekt_domain_model_ektpovezavepoklicev']['ctrl']['label_alt_force'] = TRUE;
$TCA['tx_a3ekt_domain_model_povezavepoklicev']['ctrl']['type'] = 'tip';
$TCA['tx_a3ekt_domain_model_dovoljenje']['ctrl']['label_alt'] = 'oznaka';
$TCA['tx_a3ekt_domain_model_dovoljenje']['ctrl']['label_alt_force'] = TRUE;
$TCA['tx_a3ekt_domain_model_poklic']['ctrl']['type'] = 'cezmejno_priznavanje_poklica';
$TCA['tx_a3ekt_domain_model_poklic']['ctrl']['label_alt'] = 'oznaka';
$TCA['tx_a3ekt_domain_model_poklic']['ctrl']['label_alt_force'] = TRUE;
$TCA['tx_a3ekt_domain_model_drugipogoj']['ctrl']['label_alt'] = 'oznaka';
$TCA['tx_a3ekt_domain_model_drugipogoj']['ctrl']['label_alt_force'] = TRUE;
$TCA['tx_a3ekt_domain_model_cezmejnodovoljenje']['ctrl']['label_alt'] = 'oznaka';
$TCA['tx_a3ekt_domain_model_cezmejnodovoljenje']['ctrl']['label_alt_force'] = TRUE;
$TCA['tx_a3ekt_domain_model_cezmejnoopravljenjepoklica']['ctrl']['label_alt'] = 'oznaka';
$TCA['tx_a3ekt_domain_model_cezmejnoopravljenjepoklica']['ctrl']['label_alt_force'] = TRUE;
$TCA['tx_a3ekt_domain_model_povezavepoklicev']['ctrl']['label'] = 'poklic';
$TCA['tx_a3ekt_domain_model_povezavepoklicev']['ctrl']['label_alt'] = 'dovoljenje';
$TCA['tx_a3ekt_domain_model_povezavepoklicev']['ctrl']['label_alt_force'] = TRUE;
$TCA['tx_a3ekt_domain_model_povezavepogojev']['ctrl']['label_userFunc'] = 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->povezavePogojevTitle';
$TCA['tx_a3ekt_domain_model_povezavepoklicev']['ctrl']['label_userFunc'] = 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->povezavePoklicevTitle';
$TCA['tx_a3ekt_domain_model_ektdejavnost']['ctrl']['label_userFunc'] = 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->nazivTitle';
$TCA['tx_a3ekt_domain_model_dejavnost']['ctrl']['label_userFunc'] = 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->nazivTitle';
$TCA['tx_a3ekt_domain_model_ektdejavnost']['ctrl']['searchFields'] = 'naziv';
$TCA['tx_a3ekt_domain_model_dejavnost']['ctrl']['searchFields'] = 'naziv';
$TCA['tx_a3ekt_domain_model_dovoljenje']['ctrl']['searchFields'] = 'naziv';
$TCA['tx_a3ekt_domain_model_poklic']['ctrl']['searchFields'] = 'naziv';
$TCA['tx_a3ekt_domain_model_organ']['ctrl']['searchFields'] = 'naziv';
$TCA['tx_a3ekt_domain_model_pravnapodlaga']['ctrl']['searchFields'] = 'naziv';
$TCA['tx_a3ekt_domain_model_sorodnepovezave']['ctrl']['searchFields'] = 'naziv';
$TCA['tx_a3ekt_domain_model_skddejavnost']['ctrl']['searchFields'] = 'deskriptor';
$TCA['tx_a3ekt_domain_model_sektor']['ctrl']['searchFields'] = 'naziv';
$TCA['tx_a3ekt_domain_model_kontaktnaoseba']['ctrl']['searchFields'] = 'ime_priimek';
$TCA['tx_a3ekt_domain_model_vloga']['ctrl']['searchFields'] = 'zaporedna_stevilka';
$TCA['tx_a3ekt_domain_model_vlogapriloga']['ctrl']['searchFields'] = 'stevilka_vloge';
$TCA['tx_a3ekt_domain_model_drugipogoj']['ctrl']['searchFields'] = 'naziv';
$TCA['tx_a3ekt_domain_model_registerdovoljenj']['ctrl']['searchFields'] = 'naziv';
$TCA['tx_a3ekt_domain_model_cezmejnodovoljenje']['ctrl']['searchFields'] = 'naziv';
$TCA['tx_a3ekt_domain_model_dovoljenjevtujini']['ctrl']['searchFields'] = 'naziv';

/*** Sync stuff - start ***/

$GLOBALS['a3_ekt']['dontSyncColumns'] = array(
	'step01', 'step02', 'step03', 'step04', 'step05', 'step06', 'step07', 'step08', 'step09', 'step10', 'step11',
	'step99',
	'step08_0', 'step08_1', 'step08_2', 'step08_3', 'step08_99',
	'step09_0', 'step09_1', 'step09_2', 'step09_3', 'step09_4', 'step09_5', 'step09_99',
	'stran', 'sekundarne_vsebine',
	'tx_externalimporttut_externalid',
	'endtime', 'l10n_diffsource', 'l10n_parent', 'line', 'spacer', 'starttime', 'sys_language_uid', 't3ver_label'
);

// global add externalid field function
if ( !function_exists(a3_ekt_addExternalidColumn) ) {
	function a3_ekt_addExternalidColumn($table) {
		global $TCA, $GLOBALS;
	
			// Load description of table tt_news
		t3lib_div::loadTCA($table);

			// Add a new column for containing the external id
		$tempColumns = array(
			'tx_externalimporttut_externalid' => array(
				'exclude' => 0,
				'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_externalimporttut_ttnews.externalid',
				'config' => array(
					'type' => 'input',
					'size' => '5',
					'readOnly' => 1
				)
			),
		);
		t3lib_extMgm::addTCAcolumns($table, $tempColumns, 1);
		t3lib_extMgm::addToAllTCAtypes($table, 'tx_externalimporttut_externalid');

		$TCA[$table]['columns']['tx_externalimporttut_externalid']['external'] = array(
			0 => array(
				'field' => 'uid'
			)
		);
	
		return true;
	}
}

if ( !function_exists(a3_ekt_mappingProcedure) ) {
	// global mapping function
	function a3_ekt_mappingProcedure($table) {
		global $TCA, $GLOBALS;
	
		//
		// go thru all fields and map them (exclude dontSyncColumns)
		// if the field is relation to another field, mark the reference_field to tx_externalimporttut_externalid
		//
		foreach($TCA[$table]['columns'] as $column_key => $column_value) {
		
			// if not in dontSyncColumns do some kind of mapping
			if ( !in_array($column_key, $GLOBALS['a3_ekt']['dontSyncColumns']) ) {

				// relational or regular mapping ?

				// relational mapping (use the tx_externalimporttut_externalid to lookup old uid and use the new uid)
				$foreign_table = $TCA[$table]['columns'][$column_key]['config']['foreign_table'];
				if ( isset($foreign_table) ) {

					// everything is setup properly, use the externalid for the mapping uid
					if ( is_array($TCA[$foreign_table]['ctrl']['external']) ) {

						$TCA[$table]['columns'][$column_key]['external'] = array(
							0 => array(
								'field' => $column_key,
								'mapping' => array(
									'table' => $TCA[$table]['columns'][$column_key]['config']['foreign_table'],
									'reference_field' => 'tx_externalimporttut_externalid'
								),
								'disabledOperations' => 'update'
							)
						);

					// use regular mapping, but mark that it probably should be a relational mapping since there
					// is a relation to another table in the TCA definition
					} else {

						$TCA[$table]['columns'][$column_key]['external'] = array(
							0 => array(
								'field' => $column_key,
								'shouldntThisBeRelationalMapping?' => true,
								'disabledOperations' => 'update'
							)
						);

					}

				// regular mapping ... string mapping
				} else {

					$TCA[$table]['columns'][$column_key]['external'] = array(
						0 => array(
							'field' => $column_key,
							'disabledOperations' => 'update'
						)
					);
				}

			}
		}
	
		return true;
	}
}

if ( !function_exists(a3_ekt_markTableExternal) ) {
	// global mapping function
	function a3_ekt_markTableExternal($table, $priority) {
		global $TCA, $GLOBALS;
	
		$TCA[$table]['ctrl']['external'] = array(
			0 => array(
				'connector' => 'sql',
				'parameters' => array(
					'driver' => 'mysql',
					'user' => TYPO3_db_username,
					'password' => TYPO3_db_password,
					'database' => TYPO3_db,
					'init' => 'SET CHARACTER SET utf8;',
					'query' => "SELECT * FROM ".$table." WHERE NOT deleted AND tx_externalimporttut_externalid = ''",
				),
				'data' => 'array',
				'reference_uid' => 'tx_externalimporttut_externalid',
				'where_clause' => "",
				'priority' => $priority,
				'disabledOperations' => 'delete',
				'description' => 'Import of all '.$table.' records'
			)
		);
	
		return true;
	}
}

a3_ekt_markTableExternal('tx_a3ekt_domain_model_posta', 10);

a3_ekt_markTableExternal('tx_a3ekt_domain_model_skddejavnost', 15);

a3_ekt_markTableExternal('tx_a3ekt_domain_model_organ', 20);
a3_ekt_markTableExternal('tx_a3ekt_domain_model_pravnapodlaga', 20);
a3_ekt_markTableExternal('tx_a3ekt_domain_model_vloga', 20);
a3_ekt_markTableExternal('tx_a3ekt_domain_model_kontaktnaoseba', 20);
a3_ekt_markTableExternal('tx_a3ekt_domain_model_korakipostopka', 20);
a3_ekt_markTableExternal('tx_a3ekt_domain_model_vlogapriloga', 20);
a3_ekt_markTableExternal('tx_a3ekt_domain_model_registerdovoljenj', 20);
a3_ekt_markTableExternal('tx_a3ekt_domain_model_stroskipostopka', 20);
a3_ekt_markTableExternal('tx_a3ekt_domain_model_sorodnepovezave', 20);
a3_ekt_markTableExternal('tx_a3ekt_domain_model_cezmejnodovoljenje', 20);

a3_ekt_markTableExternal('tx_a3ekt_domain_model_sektor', 25);

a3_ekt_markTableExternal('tx_a3ekt_domain_model_ektdejavnost', 30);
a3_ekt_markTableExternal('tx_a3ekt_domain_model_drugipogoj', 30);

a3_ekt_markTableExternal('tx_a3ekt_domain_model_cezmejnoopravljenjepoklica', 40);

a3_ekt_markTableExternal('tx_a3ekt_domain_model_dovoljenjevtujini', 50);
a3_ekt_markTableExternal('tx_a3ekt_domain_model_poklic', 50);
a3_ekt_markTableExternal('tx_a3ekt_domain_model_dejavnost', 50);
a3_ekt_markTableExternal('tx_a3ekt_domain_model_dovoljenje', 50);

a3_ekt_markTableExternal('tx_a3ekt_domain_model_ektpovezavepogoj', 90);
a3_ekt_markTableExternal('tx_a3ekt_domain_model_ektpovezavepoklicev', 90);

a3_ekt_markTableExternal('tx_a3ekt_domain_model_povezavepogojev', 100);
a3_ekt_markTableExternal('tx_a3ekt_domain_model_povezavepoklicev', 100);

/*** Sync stuff - end ***/

$TCA['ekt_descriptions'] = array();
$TCA['ekt_descriptions']['dejavnost'] = array(
	'step01' => 'Iz seznama izberite naziv regulirane dejavnosti. V primeru, da dejavnosti ni na seznamu, izberite "Ni na seznamu" in izpolnite polje 02. Naziv regulirane dejavnosti. Če ste naziv dejavnosti izbrali iz seznama dejavnosti v polju 01., vam polja 02. ni potrebno izpolniti.',
	'step02' => 'Če naziva dejavnosti niste našli v seznamu dejavnosti polja 01., navedite svoj naziv regulirane dejavnosti. Pri tem upoštevajte izraze, ki se uporabljajo v praksi.',
	'step03' => 'Na kratko v obliki definicije opišite dejavnost. Ponudniku dejavnosti jasno predstavite, kaj bo z registracijo te dejavnosti na trgu lahko opravljal. Opis naj bo poljuden in jedrnat. ',
	'step04' => '',
	'step05' => 'Iz seznama izberite organ, ki je pristojen za zakonodajo in druge predpise s področja obravnavane dejavnosti. Nato kliknite na povezavo na desni strani "Ustvari nov kontakt" in v polja vnesite podatke o kontaktu.',
	'step06' => 'Izberite pravne akte (zakone in podzakonske akte), ki predpisujejo pogoje za opravljanje dejavnosti. ',
	'step07' => 'Za potrebe iskalnika, ki bo na portalu uporabniku omogočal iskanje informacij o dejavnosti, navedite do tri pojme ali besedne zveze, ki se povezujejo z dejavnostjo (npr. frizerska dejavnost-fizer, frizerski salon, nega las, frizerstvo).',
	'step08' => 'Kliknite na povezavo "Ustvari povezavo" in v polje vnesite naziv  in naslov spletnih povezav, ki bodo uporabniku omogočale dostop do dodatnih informacij o obravnavani dejavnosti. Navedite do tri spletne povezave. ',
	'step09' => 'Izberite organ, ki je pristojen za izvedbo postopka za pridobitev dovoljenja. V primeru, da organa ni na seznamu, izberite -- Ni organa -- in pošljite mail na urednistvi@eugo.gov.si, vpišite podatke o kontaktu.',
	'step10' => 'Izberite eno izmed možnosti',
	'step10_0' => 'Za nekatere dejavnosti kljub določbam poklicne ali storitvene direktive velja, da jih ni možno opravljati občasno / čezmejno.',
	'step10_1' => 'Dejavnost je možno opravljati občasno / čezmejno, nacionalni predpisi države gostiteljice pa ne predpisujejo prijave opravljanja storitve oziroma drugih pogojev za tujega ponudnika.',
	'step10_2' => 'Država gostiteljica lahko od ponudnika zahteva izpolnjevanje pogojev za čezmejno opravljanje dejavnosti, za kar ponudnik potrebuje dovoljenje.',
	'step10_3' => 'Država gostiteljica lahko od ponudnika zahteva, da jo obvesti o opravljanju čezmejne dejavnosti s prijavo, ne zahteva pa dodatnih dovoljenj. ',
	'step10_99' => 'Izpolnite polje Obrazložitev.',
	'step11' => 'Kratek opis pogojev'
);
$TCA['ekt_descriptions']['poklic'] = array(
	'step01' => 'Navedite naziv reguliranega poklica oziroma poklicne kvalifikacije.',
	'step02' => 'Na kratko, v obliki definicije opišite, katere storitve se lahko opravlja s pridobljenim poklicem oziroma poklicno kvalifikacijo. Opis naj bo poljuden in jedrnat, saj želimo, da ga razume tudi oseba, ki se prvič srečuje s to dejavnostjo.',
	'step03' => 'Iz seznama izberite organ, ki je pristojen za zakonodajo in druge predpise s področja obravnavanega poklica. Nato kliknite na povezavo na desni strani "Ustvari nov kontakt" in v polja vnesite podatke o kontaktu.',
	'step04' => 'Izberite pravne akte (zakone in podzakonske akte), ki predpisujejo pogoje za opravljanje poklica.',
	'step05' => 'Za potrebe iskalnika, ki bo na portalu uporabniku omogočal iskanje informacij o poklicni kvalifikaciji/reguliranem poklicu, navedite do tri pojme ali besedne zveze, ki se povezujejo s poklicem.',
	'step06' => 'Kliknite na povezavo "Ustvari povezavo" in v polje vnesite naziv  in naslov spletnih povezav, ki bodo uporabniku omogočale dostop do dodatnih informacij o obravnavanem poklicu. Navedite do tri spletne povezave.',
	'step07' => ''
);
$TCA['ekt_descriptions']['dovoljenje'] = array(
	'step01' => '',
	'step02' => 'Na kratko opišite dovoljenje in katere pravice pridobi ponudnik s pridobitvijo dovoljenja. Opis naj bo poljuden in jedrnat, saj želimo, da ga razume tudi oseba, ki se prvič sreča s to dejavnostjo.',
	'step03' => 'Iz seznama izberite organ, ki je pristojen za vodenje postopka in izdajo dovoljenja. Nato kliknite na povezavo na desni strani "Ustvari nov kontakt" in v polja vnesite podatke o kontaktu.',
	'step04' => 'Kliknite na povezavo "Ustvari povezavo na vlogo" in v polja vnesite nazive in spletne naslove vlog v vseh jezikih, ki so na voljo uporabniku. V primeru, da obstaja več vlog za eno dovoljenje, obvezno vnesite zaporedno številko vloge.',
	'step05' => 'Kliknite na povezavo "Ustvari prilogo" in v polje vnesite nazive prilog in izberite način pridobitve. V primeru, da ste v polju 04 navedli več vlog za pridobitev dovoljenja, navedite tudi zaporedno številko vloge na katero se nanaša priloga.',
	'step06' => 'Kliknite na povezavo "dodaj strošek" in navedite naziv stroška (npr. upravne takse, stroški izdaje dovoljenja), znesek in izberite pravno podlago za odmero stroškov.',
	'step07' => 'Kliknite na povezavo ""dodaj korak"" in izberite iz seznama korak v postopku ter k vsakemu koraku dodajte terminski rok v katerem se mora posamezna aktivnost opraviti.',
	'step08' => 'V primeru, da v zvezi z dovoljenjem obstajajo še kakšne druge aktivnosti, kot je na primer članstvo ali obnova dovoljenja, jih navedite.',
	'step09' => '',
	'step10' => 'Izberite pravne akte (zakone in podzakonske akte), ki predpisujejo in obravnavajo postopek za pridobitev dovoljenja.',
	'step11' => 'Vnesite spletno povezavo do registra (seznama, kataloga ...) dovoljenja.'
);
$TCA['ekt_descriptions']['dovoljenjevtujini'] = array(
	'step01' => 'Navedite naziv dovoljenja (primer: odločba o priznanju poklicnih kvalifikacij za regulirani poklic odgovorni projektant).',
	'step02' => 'Vpišite način priznavanja poklicne kvalifikacije, pridobljene v tujini.',
	'step03' => 'Iz seznama izberite organ, ki je pristojen za vodenje postopka in izdajo dovoljenja. Nato kliknite na povezavo na desni strani "Ustvari nov kontakt" in v polja vnesite podatke o kontaktu.',
	'step04' => 'Kliknite na povezavo "Ustvari povezavo na vlogo" in v polja vnesite nazive in spletne naslove vlog v vseh jezikih, ki so na voljo uporabniku. V primeru, da obstaja več vlog za eno dovoljenje, obvezno vnesite zaporedno številko vloge.',
	'step05' => 'Kliknite na povezavo "Ustvari prilogo" in v polje vnesite nazive prilog in izberite način pridobitve. V primeru, da ste v polju 04 navedli več vlog za pridobitev dovoljenja, navedite tudi zaporedno številko vloge na katero se nanaša priloga.',
	'step06' => 'Kliknite na povezavo "Dodaj strošek" in navedite naziv stroška (npr. upravne takse, stroški izdaje dovoljenja), znesek in izberite pravno podlago za odmero stroškov.',
	'step07' => 'Kliknite na povezavo "Dodaj korak" in izberite iz seznama korak v postopku ter k vsakemu koraku dodajte terminski rok v katerem se mora posamezna aktivnost opraviti.',
	'step08' => 'V primeru, da v zvezi z dovoljenjem obstajajo še kakšne druge aktivnosti, kot je na primer članstvo ali obnova dovoljenja, jih navedite.',
	'step09' => '',
	'step10' => 'Izberite pravne akte (zakone in podzakonske akte), ki predpisujejo in obravnavajo postopek za pridobitev dovoljenja.'
);
$TCA['ekt_descriptions']['cezmejnodovoljenje'] = array(
	'step01' => 'Navedite naziv dovoljenja, ki ga je potrebno pridobiti za čezmejno/občasno opravljanje dejavnosti.',
	'step02' => 'Iz seznama izberite organ, ki je pristojen za vodenje postopka in izdajo dovoljenja. Nato kliknite na povezavo na desni strani "Ustvari nov kontakt" in v polja vnesite podatke o kontaktu.',
	'step03' => 'Kliknite na povezavo "Ustvari povezavo na vlogo" in v polja vnesite nazive in spletne naslove vlog v vseh jezikih, ki so na voljo uporabniku. V primeru, da obstaja več vlog za eno dovoljenje, obvezno vnesite zaporedno številko vloge.',
	'step04' => 'Kliknite na povezavo "Ustvari prilogo" in v polje vnesite nazive prilog in izberite način pridobitve. V primeru, da ste v polju 04 navedli več vlog za pridobitev dovoljenja, navedite tudi zaporedno številko vloge na katero se nanaša priloga.',
	'step05' => 'Kliknite na povezavo "dodaj strošek" in navedite naziv stroška (npr. upravne takse, stroški izdaje dovoljenja), znesek in izberite pravno podlago za odmero stroškov.',
	'step06' => 'Kliknite na povezavo "dodaj korak" in izberite iz seznama korak v postopku ter k vsakemu koraku dodajte terminski rok v katerem se mora posamezna aktivnost opraviti.',
	'step07' => 'Izberite pravne akte (zakone in podzakonske akte), ki predpisujejo in obravnavajo postopek za pridobitev dovoljenja.',
	'step08' => '',
	'step09' => '',
);
$TCA['ekt_descriptions']['drugipogoj'] = array(
	'step01' => '',
	'step02' => 'Na kratko opišite pogoj.',
	'step03' => 'Izberite  organ, ki je pristojen za zakonodajo in druge predpise.Nato kliknite na povezavo na desni strani "Ustvari nov kontakt" in v polja vnesite podatke o kontaktu.',
	'step04' => 'Izberite pravne akte (zakone in podzakonske akte), ki predpisujejo in obravnavajo postopek za pridobitev dokazila o izpolnjevanju pogoja. ',
	'step05' => 'Navedite naziv potrdila, ki dokazuje izpolnjevanje pogoja',
	'step06' => 'Izberite organ pristojen za izdajo potrdila. V primeru, da poznate kontaktno osebo pri pristojnem organu, kliknite na povezavo na desni strani "Ustvari nov kontakt" in v polja vnesite podatke o kontaktu.',
	'step07' => '',
);
$TCA['ekt_descriptions']['cezmejnoopravljenjepoklica'] = array(
	'step01' => 'Navedite naziv dovoljenja, ki ga je potrebno pridobiti za čezmejno/občasno opravljanje poklicne kvalifikacije.',
	'step02' => 'Iz seznama izberite organ, ki je pristojen za vodenje postopka in izdajo dovoljenja. Nato kliknite na povezavo na desni strani "Ustvari nov kontakt" in v polja vnesite podatke o kontaktu.',
	'step03' => 'Kliknite na povezavo "Ustvari povezavo na vlogo" in v polja vnesite nazive in spletne naslove vlog v vseh jezikih, ki so na voljo uporabniku. V primeru, da obstaja več vlog za eno dovoljenje, obvezno vnesite zaporedno številko vloge.',
	'step04' => 'Kliknite na povezavo "Ustvari prilogo" in v polje vnesite nazive prilog in izberite način pridobitve. V primeru, da ste v polju 04 navedli več vlog za pridobitev dovoljenja, navedite tudi zaporedno številko vloge na katero se nanaša priloga.',
	'step05' => 'Kliknite na povezavo "dodaj strošek" in navedite naziv stroška (npr. upravne takse, stroški izdaje dovoljenja), znesek in izberite pravno podlago za odmero stroškov.',
	'step06' => 'Kliknite na povezavo "dodaj korak" in izberite iz seznama korak v postopku ter k vsakemu koraku dodajte terminski rok v katerem se mora posamezna aktivnost opraviti.',
	'step07' => 'Izberite pravne akte (zakone in podzakonske akte), ki predpisujejo in obravnavajo postopek za pridobitev dovoljenja.',
	'step08' => '',
	'step09' => '',
);
$TCA['ekt_descriptions']['cezmejnoopravljanjepoklica'] = array(
        'step01' => 'Navedite naziv dovoljenja, ki ga je potrebno pridobiti za čezmejno/občasno opravljanje poklica.',
        'step02' => 'Iz seznama izberite organ, ki je pristojen za vodenje postopka in izdajo dovoljenja. Nato kliknite na povezavo na desni strani "Ustvari nov kontakt" in v polja vnesite podatke o kontaktu.',
        'step03' => 'Kliknite na povezavo "Ustvari povezavo na vlogo" in v polja vnesite nazive in spletne naslove vlog v vseh jezikih, ki so na voljo uporabniku. V primeru, da obstaja več vlog za eno dovoljenje, obvezno vnesite zaporedno številko vloge.',
        'step04' => 'Kliknite na povezavo "Ustvari prilogo" in v polje vnesite nazive prilog in izberite način pridobitve. V primeru, da ste v polju 04 navedli več vlog za pridobitev dovoljenja, navedite tudi zaporedno številko vloge na katero se nanaša priloga.',
        'step05' => 'Kliknite na povezavo "dodaj strošek" in navedite naziv stroška (npr. upravne takse, stroški izdaje dovoljenja), znesek in izberite pravno podlago za odmero stroškov.',
        'step06' => 'Kliknite na povezavo "dodaj korak" in izberite iz seznama korak v postopku ter k vsakemu koraku dodajte terminski rok v katerem se mora posamezna aktivnost opraviti.',
        'step07' => 'Izberite pravne akte (zakone in podzakonske akte), ki predpisujejo in obravnavajo postopek za pridobitev dovoljenja.',
        'step08' => '',
        'step09' => '',
);
$TCA['ekt_descriptions']['ektpovezavepogoj'] = array(
        'step01' => 'Sestavite opise pogojev.',
        'step02' => 'Sem se vpišejo tekstovni opisi pogojev v kolikor jih ne moremo »sestaviti« z uporabo vmesnika v koraku 01.'
);
$TCA['ekt_descriptions']['ektpovezavepoklicev'] = array(
        'step01' => 'Sestavite opise pogojev.',
        'step02' => 'Sem se vpišejo tekstovni opisi pogojev v kolikor jih ne moremo »sestaviti« z uporabo vmesnika v koraku 01.'
);

require_once(t3lib_extMgm::extPath('a3_ekt').'class.tx_a3ekt_previewEugoRecord.php');
require_once(t3lib_extMgm::extPath('a3_ekt').'class.tx_a3ekt_previewInlineEugoRecord.php');
require_once(t3lib_extMgm::extPath('a3_ekt').'class.tx_a3ekt_postBeSave.php');

if (TYPO3_MODE=="BE")   {
    $TBE_STYLES['inDocStyles_TBEstyle'] .= '@import "../typo3conf/ext/a3_ekt/Resources/Public/Styles/be.css";';
}

?>