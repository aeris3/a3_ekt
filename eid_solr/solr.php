<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
*  Jernej Zorec <jzorec@aeris3.si>, Aeris3
*  Robert Ferencek <rferencek@aeris3.si>, Aeris3
*  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/


# TSFE initialization
tslib_eidtools::connectDB();
$pageId     = filter_var(t3lib_div::_GET('id'), FILTER_SANITIZE_NUMBER_INT);
$languageId = filter_var(
	t3lib_div::_GET('L'),
	FILTER_VALIDATE_INT,
	array('options' => array('default' => 0, 'min_range' => 0))
);

$TSFE = t3lib_div::makeInstance('tslib_fe', $GLOBALS['TYPO3_CONF_VARS'], $pageId, 0, TRUE);
$TSFE->initFEuser();
$TSFE->initUserGroups();
$TSFE->sys_page = t3lib_div::makeInstance('t3lib_pageSelect');
$TSFE->rootLine = $TSFE->sys_page->getRootLine($pageId, '');
$TSFE->initTemplate();
$TSFE->getConfigArray();
$TSFE->includeTCA();
$TSFE->sys_language_uid = $languageId;

$solrConfiguration = tx_solr_Util::getSolrConfiguration();

#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

# Building Query

// deprecated ... used in solr version 2.1.x ...: $site = tx_solr_Site::getSiteByPageId($pageId);
$q    = trim(t3lib_div::_GP('q'));
$lang = trim(t3lib_div::_GP('L'));

$ektQuery = t3lib_div::makeInstance('tx_a3ekt_EktQuery', $q);
$ektQuery->setUserAccessGroups(explode(',', $TSFE->gr_list));
$ektQuery->setSiteHashFilter('www.eugo.gov.si,app.eugo.sigov.si,eugo.gov.si,test.eugo.sigov.si,sbp.localhost,ekt.localhost,ekt.aeris3.si,sbp.aeris3.si,dev-ekt.aeris3.si');
// deprecated ... used in solr version 2.1.x ...: $site = tx_solr_Site::getSiteByPageId($pageId);

$language = 0;
if ($TSFE->sys_language_uid) {
	$language = $TSFE->sys_language_uid;
}
$ektQuery->addFilter('language:' . $language);
$ektQuery->setOmitHeader();

$additionalFilters = t3lib_div::_GET('filters');
if (!empty($additionalFilters)) {
	$additionalFilters = json_decode($additionalFilters);
	foreach ($additionalFilters as $additionalFilter) {
		$ektQuery->addFilter($additionalFilter);
	}
}

#--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

	// Search
$solr   = t3lib_div::makeInstance('tx_solr_ConnectionManager')->getConnectionByPageId(
	$pageId,
	$languageId
);

// building the types query from TS configuration
$fq_string = '';
foreach($solrConfiguration['index.']['queue.'] as $key => $type) {
	if ( substr($type['table'], 0, 8) == 'tx_a3ekt' ) {
		$fq_string .= '(type:"' . $type['table'] . '") OR ';
	}
}
$fq_string = rtrim($fq_string, ' OR');

$queryParameters = array(
	'fl'			=> 'title,type,url',
	'fq'			=> $fq_string
);

$returnData = json_decode($solr->search('title:*'.$q.'*', 0, 5, $queryParameters)->getRawResponse());

// cleaning json type so we can use it in javascript later
foreach($returnData->response->docs as $doc) {
	$doc->type = str_replace('tx_a3ekt_domain_model_', '', $doc->type);
	if ( $lang == '1' ) {
		switch($doc->type) {
			case 'poklic':
				$doc->type = 'profession';
			break;
			case 'dovoljenje':
				$doc->type = 'permit';
			break;
			case 'dejavnost':
				$doc->type = 'activity';
			break;
		}
	}
}
$ajaxReturnData = json_encode($returnData->response->docs);

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-cache, must-revalidate');
header('Pragma: no-cache');
header('Content-Length: ' . strlen($ajaxReturnData));
header('Content-Type: application/json; charset=utf-8');
header('Content-Transfer-Encoding: 8bit');
echo $ajaxReturnData;

?>