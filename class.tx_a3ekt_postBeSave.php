<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2012 Bogdan Štelcar <bstelcar@aeris3.si>, Aeris3
*  Jernej Zorec <jzorec@aeris3.si>, Aeris3
*  Robert Ferencek <rferencek@aeris3.si>, Aeris3
*  Gregor Kirbiš <gkirbis@aeris3.si>, Aeris3
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * A query specialized to get search suggestions
 *
 * @author	Jernej Zorec <jzorec@aeris3.si>, Aeris3
 * @package a3_ekt
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 */
class tx_a3ekt_postBeSave {

	public function processDatamap_preProcessFieldArray (&$incomingFieldArray, $table, $id, $obj) {

		if ($incomingFieldArray['__cezmejno_opravljanje'] != null && $incomingFieldArray['dejavnost'] > 0) {

            //debug($incomingFieldArray, "dejavnost on save");

            $dejavnostRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_DejavnostRepository');
        	$query = $dejavnostRepository->createQuery();
        	$query->getQuerySettings()->setRespectStoragePage(false);
        	$query->getQuerySettings()->setReturnRawQueryResult(true);
        	$query->statement("
        		UPDATE
        			tx_a3ekt_domain_model_dejavnost 
        		SET 
        			cezmejno_opravljanje=".intval($incomingFieldArray['__cezmejno_opravljanje']).",
                    organ_za_prijavo_brez_postopka=".intval($incomingFieldArray['__organ_za_prijavo_brez_postopka']).",
                    cezmejno_opravljenje_obrazlozitev='".$incomingFieldArray['__cezmejno_opravljenje_obrazlozitev']."'
        		WHERE 
        			uid=".intval($incomingFieldArray['dejavnost'])
        	)->execute();
            //prijava_s_postopkom_pridobitve_dovoljenja=".intval($incomingFieldArray['__prijava_s_postopkom_pridobitve_dovoljenja']).",

            //pogoji
            $pid = $query->statement("
                SELECT
                    pid
                FROM
                    tx_a3ekt_domain_model_dejavnost 
                WHERE 
                    uid=".intval($incomingFieldArray['dejavnost'])
            )->execute();

            $pid = $pid[0]['pid'];

            $pogojRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository');
            $query = $pogojRepository->createQuery();
            $query->getQuerySettings()->setRespectStoragePage(false);
            $query->getQuerySettings()->setReturnRawQueryResult(true);

            //čezmejno
            $pogoji = explode(",", $incomingFieldArray['__pogoj']);
            
            foreach($pogoji as $pogoj) {

                //if new record
                if(strpos($pogoj, "NEW")!==false){
                    $data = $query->statement("
                        INSERT INTO 
                            tx_a3ekt_domain_model_povezavepogojev
                            (pid,ektpovezavepogoj,tip)
                        VALUES
                            (".intval($pid).",".intval($id).",3)
                    ")->execute();
                }
            }
        }

        //poklic
        if ($incomingFieldArray['__cezmejno_opravljanje'] != null && $incomingFieldArray['poklic'] > 0) {

            //debug($incomingFieldArray, "poklic on save");

            $poklicRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PoklicRepository');
            $query = $poklicRepository->createQuery();
            $query->getQuerySettings()->setRespectStoragePage(false);
            $query->getQuerySettings()->setReturnRawQueryResult(true);
            $query->statement("
                UPDATE
                    tx_a3ekt_domain_model_poklic 
                SET 
                    cezmejno_priznavanje_poklica=".intval($incomingFieldArray['__cezmejno_opravljanje']).",
                    organ_za_prijavo_brez_postopka=".intval($incomingFieldArray['__organ_za_prijavo_brez_postopka']).",
                    cezmejno_priznavanje_obrazlozitev='".$incomingFieldArray['__cezmejno_opravljenje_obrazlozitev']."'
                WHERE 
                    uid=".intval($incomingFieldArray['poklic'])
            )->execute();
            //prijava_s_postopkom_pridobitve_dovoljenja=".intval($incomingFieldArray['__prijava_s_postopkom_pridobitve_dovoljenja']).",

            //pogoji
            $pid = $query->statement("
                SELECT
                    pid
                FROM
                    tx_a3ekt_domain_model_poklic 
                WHERE 
                    uid=".intval($incomingFieldArray['poklic'])
            )->execute();

            $pid = $pid[0]['pid'];

            $pogojRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository');
            $query = $pogojRepository->createQuery();
            $query->getQuerySettings()->setRespectStoragePage(false);
            $query->getQuerySettings()->setReturnRawQueryResult(true);

            //čezmejno
            $pogoji = explode(",", $incomingFieldArray['__pogoj']);
            
            foreach($pogoji as $pogoj) {

                //if new record
                if(strpos($pogoj, "NEW")!==false){
                    $data = $query->statement("
                        INSERT INTO 
                            tx_a3ekt_domain_model_povezavepoklicev
                            (pid,ektpovezavepoklicev,tip)
                        VALUES
                            (".intval($pid).",".intval($id).",2)
                    ")->execute();
                }
            }

            //dovoljenje v tujini
            $pogoji = explode(",", $incomingFieldArray['__pogoj1']);
            
            foreach($pogoji as $pogoj) {

                //if new record
                if(strpos($pogoj, "NEW")!==false){
                    $data = $query->statement("
                        INSERT INTO 
                            tx_a3ekt_domain_model_povezavepoklicev
                            (pid,ektpovezavepoklicev,tip)
                        VALUES
                            (".intval($pid).",".intval($id).",4)
                    ")->execute();
                }
            }
        }

		// clean __ fields
		foreach ($incomingFieldArray as $key => $value) {
			$length = strlen('__');

        	if ( substr( $key, 0, $length ) === '__' ) {
            	unset($incomingFieldArray[$key]);
        	}
		}

        

	}
	
	public function customFormDejavnosti($PA, $fobj) {

		$dejavnostRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_DejavnostRepository');
		$query = $dejavnostRepository->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setReturnRawQueryResult(true);

        $cezmejnoOpravljanje = $query->statement("
        	SELECT
        		cezmejno_opravljanje
        	FROM
        		tx_a3ekt_domain_model_dejavnost 
        	WHERE 
        		uid=".intval($PA['row']['dejavnost'])
        )->execute();

        $cezmejnoOpravljanje = $cezmejnoOpravljanje[0]['cezmejno_opravljanje'];

        //selected item
        $formChecked = array();
        switch($cezmejnoOpravljanje) {
        	case 0:
        		$formChecked[0] = "checked";
        		break;
        	case 1:
        		$formChecked[1] = "checked";
        		break;
        	case 2:
        		$formChecked[2] = "checked";
        		break;
        	case 3:
        		$formChecked[3] = "checked";
        		break;
        	case 99:
        		$formChecked[99] = "checked";
        		break;
        	default:
        		die("error");
        }

        //form
		$items = '
			<input onclick="'.$PA['fieldChangeFunc']['TBE_EDITOR_fieldChanged'].'if (confirm(TBE_EDITOR.labels.onChangeAlert) &amp;&amp; TBE_EDITOR.checkSubmit(-1)){ TBE_EDITOR.submitForm() };" class="radio" type="radio" name="'.$PA['itemFormElName'].'" value="0" id="'.$PA['itemFormElID'].'_0" '.$formChecked[0].'/>
			<label for="'.$PA['itemFormElID'].'_0">Ni možno</label><br />
			
			<input onclick="'.$PA['fieldChangeFunc']['TBE_EDITOR_fieldChanged'].'if (confirm(TBE_EDITOR.labels.onChangeAlert) &amp;&amp; TBE_EDITOR.checkSubmit(-1)){ TBE_EDITOR.submitForm() };" class="radio" type="radio" name="'.$PA['itemFormElName'].'" value="1" id="'.$PA['itemFormElID'].'_1" '.$formChecked[1].'/>
			<label for="'.$PA['itemFormElID'].'_1">Ni prijave</label><br />
			
			<input onclick="'.$PA['fieldChangeFunc']['TBE_EDITOR_fieldChanged'].'if (confirm(TBE_EDITOR.labels.onChangeAlert) &amp;&amp; TBE_EDITOR.checkSubmit(-1)){ TBE_EDITOR.submitForm() };" class="radio" type="radio" name="'.$PA['itemFormElName'].'" value="2" id="'.$PA['itemFormElID'].'_2" '.$formChecked[2].'/>
			<label for="'.$PA['itemFormElID'].'_2">Prijava s postopkom pridobitve dovoljenja</label><br />
			
			<input onclick="'.$PA['fieldChangeFunc']['TBE_EDITOR_fieldChanged'].'if (confirm(TBE_EDITOR.labels.onChangeAlert) &amp;&amp; TBE_EDITOR.checkSubmit(-1)){ TBE_EDITOR.submitForm() };" class="radio" type="radio" name="'.$PA['itemFormElName'].'" value="3" id="'.$PA['itemFormElID'].'_3" '.$formChecked[3].'/>
			<label for="'.$PA['itemFormElID'].'_3">Prijava brez postopka pridobitve dovoljenja</label><br />
			
			<input onclick="'.$PA['fieldChangeFunc']['TBE_EDITOR_fieldChanged'].'if (confirm(TBE_EDITOR.labels.onChangeAlert) &amp;&amp; TBE_EDITOR.checkSubmit(-1)){ TBE_EDITOR.submitForm() };" class="radio" type="radio" name="'.$PA['itemFormElName'].'" value="99" id="'.$PA['itemFormElID'].'_99" '.$formChecked[99].'/> 
			<label for="'.$PA['itemFormElID'].'_99">Drugo</label><br />

		';
		return $items;
	}

    public function customFormPoklici($PA, $fobj) {

        $poklicRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PoklicRepository');
        $query = $poklicRepository->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setReturnRawQueryResult(true);

        $cezmejnoOpravljanje = $query->statement("
            SELECT
                cezmejno_priznavanje_poklica
            FROM
                tx_a3ekt_domain_model_poklic 
            WHERE 
                uid=".intval($PA['row']['poklic'])
        )->execute();

        $cezmejnoOpravljanje = $cezmejnoOpravljanje[0]['cezmejno_priznavanje_poklica'];

        //selected item
        $formChecked = array();
        switch($cezmejnoOpravljanje) {
            case 0:
                $formChecked[0] = "checked";
                break;
            case 1:
                $formChecked[1] = "checked";
                break;
            case 2:
                $formChecked[2] = "checked";
                break;
            case 3:
                $formChecked[3] = "checked";
                break;
            case 99:
                $formChecked[99] = "checked";
                break;
            default:
                die("error");
        }

        //form
        $items = '
            <input onclick="'.$PA['fieldChangeFunc']['TBE_EDITOR_fieldChanged'].'if (confirm(TBE_EDITOR.labels.onChangeAlert) &amp;&amp; TBE_EDITOR.checkSubmit(-1)){ TBE_EDITOR.submitForm() };" class="radio" type="radio" name="'.$PA['itemFormElName'].'" value="0" id="'.$PA['itemFormElID'].'_0" '.$formChecked[0].'/>
            <label for="'.$PA['itemFormElID'].'_0">Ni možno</label><br />
            
            <input onclick="'.$PA['fieldChangeFunc']['TBE_EDITOR_fieldChanged'].'if (confirm(TBE_EDITOR.labels.onChangeAlert) &amp;&amp; TBE_EDITOR.checkSubmit(-1)){ TBE_EDITOR.submitForm() };" class="radio" type="radio" name="'.$PA['itemFormElName'].'" value="1" id="'.$PA['itemFormElID'].'_1" '.$formChecked[1].'/>
            <label for="'.$PA['itemFormElID'].'_1">Ni prijave</label><br />
            
            <input onclick="'.$PA['fieldChangeFunc']['TBE_EDITOR_fieldChanged'].'if (confirm(TBE_EDITOR.labels.onChangeAlert) &amp;&amp; TBE_EDITOR.checkSubmit(-1)){ TBE_EDITOR.submitForm() };" class="radio" type="radio" name="'.$PA['itemFormElName'].'" value="2" id="'.$PA['itemFormElID'].'_2" '.$formChecked[2].'/>
            <label for="'.$PA['itemFormElID'].'_2">Prijava s postopkom pridobitve dovoljenja</label><br />
            
            <input onclick="'.$PA['fieldChangeFunc']['TBE_EDITOR_fieldChanged'].'if (confirm(TBE_EDITOR.labels.onChangeAlert) &amp;&amp; TBE_EDITOR.checkSubmit(-1)){ TBE_EDITOR.submitForm() };" class="radio" type="radio" name="'.$PA['itemFormElName'].'" value="3" id="'.$PA['itemFormElID'].'_3" '.$formChecked[3].'/>
            <label for="'.$PA['itemFormElID'].'_3">Prijava brez postopka pridobitve dovoljenja</label><br />
            
            <input onclick="'.$PA['fieldChangeFunc']['TBE_EDITOR_fieldChanged'].'if (confirm(TBE_EDITOR.labels.onChangeAlert) &amp;&amp; TBE_EDITOR.checkSubmit(-1)){ TBE_EDITOR.submitForm() };" class="radio" type="radio" name="'.$PA['itemFormElName'].'" value="99" id="'.$PA['itemFormElID'].'_99" '.$formChecked[99].'/> 
            <label for="'.$PA['itemFormElID'].'_99">Drugo</label><br />

        ';
        return $items;
    }

    public function getMainFields_preProcess($s_table, &$a_row, $o_parent) {
        GLOBAL $TCA;

        if($a_row['dejavnost']!=NULL && $a_row['dejavnost']>0) {
            $dejavnostRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_DejavnostRepository');
            $query = $dejavnostRepository->createQuery();
            $query->getQuerySettings()->setRespectStoragePage(false);
            $query->getQuerySettings()->setReturnRawQueryResult(true);

            $data = $query->statement("
                SELECT
                    cezmejno_opravljanje, prijava_s_postopkom_pridobitve_dovoljenja, organ_za_prijavo_brez_postopka, cezmejno_opravljenje_obrazlozitev
                FROM
                    tx_a3ekt_domain_model_dejavnost 
                WHERE 
                    uid=".intval($a_row['dejavnost'])." AND not deleted AND not hidden"
            )->execute();

            $cezmejnoOpravljanje = $data[0]['cezmejno_opravljanje'];

            $TCA['tx_a3ekt_domain_model_ektpovezavepogoj']['types']['0'] = $TCA['tx_a3ekt_domain_model_ektpovezavepogoj']['types'][$cezmejnoOpravljanje];

            //$a_row['__prijava_s_postopkom_pridobitve_dovoljenja']=$data[0]['prijava_s_postopkom_pridobitve_dovoljenja'];
            $a_row['__organ_za_prijavo_brez_postopka']=$data[0]['organ_za_prijavo_brez_postopka'];
            $a_row['__cezmejno_opravljenje_obrazlozitev']=$data[0]['cezmejno_opravljenje_obrazlozitev'];

            //pogoji
            $pogoji = explode(",",$a_row['pogoj']);

            $pogojRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePogojevRepository');
            $query = $pogojRepository->createQuery();
            $query->getQuerySettings()->setRespectStoragePage(false);
            $query->getQuerySettings()->setReturnRawQueryResult(true);

            foreach($pogoji as $pogoj) {
                $data = $query->statement("
                    SELECT
                        tip
                    FROM
                        tx_a3ekt_domain_model_povezavepogojev
                    WHERE 
                        uid=".intval($pogoj)." AND not deleted"
                )->execute();

                if($data[0]['tip']=="3")
                    $customPogoji.=$pogoj.',';
                else 
                    $customPogoji2.=$pogoj.',';
            }
            $customPogoji = substr_replace($customPogoji ,"",-1);
            $customPogoji2 = substr_replace($customPogoji2 ,"",-1);
            
            $a_row['__pogoj'] = $customPogoji;
            $a_row['pogoj'] = $customPogoji2;

            //debug($a_row, "dejavnost on open");
        }

        //poklici
        if($a_row['poklic']!=NULL && $a_row['poklic']>0) {

            $poklicRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PoklicRepository');
            $query = $poklicRepository->createQuery();
            $query->getQuerySettings()->setRespectStoragePage(false);
            $query->getQuerySettings()->setReturnRawQueryResult(true);

            $data = $query->statement("
                SELECT
                    cezmejno_priznavanje_poklica, prijava_s_postopkom_pridobitve_dovoljenja, organ_za_prijavo_brez_postopka, cezmejno_priznavanje_obrazlozitev
                FROM
                    tx_a3ekt_domain_model_poklic
                WHERE 
                    uid=".intval($a_row['poklic'])." AND not deleted AND not hidden"
            )->execute();

            $cezmejnoOpravljanje = $data[0]['cezmejno_priznavanje_poklica'];

            $TCA['tx_a3ekt_domain_model_ektpovezavepoklicev']['types']['0'] = $TCA['tx_a3ekt_domain_model_ektpovezavepoklicev']['types'][$cezmejnoOpravljanje];

            //$a_row['__prijava_s_postopkom_pridobitve_dovoljenja']=$data[0]['prijava_s_postopkom_pridobitve_dovoljenja'];
            $a_row['__organ_za_prijavo_brez_postopka']=$data[0]['organ_za_prijavo_brez_postopka'];
            $a_row['__cezmejno_opravljenje_obrazlozitev']=$data[0]['cezmejno_priznavanje_obrazlozitev'];

            //pogoji
            $pogoji = explode(",",$a_row['pogoj']);

            $pogojRepository = t3lib_div::makeInstance('Tx_A3Ekt_Domain_Repository_PovezavePoklicevRepository');
            $query = $pogojRepository->createQuery();
            $query->getQuerySettings()->setRespectStoragePage(false);
            $query->getQuerySettings()->setReturnRawQueryResult(true);

            foreach($pogoji as $pogoj) {
                $data = $query->statement("
                    SELECT
                        tip
                    FROM
                        tx_a3ekt_domain_model_povezavepoklicev
                    WHERE 
                        uid=".intval($pogoj)." AND not deleted"
                )->execute();

                if($data[0]['tip']=="2")
                    $customPogoji.=$pogoj.',';
                elseif($data[0]['tip']=="4")
                    $customPogoji1.=$pogoj.',';
                else 
                    $customPogoji2.=$pogoj.',';
            }
            $customPogoji = substr_replace($customPogoji ,"",-1);
            $customPogoji1 = substr_replace($customPogoji1 ,"",-1);
            $customPogoji2 = substr_replace($customPogoji2 ,"",-1);
            
            $a_row['__pogoj'] = $customPogoji;
            $a_row['__pogoj1'] = $customPogoji1;
            $a_row['pogoj'] = $customPogoji2;

            //debug($a_row, "poklic on open");
        }
    }
}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/a3_ekt/class.tx_a3ekt_postBeSave.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/a3_ekt/class.tx_a3ekt_postBeSave.php']);
}

?>