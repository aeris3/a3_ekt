<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_a3ekt_domain_model_povezavepoklicev'] = array(
	'ctrl' => $TCA['tx_a3ekt_domain_model_povezavepoklicev']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, tip, pogoj, dovoljenje, cezmejno_dovoljenje, drugi_pogoj',
	),
	'types' => array(
		'0' => array('showitem' => 'tip'),
		'1' => array('showitem' => '--palette--;;main1, pogoj'),
		'2' => array('showitem' => '--palette--;;main2, pogoj'),
		'3' => array('showitem' => '--palette--;;main3, pogoj'),
		'4' => array('showitem' => '--palette--;;main4, pogoj'),
		'5' => array('showitem' => '--palette--;;main5, pogoj'),
		'98' => array('showitem' => 'tip'),
		'99' => array('showitem' => 'tip'),
	),
	'palettes' => array(
		'main1' => array('showitem' => 'tip, dovoljenje', 'canNotCollapse' => TRUE),
		'main2' => array('showitem' => 'tip, cezmejno_dovoljenje', 'canNotCollapse' => TRUE),
		'main3' => array('showitem' => 'tip, drugi_pogoj', 'canNotCollapse' => TRUE),
		'main4' => array('showitem' => 'tip, dovoljenje_v_tujini', 'canNotCollapse' => TRUE),
		'main5' => array('showitem' => 'tip, poklic', 'canNotCollapse' => TRUE),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_a3ekt_domain_model_povezavepoklicev',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_povezavepoklicev.pid=###CURRENT_PID### AND tx_a3ekt_domain_model_povezavepoklicev.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'tip' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_povezavepoklicev.tip',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('--- izberi ---', 0),
					array('--- ali ---', 99),
					array('--- sklop ---', 98),
					array('Dovoljenje', 1),
					array('Čezmejno dovoljenje', 2),
					array('Drugi pogoj', 3),
					array('Dovoljenje v tujini', 4),
					array('Poklic', 5),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => 'required'
			),
		),
		'dovoljenje' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_povezavepoklicev.dovoljenje',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_dovoljenje',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_dovoljenje.pid = ###CURRENT_PID###',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
				'wizards' => array(
					'_PADDING' => 5,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_dovoljenje',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'set'
						),
						'script' => 'wizard_add.php',
					),
					'edit' => array(
						'type' => 'popup',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
						'icon' => 'edit2.gif',
						'params' => array(
                            'table' => 'tx_a3ekt_domain_model_dovoljenje',
						),
						'script' => 'wizard_edit.php',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
					),
				
				),
			),
		),
		'pogoj' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_povezavepoklicev.pogoj',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_povezavepoklicev',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_povezavepoklicev.pid = ###CURRENT_PID###',
				'foreign_field' => 'povezavepoklicev',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'cezmejno_dovoljenje' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_povezavepoklicev.cezmejno_dovoljenje',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_cezmejnoopravljenjepoklica',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_cezmejnoopravljenjepoklica.pid = ###CURRENT_PID###',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
				'wizards' => array(
					'_PADDING' => 5,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_cezmejnoopravljenjepoklica',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'set'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
						'type' => 'popup',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
						'icon' => 'edit2.gif',
						'params' => array(
                            'table' => 'tx_a3ekt_domain_model_cezmejnoopravljenjepoklica',
						),
						'script' => 'wizard_edit.php',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
					),
				),
			),
		),
		'drugi_pogoj' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_povezavepoklicev.drugi_pogoj',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_drugipogoj',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_drugipogoj.pid = ###CURRENT_PID###',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
				'wizards' => array(
					'_PADDING' => 5,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_drugipogoj',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'set'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_drugipogoj',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
				),
			),
		),
		'dovoljenje_v_tujini' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_povezavepoklicev.dovoljenje_v_tujini',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_dovoljenjevtujini',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_dovoljenjevtujini.pid = ###CURRENT_PID###',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
				'wizards' => array(
					'_PADDING' => 5,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_dovoljenjevtujini',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'set'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_dovoljenjevtujini',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
				),
			),
		),
		'poklic' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_povezavepoklicev.poklic',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_poklic',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_poklic.pid = ###CURRENT_PID###',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
				'wizards' => array(
					'_PADDING' => 5,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_poklic',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'set'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_poklic',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
				),
			),
		),
		'ektpovezavepoklicev' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

// add column 'tx_externalimporttut_externalid'
a3_ekt_addExternalidColumn('tx_a3ekt_domain_model_povezavepoklicev');

// do the mapping procedure
a3_ekt_mappingProcedure('tx_a3ekt_domain_model_povezavepoklicev');

?>
