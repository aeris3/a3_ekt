<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_a3ekt_domain_model_stroskipostopka'] = array(
	'ctrl' => $TCA['tx_a3ekt_domain_model_stroskipostopka']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, naziv_stroska, znesek, naziv_predpisa, drugo',
	),
	'types' => array(
		'1' => array('showitem' => '--palette--;;main'),
	),
	'palettes' => array(
		'main' => array('showitem' => 'naziv_stroska, znesek, naziv_predpisa, drugo', 'canNotCollapse' => 1),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_a3ekt_domain_model_stroskipostopka',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_stroskipostopka.pid=###CURRENT_PID### AND tx_a3ekt_domain_model_stroskipostopka.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'naziv_stroska' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_stroskipostopka.naziv_stroska',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'znesek' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_stroskipostopka.znesek',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'double2,required'
			),
		),
		'naziv_predpisa' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_stroskipostopka.naziv_predpisa',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'drugo' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_stroskipostopka.drugo',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'dovoljenje' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'poklic' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'cezmejnodovoljenje' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

// add column 'tx_externalimporttut_externalid'
a3_ekt_addExternalidColumn('tx_a3ekt_domain_model_stroskipostopka');

// do the mapping procedure
a3_ekt_mappingProcedure('tx_a3ekt_domain_model_stroskipostopka');

?>