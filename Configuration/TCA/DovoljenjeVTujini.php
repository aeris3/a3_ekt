<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_a3ekt_domain_model_dovoljenjevtujini'] = array(
	'ctrl' => $TCA['tx_a3ekt_domain_model_dovoljenjevtujini']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, naziv_dovoljenja, oznaka, opis, nacin_priznavanja, obvezno_clanstvo, obnova, termin_obnove, opis_obnove, drugo, pristojni_organ, pristojni_organ_kontakt, vloge, priloge_k_vlogi, stroski_postopka, koraki_postopka, pravno_sredstvo, pritozbeni_organ, molk_organa, pravne_podlage_tujina',
	),
	'types' => array(
		'1' => array('showitem' => 'hidden;;1, step01, --palette--;;step01, step02, --palette--;;step02, step03, --palette--;;step03, step04, --palette--;;step04, step05, --palette--;;step05, step06, --palette--;;step06, step07, --palette--;;step07, step08, --palette--;;step08, step09, --palette--;;step09, step10, --palette--;;step10'),
	),
	'palettes' => array(
		'step01' => array('showitem' => 'naziv_dovoljenja, oznaka, --linebreak--, opis, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step02' => array('showitem' => 'nacin_priznavanja, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step03' => array('showitem' => 'pristojni_organ, pristojni_organ_kontakt, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step04' => array('showitem' => 'vloge, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step05' => array('showitem' => 'priloge_k_vlogi, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step06' => array('showitem' => 'stroski_postopka, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step07' => array('showitem' => 'koraki_postopka, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step08' => array('showitem' => 'obvezno_clanstvo, --linebreak--, obnova, --linebreak--, termin_obnove, --linebreak--, opis_obnove, --linebreak--, drugo, --linebreak--, spacer', 'canNotCollapse' => TRUE),
//		'step09' => array('showitem' => 'pritozbeni_organ, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step09' => array('showitem' => 'pravno_sredstvo, --linebreak--, pritozbeni_organ, --linebreak--, molk_organa, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step10' => array('showitem' => 'pravne_podlage_tujina, --linebreak--, spacer', 'canNotCollapse' => TRUE),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_a3ekt_domain_model_dovoljenjevtujini',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_dovoljenjevtujini.pid=###CURRENT_PID### AND tx_a3ekt_domain_model_dovoljenjevtujini.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'naziv_dovoljenja' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.naziv_dovoljenja',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'oznaka' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.oznaka',
			'config' => array(
				'type' => 'input',
				'size' => 15,
				'eval' => 'trim'
			),
			
		),
		'opis' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.opis',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
                'wizards' => array(
                    'RTE' => array(
                        'icon' => 'wizard_rte2.gif',
                        'notNewRecords'=> 1,
                        'RTEonly' => 1,
                        'script' => 'wizard_rte.php',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
                        'type' => 'script'
                    )
                )
			),
			'defaultExtras' => 'richtext[]:rte_transform[mode=ts]'
		),
		'nacin_priznavanja' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.nacin_priznavanja',
			'config' => array(
				'type' => 'radio',
				'items' => array(
					array('Avtomatično priznavanje', 1),
					array('Splošni sistem', 2),
					array('Priznavanje poklicnih izkušenj', 3),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'obvezno_clanstvo' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.obvezno_clanstvo',
			'config' => array(
				'type' => 'check',
			),
		),
		'obnova' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.obnova',
			'config' => array(
				'type' => 'check',
			),
		),
		'termin_obnove' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.termin_obnove',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'opis_obnove' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.opis_obnove',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
                'wizards' => array(
                    'RTE' => array(
                        'icon' => 'wizard_rte2.gif',
                        'notNewRecords'=> 1,
                        'RTEonly' => 1,
                        'script' => 'wizard_rte.php',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
                        'type' => 'script'
                    )
                )
			),
			'defaultExtras' => 'richtext[]:rte_transform[mode=ts]'
		),
		'drugo' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.drugo',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
                'wizards' => array(
                    'RTE' => array(
                        'icon' => 'wizard_rte2.gif',
                        'notNewRecords'=> 1,
                        'RTEonly' => 1,
                        'script' => 'wizard_rte.php',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
                        'type' => 'script'
                    )
                )
			),
			'defaultExtras' => 'richtext[]:rte_transform[mode=ts]'
		),
		'pristojni_organ' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.pristojni_organ',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_organ',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_organ.pid = ###CURRENT_PID###',
				'allowed' => 'tx_a3ekt_domain_model_organ',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
				'wizards' => array(
					'suggest' => array(    
						'type' => 'suggest',
						'default' => array(
							'searchWholePhrase' => 1,
							'addWhere' => 'AND pid = ###CURRENT_PID###'
						),
					),
					'_PADDING' => 5,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_organ',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_organ',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
				),
			),
		),
		'pristojni_organ_kontakt' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.pristojni_organ_kontakt',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_kontaktnaoseba',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_kontaktnaoseba.pid = ###CURRENT_PID###',
				'minitems' => 0,
				'maxitems' => 1,
				'appearance' => array(
					'collapse' => 1,
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'vloge' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.vloge',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_vloga',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_vloga.pid = ###CURRENT_PID###',
				'foreign_field' => 'dovoljenjevtujini',
				'foreign_sortby' => 'sorting',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1,
					'useSortable' => 1
				),
			),
		),
		'priloge_k_vlogi' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.priloge_k_vlogi',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_vlogapriloga',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_vlogapriloga.pid = ###CURRENT_PID###',
				'foreign_field' => 'dovoljenjevtujini',
				'foreign_sortby' => 'sorting',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1,
					'useSortable' => 1
				),
			),
		),
		'stroski_postopka' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.stroski_postopka',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_stroskipostopka',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_stroskipostopka.pid = ###CURRENT_PID###',
				'foreign_field' => 'dovoljenjevtujini',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'koraki_postopka' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.koraki_postopka',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_korakipostopka',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_korakipostopka.pid = ###CURRENT_PID###',
				'foreign_field' => 'dovoljenjevtujini',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'pritozbeni_organ' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.pritozbeni_organ',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_organ',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_organ.pid = ###CURRENT_PID###',
				'allowed' => 'tx_a3ekt_domain_model_organ',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
				'wizards' => array(
					'suggest' => array(    
						'type' => 'suggest',
						'default' => array(
							'searchWholePhrase' => 1,
							'addWhere' => 'AND pid = ###CURRENT_PID###'
						),
					),
					'_PADDING' => 5,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_organ',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_organ',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
				),
			),
		),
		'pravne_podlage_tujina' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.pravne_podlage_tujina',
			'config' => array(
				'type' => 'group',
				'foreign_table' => 'tx_a3ekt_domain_model_pravnapodlaga',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_pravnapodlaga.pid = ###CURRENT_PID###',
				'maxitems'      => 9999,
				'size' => 5,
                'internal_type' => 'db',
                'allowed' => 'tx_a3ekt_domain_model_pravnapodlaga',
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
				'items' => array(
					array('--- Izberi ---', 0),
				),
                'wizards' => array(
                    'suggest' => array(    
						'type' => 'suggest',
						'default' => array(
							'searchWholePhrase' => 1,
							'addWhere' => 'AND pid = ###CURRENT_PID###'
						),
					),
					'_PADDING' => 10,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_pravnapodlaga',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_pravnapodlaga',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
                ),
			),
		),
		'pravno_sredstvo' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.pravno_sredstvo',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('--- Izberi ---', 0),
					array('Pritožba', 1),
					array('Upravni sport', 2),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'molk_organa' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenjevtujini.molk_organa',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			),
		),
		'spacer' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->spacerField',
			),
		),
		'line' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->lineField',
			),
		),
		'step01' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '01. NAZIV DOVOLJENJA',
					'description' => $TCA['ekt_descriptions']['dovoljenjevtujini']['step01']
				),
			),
		),
		'step02' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '02. RAVEN KVALIFIKACIJE',
					'description' => $TCA['ekt_descriptions']['dovoljenjevtujini']['step02']
				),
			),
		),
		'step03' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '03. PRISTOJNI ORGAN',
					'description' => $TCA['ekt_descriptions']['dovoljenjevtujini']['step03']
				),
			),
		),
		'step04' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '04. VLOGA ZA PRIDOBITEV DOVOLJENJA',
					'description' => $TCA['ekt_descriptions']['dovoljenjevtujini']['step04']
				),
			),
		),
		'step05' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '05. PRILOGE K VLOGI',
					'description' => $TCA['ekt_descriptions']['dovoljenjevtujini']['step05']
				),
			),
		),
		'step06' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '06. STROŠKI POSTOPKA',
					'description' => $TCA['ekt_descriptions']['dovoljenjevtujini']['step06']
				),
			),
		),
		'step07' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '07. KORAKI V POSTOPKU',
					'description' => $TCA['ekt_descriptions']['dovoljenjevtujini']['step07']
				),
			),
		),
		'step08' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '08. DRUGE AKTIVNOSTI POVEZANE Z DOVOLJENJEM',
					'description' => $TCA['ekt_descriptions']['dovoljenjevtujini']['step08']
				),
			),
		),
		'step09' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '09. PRAVNO VARSTVO',
					'description' => $TCA['ekt_descriptions']['dovoljenjevtujini']['step09']
				),
			),
		),
		'step10' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '10. PRAVNA PODLAGA',
					'description' => $TCA['ekt_descriptions']['dovoljenjevtujini']['step10']
				),
			),
		),
	),
);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$TCA['tx_a3ekt_domain_model_dovoljenjevtujini']['columns']['hidden']['config']['default'] = '1';

// add column 'tx_externalimporttut_externalid'
a3_ekt_addExternalidColumn('tx_a3ekt_domain_model_dovoljenjevtujini');

// do the mapping procedure
a3_ekt_mappingProcedure('tx_a3ekt_domain_model_dovoljenjevtujini');

?>