<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_a3ekt_domain_model_poklic'] = array(
	'ctrl' => $TCA['tx_a3ekt_domain_model_poklic']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, naziv, oznaka, opis, kljucne_besede, pravno_sredstvo, opomba, zakonodajni_organ_kontakt, sorodne_povezave, pravne_podlage, zakonodajni_organ, cezmejno_priznavanje_poklica, cezmejno_priznavanje_obrazlozitev',
	),
	'types' => array(
		'0' => array('showitem' => 'hidden;;1, step01, --palette--;;step01, --linebreak--, step02, --palette--;;step02, step03, --palette--;;step03, step04, --palette--;;step04, step05, --palette--;;step05, step06, --palette--;;step06, step07, --palette--;;step07, step08_0, --palette--;;step08_0, step99, --palette--;;step99, line, --palette--;;step11, step12, --palette--;;step12'),
		'1' => array('showitem' => 'hidden;;1, step01, --palette--;;step01, --linebreak--, step02, --palette--;;step02, step03, --palette--;;step03, step04, --palette--;;step04, step05, --palette--;;step05, step06, --palette--;;step06, step07, --palette--;;step07, step08_1, --palette--;;step08_1, step99, --palette--;;step99, line, --palette--;;step11, step12, --palette--;;step12'),
		'2' => array('showitem' => 'hidden;;1, step01, --palette--;;step01, --linebreak--, step02, --palette--;;step02, step03, --palette--;;step03, step04, --palette--;;step04, step05, --palette--;;step05, step06, --palette--;;step06, step07, --palette--;;step07, step08_2, --palette--;;step08_2, step99, --palette--;;step99, line, --palette--;;step11, step12, --palette--;;step12'),
        '3' => array('showitem' => 'hidden;;1, step01, --palette--;;step01, --linebreak--, step02, --palette--;;step02, step03, --palette--;;step03, step04, --palette--;;step04, step05, --palette--;;step05, step06, --palette--;;step06, step07, --palette--;;step07, step08_3, --palette--;;step08_3, step99, --palette--;;step99, line, --palette--;;step11, step12, --palette--;;step12'),
		'99' => array('showitem' => 'hidden;;1, step01, --palette--;;step01, --linebreak--, step02, --palette--;;step02, step03, --palette--;;step03, step04, --palette--;;step04, step05, --palette--;;step05, step06, --palette--;;step06, step07, --palette--;;step07, step08_99, --palette--;;step08_99, step99, --palette--;;step99, line, --palette--;;step11, step12,, --palette--;;step12'),
	),
	'palettes' => array(
		'step01' => array('showitem' => 'naziv, oznaka, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step02' => array('showitem' => 'opis, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step03' => array('showitem' => 'zakonodajni_organ, zakonodajni_organ_kontakt, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step04' => array('showitem' => 'pravne_podlage, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step05' => array('showitem' => 'kljucne_besede, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step06' => array('showitem' => 'sorodne_povezave, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step07' => array('showitem' => 'opomba, --linebreak--, spacer', 'canNotCollapse' => TRUE),
        'step12' => array('showitem' => 'vir, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step08_0' => array('showitem' => 'cezmejno_priznavanje_poklica, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step08_1' => array('showitem' => 'cezmejno_priznavanje_poklica, --linebreak--, spacer', 'canNotCollapse' => TRUE),
        'step08_2' => array('showitem' => 'cezmejno_priznavanje_poklica, --linebreak--, spacer, --linebreak--, prijava_s_postopkom_pridobitve_dovoljenja, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step08_3' => array('showitem' => 'cezmejno_priznavanje_poklica, --linebreak--, spacer, --linebreak--, organ_za_prijavo_brez_postopka, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step08_99' => array('showitem' => 'cezmejno_priznavanje_poklica, --linebreak--, spacer, --linebreak--, cezmejno_priznavanje_obrazlozitev, --linebreak--, spacer', 'canNotCollapse' => TRUE),
        'step99' => array('showitem' => 'sekundarne_vsebine, --linebreak--, spacer', 'canNotCollapse' => TRUE),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_a3ekt_domain_model_poklic',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_poklic.pid=###CURRENT_PID### AND tx_a3ekt_domain_model_poklic.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'naziv' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.naziv',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'oznaka' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.oznaka',
			'config' => array(
				'type' => 'input',
				'size' => 15,
				'eval' => 'trim'
			),
		),
		'opis' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.opis',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
                'wizards' => array(
                    'RTE' => array(
                        'icon' => 'wizard_rte2.gif',
                        'notNewRecords'=> 1,
                        'RTEonly' => 1,
                        'script' => 'wizard_rte.php',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
                        'type' => 'script'
                    )
                )
			),
			'defaultExtras' => 'richtext[]:rte_transform[mode=ts]'
		),
		'kljucne_besede' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.kljucne_besede',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			),
		),
		'opis_obnove' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.opis_obnove',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			),
		),
		'pravno_sredstvo' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.pravno_sredstvo',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('--- Izberi ---', 0),
					array('Pritožba', 1),
					array('Upravni sport', 2),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'opomba' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.opomba',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
                'wizards' => array(
                    'RTE' => array(
                        'icon' => 'wizard_rte2.gif',
                        'notNewRecords'=> 1,
                        'RTEonly' => 1,
                        'script' => 'wizard_rte.php',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
                        'type' => 'script'
                    )
                )
			),
			'defaultExtras' => 'richtext[]:rte_transform[mode=ts]'
		),
		'zakonodajni_organ_kontakt' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.zakonodajni_organ_kontakt',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_kontaktnaoseba',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_kontaktnaoseba.pid = ###CURRENT_PID###',
				'minitems' => 0,
				'maxitems' => 1,
				'appearance' => array(
					'collapse' => 1,
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'sorodne_povezave' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.sorodne_povezave',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_sorodnepovezave',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_sorodnepovezave.pid = ###CURRENT_PID###',
				'foreign_field' => 'poklic',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'pravne_podlage' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.pravne_podlage',
			'config' => array(
				'type' => 'group',
				'foreign_table' => 'tx_a3ekt_domain_model_pravnapodlaga',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_pravnapodlaga.pid = ###CURRENT_PID###',
				'maxitems'      => 9999,
				'size' => 5,
                'internal_type' => 'db',
                'allowed' => 'tx_a3ekt_domain_model_pravnapodlaga',
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
				'items' => array(
					array('--- Izberi ---', 0),
				),
                'wizards' => array(
                    'suggest' => array(    
						'type' => 'suggest',
						'default' => array(
							'searchWholePhrase' => 1,
							'addWhere' => 'AND pid = ###CURRENT_PID###'
						),
					),
					'_PADDING' => 10,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_pravnapodlaga',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_pravnapodlaga',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
                ),
			),
		),
		'zakonodajni_organ' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.zakonodajni_organ',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_organ',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_organ.pid = ###CURRENT_PID###',
				'allowed' => 'tx_a3ekt_domain_model_organ',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
				'wizards' => array(
					'suggest' => array(    
						'type' => 'suggest',
						'default' => array(
							'searchWholePhrase' => 1,
							'addWhere' => 'AND pid = ###CURRENT_PID###'
						),
					),
					'_PADDING' => 5,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_organ',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_organ',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
				),
			),
		),
		'cezmejno_priznavanje_poklica' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.cezmejno_opravljanje_poklica',
			'config' => array(
				'type' => 'radio',
				'items' => array(
					array('Ni možno', 0),
					array('Ni prijave', 1),
					array('Prijava s postopkom pridobitve dovoljenja', 2),
					array('Prijava brez postopka pridobitve dovoljenja', 3),
					array('Drugo', 99),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'cezmejno_priznavanje_obrazlozitev' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.cezmejno_opravljenje_obrazlozitev',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
                'wizards' => array(
                    'RTE' => array(
                        'icon' => 'wizard_rte2.gif',
                        'notNewRecords'=> 1,
                        'RTEonly' => 1,
                        'script' => 'wizard_rte.php',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
                        'type' => 'script'
                    )
                )
			),
			'defaultExtras' => 'richtext[]:rte_transform[mode=ts]'
		),
		'organ_za_prijavo_brez_postopka' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.organ_za_prijavo_brez_postopka',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_organ',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_organ.pid = ###CURRENT_PID###',
				'allowed' => 'tx_a3ekt_domain_model_organ',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
				'wizards' => array(
					'suggest' => array(    
						'type' => 'suggest',
						'default' => array(
							'searchWholePhrase' => 1,
							'addWhere' => 'AND pid = ###CURRENT_PID###'
						),
					),
					'_PADDING' => 5,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_organ',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_organ',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
				),
			),
		),
        'prijava_s_postopkom_pridobitve_dovoljenja' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.prijava_s_postopkom_pridobitve_dovoljenja',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_cezmejnoopravljenjepoklica',
                                'foreign_table_where' => 'AND tx_a3ekt_domain_model_cezmejnoopravljenjepoklica.pid = ###CURRENT_PID###',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
			),
		),
        'sekundarne_vsebine' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.sekundarne_vsebine',
			'config' => array(
				'type' => 'group',
				'foreign_table' => 'tt_content',
                //'foreign_field' => 'sekundarne_vsebine',
				'maxitems'      => 9999,
				'size' => 5,
                'internal_type' => 'db',
                'allowed' => 'tt_content',
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
                'wizards' => array(
                    'suggest' => array(    
                        'type' => 'suggest',
                    ),
                ),
			),
		),
        'vir' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_poklic.vir',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_organ',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_organ.pid = ###CURRENT_PID### AND tx_a3ekt_domain_model_organ.sys_language_uid = ###REC_FIELD_sys_language_uid###',
				'allowed' => 'tx_a3ekt_domain_model_organ',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
				'wizards' => array(
					'suggest' => array(    
						'type' => 'suggest',
						'default' => array(
							'searchWholePhrase' => 1,
							'addWhere' => 'AND pid = ###CURRENT_PID###'
						),
					),
					'_PADDING' => 5,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_organ',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_organ',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
				),
			),
		),
		'spacer' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->spacerField',
			),
		),
		'line' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->lineField',
			),
		),
		'step01' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '01. NAZIV POKLICNE KVALIFIKACIJE / REGULIRANEGA POKLICA',
					'description' => $TCA['ekt_descriptions']['poklic']['step01']
				),
			),
		),
		'step02' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '02. OPIS POKLICNE KVALIFIKACIJ / REGULIRANEGA POKLICA',
					'description' => $TCA['ekt_descriptions']['poklic']['step02']
				),
			),
		),
		'step03' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '03. ZAKONODAJNI ORGAN',
					'description' => $TCA['ekt_descriptions']['poklic']['step03']
				),
			),
		),
		'step04' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '04. PRAVNA PODLAGA',
					'description' => $TCA['ekt_descriptions']['poklic']['step04']
				),
			),
		),
		'step05' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '05. KLJUČNE BESEDE',
					'description' => $TCA['ekt_descriptions']['poklic']['step05']
				),
			),
		),
		'step06' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '06. SORODNE POVEZAVE',
					'description' => $TCA['ekt_descriptions']['poklic']['step06']
				),
			),
		),
		'step07' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '07. OPIS POGOJEV',
					'description' => $TCA['ekt_descriptions']['poklic']['step07']
				),
			),
		),
		/*'step08' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '08. ČEZMEJNO/OBČASNO OPRAVLJANJE POKLICA',
					'description' => $TCA['ekt_descriptions']['poklic']['step08']
				),
			),
		),
        'step12' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '10. VIR',
					'description' => $TCA['ekt_descriptions']['poklic']['step12']
				),
			),
		),
		'step08_0' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '08. ČEZMEJNO/OBČASNO OPRAVLJANJE POKLICA',
					'description' => $TCA['ekt_descriptions']['poklic']['step08_0']
				),
			),
		),
		'step08_1' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '08. ČEZMEJNO/OBČASNO OPRAVLJANJE POKLICA',
					'description' => $TCA['ekt_descriptions']['poklic']['step08_1']
				),
			),
		),
		'step08_2' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '08. ČEZMEJNO/OBČASNO OPRAVLJANJE POKLICA',
					'description' => $TCA['ekt_descriptions']['poklic']['step08_2']
				),
			),
		),
		'step08_3' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '08. ČEZMEJNO/OBČASNO OPRAVLJANJE POKLICA',
					'description' => $TCA['ekt_descriptions']['poklic']['step08_3']
				),
			),
		),
		'step08_99' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '08. ČEZMEJNO/OBČASNO OPRAVLJANJE POKLICA',
					'description' => $TCA['ekt_descriptions']['poklic']['step08_99']
				),
			),
		),*/
        'step99' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '09. SEKUNDARNE VSEBINE',
					'description' => $TCA['ekt_descriptions']['poklic']['step99']
				),
			),
		),
	),
);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$TCA['tx_a3ekt_domain_model_poklic']['columns']['hidden']['config']['default'] = '1';

// add column 'tx_externalimporttut_externalid'
a3_ekt_addExternalidColumn('tx_a3ekt_domain_model_poklic');

// do the mapping procedure
a3_ekt_mappingProcedure('tx_a3ekt_domain_model_poklic');

?>