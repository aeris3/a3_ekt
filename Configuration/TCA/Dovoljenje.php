<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_a3ekt_domain_model_dovoljenje'] = array(
	'ctrl' => $TCA['tx_a3ekt_domain_model_dovoljenje']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, naziv, oznaka, opis, obvezno_clanstvo, obnova, termin_obnove, opis_obnove, drugo, pravno_sredstvo, molk_organa, opombe, kontaktna_oseba, priloge_k_vlogi, stroski_postopka, koraki_postopka, pritozbeni_organ, pravna_podlaga, register_dovoljenj, vloge, pristojni_organ',
	),
	'types' => array(
		'1' => array('showitem' => 'hidden;;1, step01, --palette--;;step01, --linebreak--, step02, --palette--;;step02, step03, --palette--;;step03, step04, --palette--;;step04, step05, --palette--;;step05, step06, --palette--;;step06, step07, --palette--;;step07, step08, --palette--;;step08, step09, --palette--;;step09, step10, --palette--;;step10, step11, --palette--;;step11, line, --palette--;;step12, step13, --palette--;;step13'),
	),
	'palettes' => array(
		'step01' => array('showitem' => 'naziv, oznaka, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step02' => array('showitem' => 'opis, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step03' => array('showitem' => 'pristojni_organ, kontaktna_oseba, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step04' => array('showitem' => 'vloge, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step05' => array('showitem' => 'priloge_k_vlogi, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step06' => array('showitem' => 'stroski_postopka, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step07' => array('showitem' => 'koraki_postopka, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step08' => array('showitem' => 'obvezno_clanstvo, --linebreak--, obnova, --linebreak--, termin_obnove, --linebreak--, opis_obnove, --linebreak--, drugo, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step09' => array('showitem' => 'pravno_sredstvo, --linebreak--, pritozbeni_organ, --linebreak--, molk_organa, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step10' => array('showitem' => 'pravna_podlaga, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step11' => array('showitem' => 'register_dovoljenj, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step12' => array('showitem' => 'opombe, --linebreak--, spacer', 'canNotCollapse' => TRUE),
        'step13' => array('showitem' => 'vir, --linebreak--, spacer', 'canNotCollapse' => TRUE),        
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_a3ekt_domain_model_dovoljenje',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_dovoljenje.pid=###CURRENT_PID### AND tx_a3ekt_domain_model_dovoljenje.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'naziv' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.naziv',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'oznaka' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.oznaka',
			'config' => array(
				'type' => 'input',
				'size' => 15,
				'eval' => 'trim'
			),
		),
		'opis' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.opis',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
                'wizards' => array(
                    'RTE' => array(
                        'icon' => 'wizard_rte2.gif',
                        'notNewRecords'=> 1,
                        'RTEonly' => 1,
                        'script' => 'wizard_rte.php',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
                        'type' => 'script'
                    )
                )
			),
			'defaultExtras' => 'richtext[]:rte_transform[mode=ts]'
		),
		'obvezno_clanstvo' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.obvezno_clanstvo',
			'config' => array(
				'type' => 'check',
			),
		),
		'obnova' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.obnova',
			'config' => array(
				'type' => 'check',
			),
		),
		'termin_obnove' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.termin_obnove',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim',
				'checkbox' => 1,
			),
		),
		'opis_obnove' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.opis_obnove',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
                'wizards' => array(
                    'RTE' => array(
                        'icon' => 'wizard_rte2.gif',
                        'notNewRecords'=> 1,
                        'RTEonly' => 1,
                        'script' => 'wizard_rte.php',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
                        'type' => 'script'
                    )
                )
			),
			'defaultExtras' => 'richtext[]:rte_transform[mode=ts]'
		),
		'drugo' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.drugo',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			),
		),
		'pravno_sredstvo' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.pravno_sredstvo',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('--- Izberi ---', 0),
					array('Pritožba', 1),
					array('Upravni sport', 2),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'molk_organa' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.molk_organa',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			),
		),
		'opombe' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.pomembno',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
                'wizards' => array(
                    'RTE' => array(
                        'icon' => 'wizard_rte2.gif',
                        'notNewRecords'=> 1,
                        'RTEonly' => 1,
                        'script' => 'wizard_rte.php',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
                        'type' => 'script'
                    )
                )
			),
			'defaultExtras' => 'richtext[]:rte_transform[mode=ts]'
		),
		'kontaktna_oseba' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.kontaktna_oseba',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_kontaktnaoseba',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_kontaktnaoseba.pid = ###CURRENT_PID###',
				'minitems' => 0,
				'maxitems' => 1,
				'appearance' => array(
					'collapse' => 1,
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'priloge_k_vlogi' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.priloge_k_vlogi',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_vlogapriloga',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_vlogapriloga.pid = ###CURRENT_PID###',
				'foreign_field' => 'dovoljenje',
				'foreign_sortby' => 'sorting',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'stroski_postopka' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.stroski_postopka',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_stroskipostopka',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_stroskipostopka.pid = ###CURRENT_PID###',
				'foreign_field' => 'dovoljenje',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'koraki_postopka' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.koraki_postopka',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_korakipostopka',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_korakipostopka.pid = ###CURRENT_PID###',
				'foreign_field' => 'dovoljenje',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'pritozbeni_organ' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.pritozbeni_organ',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_organ',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_organ.pid = ###CURRENT_PID###',
				'allowed' => 'tx_a3ekt_domain_model_organ',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
				'wizards' => array(
					'suggest' => array(    
						'type' => 'suggest',
						'default' => array(
							'searchWholePhrase' => 1,
							'addWhere' => 'AND pid = ###CURRENT_PID###'
						),
					),
					'_PADDING' => 5,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_organ',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_organ',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
				),
			),
		),
		'pravna_podlaga' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.pravna_podlaga',
			'config' => array(
				'type' => 'group',
				'foreign_table' => 'tx_a3ekt_domain_model_pravnapodlaga',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_pravnapodlaga.pid = ###CURRENT_PID###',
				'maxitems'      => 9999,
				'size' => 5,
                'internal_type' => 'db',
                'allowed' => 'tx_a3ekt_domain_model_pravnapodlaga',
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
				'items' => array(
					array('--- Izberi ---', 0),
				),
                'wizards' => array(
                    'suggest' => array(    
						'type' => 'suggest',
						'default' => array(
							'searchWholePhrase' => 1,
							'addWhere' => 'AND pid = ###CURRENT_PID###'
						),
					),
					'_PADDING' => 10,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_pravnapodlaga',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_pravnapodlaga',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
                ),
			),
		),
		'register_dovoljenj' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.register_dovoljenj',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_registerdovoljenj',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_registerdovoljenj.pid = ###CURRENT_PID###',
				'foreign_field' => 'dovoljenje',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'vloge' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.vloge',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_vloga',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_vloga.pid = ###CURRENT_PID###',
				'foreign_field' => 'dovoljenje',
				'foreign_sortby' => 'sorting',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapse' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1,
					'useSortable' => 1
				),
			),
		),
		'pristojni_organ' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.pristojni_organ',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_organ',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_organ.pid = ###CURRENT_PID###',
				'allowed' => 'tx_a3ekt_domain_model_organ',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
				'wizards' => array(
					'suggest' => array(    
						'type' => 'suggest',
						'default' => array(
							'searchWholePhrase' => 1,
							'addWhere' => 'AND pid = ###CURRENT_PID###'
						),
					),
					'_PADDING' => 5,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_organ',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_organ',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
				),
			),
		),
		'ektpovezavepogoj' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
        'vir' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dovoljenje.vir',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_organ',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_organ.pid = ###CURRENT_PID### AND tx_a3ekt_domain_model_organ.sys_language_uid = ###REC_FIELD_sys_language_uid###',
				'allowed' => 'tx_a3ekt_domain_model_organ',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
				'wizards' => array(
					'suggest' => array(    
						'type' => 'suggest',
						'default' => array(
							'searchWholePhrase' => 1,
							'addWhere' => 'AND pid = ###CURRENT_PID###'
						),
					),
					'_PADDING' => 5,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_organ',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_organ',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
				),
			),
		),
		'spacer' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->spacerField',
			),
		),
		'line' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->lineField',
			),
		),
		'step01' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '01. NAZIV DOVOLJENJA',
					'description' => $TCA['ekt_descriptions']['dovoljenje']['step01']
				),
			),
		),
		'step02' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '02. OPIS DOVOLJENJA',
					'description' => $TCA['ekt_descriptions']['dovoljenje']['step02']
				),
			),
		),
		'step03' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '03. PRISTOJNI ORGAN',
					'description' => $TCA['ekt_descriptions']['dovoljenje']['step03']
				),
			),
		),
		'step04' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '04. VLOGA ZA PRIDOBITEV DOVOLJENJA',
					'description' => $TCA['ekt_descriptions']['dovoljenje']['step04']
				),
			),
		),
		'step05' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '05. PRILOGE K VLOGI',
					'description' => $TCA['ekt_descriptions']['dovoljenje']['step05']
				),
			),
		),
		'step06' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '06. STROŠKI POSTOPKA',
					'description' => $TCA['ekt_descriptions']['dovoljenje']['step06']
				),
			),
		),
		'step07' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '07. KORAKI V POSTOPKU',
					'description' => $TCA['ekt_descriptions']['dovoljenje']['step07']
				),
			),
		),
		'step08' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '08. DRUGE AKTIVNOSTI POVEZANE Z DOVOLJENJEM',
					'description' => $TCA['ekt_descriptions']['dovoljenje']['step08']
				),
			),
		),
		'step09' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '09. PRAVNO VARSTVO',
					'description' => $TCA['ekt_descriptions']['dovoljenje']['step09']
				),
			),
		),
		'step10' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '10. PRAVNA PODLAGA',
					'description' => $TCA['ekt_descriptions']['dovoljenje']['step10']
				),
			),
		),
		'step11' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '11. REGISTER DOVOLJENJ',
					'description' => $TCA['ekt_descriptions']['dovoljenje']['step11']
				),
			),
		),
        'step13' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '12. VIR',
					'description' => $TCA['ekt_descriptions']['dovoljenje']['step13']
				),
			),
		),
	),
);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$TCA['tx_a3ekt_domain_model_dovoljenje']['columns']['hidden']['config']['default'] = '1';

// add column 'tx_externalimporttut_externalid'
a3_ekt_addExternalidColumn('tx_a3ekt_domain_model_dovoljenje');

// do the mapping procedure
a3_ekt_mappingProcedure('tx_a3ekt_domain_model_dovoljenje');

?>