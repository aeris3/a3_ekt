<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_a3ekt_domain_model_ektdejavnost'] = array(
	'ctrl' => $TCA['tx_a3ekt_domain_model_ektdejavnost']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, naziv, sifra, skd_dejavnosti, sektor_dejavnosti',
	),
	'types' => array(
		'1' => array('showitem' => 'hidden;;1, step01, --palette--;;step01, --linebreak--, step02, --palette--;;step02, step03, --palette--;;step03'),
	),
	'palettes' => array(
		'step01' => array('showitem' => 'sifra, --linebreak--, naziv, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step02' => array('showitem' => 'skd_dejavnosti, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step03' => array('showitem' => 'sektor_dejavnosti, --linebreak--, spacer', 'canNotCollapse' => TRUE),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_a3ekt_domain_model_ektdejavnost',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_ektdejavnost.pid=###CURRENT_PID### AND tx_a3ekt_domain_model_ektdejavnost.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'naziv' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_ektdejavnost.naziv',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'sifra' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_ektdejavnost.sifra',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'skd_dejavnosti' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_ektdejavnost.skd_dejavnosti',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_skddejavnost',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_skddejavnost.pid = ###CURRENT_PID###',
				'MM' => 'tx_a3ekt_ektdejavnost_skddejavnost_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'renderMode' => 'tree',
				'treeConfig' => array(
					'expandAll' => true,
					'parentField' => 'parent_s_k_d',
					'appearance' => array(
						'showHeader' => TRUE,
						'maxLevels' => 3,
						'nonSelectableLevels' => '0,1,2,3,4',
					),
				),
			),
		),
		'sektor_dejavnosti' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_ektdejavnost.sektor_dejavnosti',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_sektor',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_sektor.pid = ###CURRENT_PID###',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
			),
		),
		'spacer' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->spacerField',
			),
		),
		'line' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->lineField',
			),
		),
		'step01' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '01. EKT ŠIFRA IN NAZIV REGULIRANE DEJAVNOSTI'
				),
			),
		),
		'step02' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '02. SKD ŠIFRA'
				),
			),
		),
		'step03' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '03. SEKTOR'
				),
			),
		),
	),
);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$TCA['tx_a3ekt_domain_model_ektdejavnost']['columns']['hidden']['config']['default'] = '1';

// add column 'tx_externalimporttut_externalid'
a3_ekt_addExternalidColumn('tx_a3ekt_domain_model_ektdejavnost');

// do the mapping procedure
a3_ekt_mappingProcedure('tx_a3ekt_domain_model_ektdejavnost');

?>