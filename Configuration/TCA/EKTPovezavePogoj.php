<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_a3ekt_domain_model_ektpovezavepogoj'] = array(
	'ctrl' => $TCA['tx_a3ekt_domain_model_ektpovezavepogoj']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, opomba, pogoj, dejavnost',
	),
	'types' => array(
		'0' => array('showitem' => 'hidden;;1, dejavnost, spacer, step01, --palette--;;step01, __step09_0, --palette--;;__step09_0, --linebreak--, step02, --palette--;;step02'),
		'1' => array('showitem' => 'hidden;;1, dejavnost, spacer, step01, --palette--;;step01, __step09_1, --palette--;;__step09_1, --linebreak--, step02, --palette--;;step02'),
		'2' => array('showitem' => 'hidden;;1, dejavnost, spacer, step01, --palette--;;step01, __step09_2, --palette--;;__step09_2, --linebreak--, step02, --palette--;;step02'),
		'3' => array('showitem' => 'hidden;;1, dejavnost, spacer, step01, --palette--;;step01, __step09_3, --palette--;;__step09_3, --linebreak--, step02, --palette--;;step02'),
		'99' => array('showitem' => 'hidden;;1, dejavnost, spacer, step01, --palette--;;step01, __step09_99, --palette--;;__step09_99, --linebreak--, step02, --palette--;;step02'),
	),
	'palettes' => array(
		'step01' => array('showitem' => 'pogoj, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'step02' => array('showitem' => 'opomba, --linebreak--, spacer', 'canNotCollapse' => TRUE),
		'__step09_0' => array('showitem' => '__cezmejno_opravljanje, --linebreak--, spacer', 'canNotCollapse' => TRUE),
	  	'__step09_1' => array('showitem' => '__cezmejno_opravljanje, --linebreak--, spacer', 'canNotCollapse' => TRUE),
      	'__step09_2' => array('showitem' => '__cezmejno_opravljanje, --linebreak--, spacer, --linebreak--, __pogoj, --linebreak--, spacer', 'canNotCollapse' => TRUE),
	  	'__step09_3' => array('showitem' => '__cezmejno_opravljanje, --linebreak--, spacer, --linebreak--, __organ_za_prijavo_brez_postopka, --linebreak--, spacer', 'canNotCollapse' => TRUE),
	  	'__step09_99' => array('showitem' => '__cezmejno_opravljanje, --linebreak--, spacer, --linebreak--, __cezmejno_opravljenje_obrazlozitev, --linebreak--, spacer', 'canNotCollapse' => TRUE),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_a3ekt_domain_model_ektpovezavepogoj',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_ektpovezavepogoj.pid=###CURRENT_PID### AND tx_a3ekt_domain_model_ektpovezavepogoj.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'opomba' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_ektpovezavepogoj.opomba',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
                'wizards' => array(
                    'RTE' => array(
                        'icon' => 'wizard_rte2.gif',
                        'notNewRecords'=> 1,
                        'RTEonly' => 1,
                        'script' => 'wizard_rte.php',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
                        'type' => 'script'
                    )
                )
			),
			'defaultExtras' => 'richtext[]:rte_transform[mode=ts]'
		),
		'pogoj' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_ektpovezavepogoj.pogoj',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_povezavepogojev',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_povezavepogojev.pid = ###CURRENT_PID###',
				'foreign_field' => 'ektpovezavepogoj',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapse' => 0,
					'expandSingle' => 1,
					'useSortable' => 1,
					'expandSingle' => 1,
					'useSortable' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'dejavnost' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_ektpovezavepogoj.dejavnost',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_dejavnost',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_dejavnost.pid = ###CURRENT_PID###',
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
				'wizards' => array(
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_dejavnost',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_dejavnost',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
					'_PADDING' => 2,
					'suggest' => array(    
						'type' => 'suggest',
						'default' => array(
							'searchWholePhrase' => 1,
							'addWhere' => 'AND pid = ###CURRENT_PID###'
						),
					),
				),
			),
		),
		'step01' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
				'type' => 'user',
				'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '01. POGOJI',
					'description' => $TCA['ekt_descriptions']['ektpovezavepogoj']['step01']
				),
			),
		),
		//custom form moved from dejavnosti
		'__cezmejno_opravljanje' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dejavnost.cezmejno_opravljanje',
			'config' => array(
				//'type' => 'radio',
				/*'foreign_table' => 'tx_a3ekt_domain_model_dejavnost',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_dejavnost.uid = 650',*/
				/*'items' => array(
					array('Ni možno', 0),
					array('Ni prijave', 1),
					array('Prijava s postopkom pridobitve dovoljenja', 2),
					array('Prijava brez postopka pridobitve dovoljenja', 3),
					array('Drugo', 99),
				),*/
				'type' => 'user',
  				'userFunc' => 'EXT:class.tx_a3ekt_postBeSave.php:tx_a3ekt_postBeSave->customFormDejavnosti',
				'size' => 1,
				'maxitems' => 1,
				'eval' => '',
				/*'parameters' => array (
					'uid' => ###THIS_UID###
				)*/
			),
		),
		//custom form moved from dejavnosti
		'__cezmejno_opravljenje_obrazlozitev' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dejavnost.cezmejno_opravljenje_obrazlozitev',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
                'wizards' => array(
                    'RTE' => array(
                        'icon' => 'wizard_rte2.gif',
                        'notNewRecords'=> 1,
                        'RTEonly' => 1,
                        'script' => 'wizard_rte.php',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
                        'type' => 'script'
                    )
                )
			),
			'defaultExtras' => 'richtext[]:rte_transform[mode=ts]'
		),
		//custom form moved from dejavnosti
		/*'__prijava_s_postopkom_pridobitve_dovoljenja' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dejavnost.prijava_s_postopkom_pridobitve_dovoljenja',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_cezmejnodovoljenje',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_cezmejnodovoljenje.pid = ###CURRENT_PID###',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
			
			'wizards' => array(*/
					/*'suggest' => array(    
						'type' => 'suggest',
						'default' => array(
							'searchWholePhrase' => 1,
							'addWhere' => 'AND pid = ###CURRENT_PID###'
						),
					),*/
					/*'_PADDING' => 5,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_cezmejnodovoljenje',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_cezmejnodovoljenje',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
				),
			),
		),*/
		//custom form moved from dejavnosti
		'__pogoj' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_ektpovezavepogoj.pogoj',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_a3ekt_domain_model_povezavepogojev',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_povezavepogojev.pid = ###CURRENT_PID###',
				'foreign_field' => 'ektpovezavepogoj',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapse' => 0,
					'expandSingle' => 1,
					'useSortable' => 1,
					'expandSingle' => 1,
					'useSortable' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		//custom form moved from dejavnosti
		'__organ_za_prijavo_brez_postopka' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_dejavnost.organ_za_prijavo_brez_postopka',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_organ',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_organ.pid = ###CURRENT_PID###',
				'allowed' => 'tx_a3ekt_domain_model_organ',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
				'wizards' => array(
					/*'suggest' => array(    
						'type' => 'suggest',
						'default' => array(
							'searchWholePhrase' => 1,
							'addWhere' => 'AND pid = ###CURRENT_PID###'
						),
					),*/
					'_PADDING' => 5,
					'add' => array(
						'type' => 'script',
						'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_add_title',
						'icon' => 'add.gif',
						'params' => array(
										'table' => 'tx_a3ekt_domain_model_organ',
										'pid' => '###CURRENT_PID###',
										'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
                    'edit' => array(
                        'type' => 'popup',
                        'title' => 'LLL:EXT:lang/locallang_tca.xml:be_users.usergroup_edit_title',
                        'icon' => 'edit2.gif',
                        'params' => array(
                            'table' => 'tx_a3ekt_domain_model_organ',
                        ), 
                        'script' => 'wizard_edit.php',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=500,width=660,status=0,menubar=0,scrollbars=1',
                    ),
				),
			),
		),
		'__step09_0' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '02. ČEZMEJNO/OBČASNO OPRAVLJANJE DEJAVNOSTI',
					'description' => $TCA['ekt_descriptions']['dejavnost']['step09_0']
				),
			),
		),
		'__step09_1' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '02. ČEZMEJNO/OBČASNO OPRAVLJANJE DEJAVNOSTI',
					'description' => $TCA['ekt_descriptions']['dejavnost']['step09_1']
				),
			),
		),
		'__step09_2' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '02. ČEZMEJNO/OBČASNO OPRAVLJANJE DEJAVNOSTI',
					'description' => $TCA['ekt_descriptions']['dejavnost']['step09_2']
				),
			),
		),
		'__step09_3' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '02. ČEZMEJNO/OBČASNO OPRAVLJANJE DEJAVNOSTI',
					'description' => $TCA['ekt_descriptions']['dejavnost']['step09_3']
				),
			),
		),
		'__step09_99' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '02. ČEZMEJNO/OBČASNO OPRAVLJANJE DEJAVNOSTI',
					'description' => $TCA['ekt_descriptions']['dejavnost']['step09_99']
				),
			),
		),
		'step02' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->descriptionField',
				'parameters' => array(
					'label' => '03. TEKSTOVNI OPIS POGOJEV',
					'description' => $TCA['ekt_descriptions']['ektpovezavepogoj']['step02']
				),
			),
		),
		'spacer' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->spacerField',
			),
		),
		'line' => array(
			'exclude' => 0,
			'label' => '',
			'config' => array (
			'type' => 'user',
			'userFunc' => 'EXT:a3_ekt/class.tx_a3ekt_tca.php:tx_a3ekt_tca->lineField',
			),
		),
	),
);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

// add column 'tx_externalimporttut_externalid'
a3_ekt_addExternalidColumn('tx_a3ekt_domain_model_ektpovezavepogoj');

// do the mapping procedure
a3_ekt_mappingProcedure('tx_a3ekt_domain_model_ektpovezavepogoj');

?>