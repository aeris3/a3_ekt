<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_a3ekt_domain_model_skddejavnost'] = array(
	'ctrl' => $TCA['tx_a3ekt_domain_model_skddejavnost']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, deskriptor, sifra_kategorije, parent_s_k_d',
	),
	'types' => array(
		'1' => array('showitem' => 'hidden;;1, deskriptor, sifra_kategorije, parent_s_k_d'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_a3ekt_domain_model_skddejavnost',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_skddejavnost.pid=###CURRENT_PID### AND tx_a3ekt_domain_model_skddejavnost.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'deskriptor' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_skddejavnost.deskriptor',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'sifra_kategorije' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_skddejavnost.sifra_kategorije',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'parent_s_k_d' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:a3_ekt/Resources/Private/Language/locallang_db.xml:tx_a3ekt_domain_model_skddejavnost.parent_s_k_d',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_a3ekt_domain_model_skddejavnost',
				'foreign_table_where' => 'AND tx_a3ekt_domain_model_skddejavnost.pid = ###CURRENT_PID###',
				'minitems' => 0,
				'maxitems' => 1,
				'items' => array(
					array('--- Izberi ---', 0),
				),
			),
		),
	),
);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

// add column 'tx_externalimporttut_externalid'
a3_ekt_addExternalidColumn('tx_a3ekt_domain_model_skddejavnost');

// do the mapping procedure
a3_ekt_mappingProcedure('tx_a3ekt_domain_model_skddejavnost');

?>