#
# Table structure for table 'tx_a3ekt_domain_model_ektdejavnost'
#
CREATE TABLE tx_a3ekt_domain_model_ektdejavnost (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	naziv varchar(255) DEFAULT '' NOT NULL,
	sifra varchar(255) DEFAULT '' NOT NULL,
	skd_dejavnosti int(11) unsigned DEFAULT '0' NOT NULL,
	sektor_dejavnosti int(11) unsigned DEFAULT '0',
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_dejavnost'
#
CREATE TABLE tx_a3ekt_domain_model_dejavnost (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	ektpovezavepogoj int(11) unsigned DEFAULT '0' NOT NULL,

	naziv varchar(255) DEFAULT '' NOT NULL,
	oznaka varchar(255) DEFAULT '' NOT NULL,
	opomba_za_e_k_t text NOT NULL,
	opis text NOT NULL,
	slika text NOT NULL,
	opis_slike varchar(255) DEFAULT '' NOT NULL,
	kljucne_besede text NOT NULL,
		cezmejno_opravljanje int(11) DEFAULT '0' NOT NULL,
		cezmejno_opravljenje_obrazlozitev text NOT NULL,
	povzetek_pogojev text NOT NULL,
	opomba text NOT NULL,
	zakonodajni_organ int(11) unsigned DEFAULT '0',
	pristojni_organ int(11) unsigned DEFAULT '0',
	pravne_podlage varchar(255) DEFAULT '' NOT NULL,
	sorodne_povezave int(11) unsigned DEFAULT '0' NOT NULL,
	zakonodajni_organ_kontakt int(11) unsigned DEFAULT '0',
	pristojni_organ_kontakt int(11) unsigned DEFAULT '0',
	dejavnost_po_e_k_t int(11) unsigned DEFAULT '0',
		organ_za_prijavo_brez_postopka int(11) unsigned DEFAULT '0',
	sekundarne_vsebine varchar(255) DEFAULT '' NOT NULL,
    vir int(11) unsigned DEFAULT '0'
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_dovoljenje'
#
CREATE TABLE tx_a3ekt_domain_model_dovoljenje (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	naziv varchar(255) DEFAULT '' NOT NULL,
	oznaka varchar(255) DEFAULT '' NOT NULL,
	opis text NOT NULL,
	obvezno_clanstvo int(11) DEFAULT '0' NOT NULL,
	obnova int(11) DEFAULT '0' NOT NULL,
	termin_obnove varchar(255) DEFAULT '' NOT NULL,
	opis_obnove text NOT NULL,
	drugo text NOT NULL,
	pravno_sredstvo int(11) DEFAULT '0' NOT NULL,
	molk_organa text NOT NULL,
	opomba text NOT NULL,
	pristojni_organ int(11) unsigned DEFAULT '0',
	kontaktna_oseba int(11) unsigned DEFAULT '0',
	vloge int(11) unsigned DEFAULT '0' NOT NULL,
	priloge_k_vlogi int(11) unsigned DEFAULT '0' NOT NULL,
	stroski_postopka int(11) unsigned DEFAULT '0' NOT NULL,
	koraki_postopka int(11) unsigned DEFAULT '0' NOT NULL,
	pritozbeni_organ int(11) unsigned DEFAULT '0',
	pravna_podlaga varchar(255) DEFAULT '' NOT NULL,
	register_dovoljenj int(11) unsigned DEFAULT '0' NOT NULL,
    vir int(11) unsigned DEFAULT '0'
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_poklic'
#
CREATE TABLE tx_a3ekt_domain_model_poklic (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	ektpovezavepoklicev int(11) unsigned DEFAULT '0' NOT NULL,

	naziv varchar(255) DEFAULT '' NOT NULL,
	oznaka varchar(255) DEFAULT '' NOT NULL,
	opis text NOT NULL,
	kljucne_besede text NOT NULL,
	opomba text NOT NULL,
	zakonodajni_organ int(11) unsigned DEFAULT '0',
	zakonodajni_organ_kontakt int(11) unsigned DEFAULT '0',
	sorodne_povezave int(11) unsigned DEFAULT '0' NOT NULL,
	pravne_podlage varchar(255) DEFAULT '' NOT NULL,
	sekundarne_vsebine varchar(255) DEFAULT '' NOT NULL,
    vir int(11) unsigned DEFAULT '0'
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,
	cezmejno_priznavanje_obrazlozitev text NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_organ'
#
CREATE TABLE tx_a3ekt_domain_model_organ (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	naziv varchar(255) DEFAULT '' NOT NULL,
	naslov varchar(255) DEFAULT '' NOT NULL,
	spletni_naslov varchar(255) DEFAULT '' NOT NULL,
	vrsta_organa int(11) DEFAULT '0' NOT NULL,
	posta int(11) unsigned DEFAULT '0',
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_posta'
#
CREATE TABLE tx_a3ekt_domain_model_posta (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	naziv varchar(255) DEFAULT '' NOT NULL,
	stevilka int(11) DEFAULT '0' NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_pravnapodlaga'
#
CREATE TABLE tx_a3ekt_domain_model_pravnapodlaga (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	dejavnost int(11) unsigned DEFAULT '0' NOT NULL,
	dovoljenje5 int(11) unsigned DEFAULT '0' NOT NULL,
	poklic int(11) unsigned DEFAULT '0' NOT NULL,
	drugipogoj int(11) unsigned DEFAULT '0' NOT NULL,
	cezmejnodovoljenje int(11) unsigned DEFAULT '0' NOT NULL,
	dovoljenjevtujini int(11) unsigned DEFAULT '0' NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	naziv varchar(255) DEFAULT '' NOT NULL,
	url varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_sorodnepovezave'
#
CREATE TABLE tx_a3ekt_domain_model_sorodnepovezave (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	dejavnost int(11) unsigned DEFAULT '0' NOT NULL,
	poklic int(11) unsigned DEFAULT '0' NOT NULL,
	drugipogoj int(11) unsigned DEFAULT '0' NOT NULL,

	naziv varchar(255) DEFAULT '' NOT NULL,
	url varchar(255) DEFAULT '' NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_skddejavnost'
#
CREATE TABLE tx_a3ekt_domain_model_skddejavnost (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	deskriptor varchar(255) DEFAULT '' NOT NULL,
	sifra_kategorije varchar(255) DEFAULT '' NOT NULL,
	parent_s_k_d int(11) unsigned DEFAULT '0',
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_sektor'
#
CREATE TABLE tx_a3ekt_domain_model_sektor (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	naziv varchar(255) DEFAULT '' NOT NULL,
	opis text NOT NULL,
	skd int(11) unsigned DEFAULT '0',
	stran int(11) unsigned DEFAULT '0',
    sekundarne_vsebine varchar(255) DEFAULT '' NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_kontaktnaoseba'
#
CREATE TABLE tx_a3ekt_domain_model_kontaktnaoseba (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	ime_priimek varchar(255) DEFAULT '' NOT NULL,
	telefon varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	email_namestnika varchar(255) DEFAULT '' NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_vloga'
#
CREATE TABLE tx_a3ekt_domain_model_vloga (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	dovoljenje int(11) unsigned DEFAULT '0' NOT NULL,
	cezmejnodovoljenje int(11) unsigned DEFAULT '0' NOT NULL,
	dovoljenjevtujini int(11) unsigned DEFAULT '0' NOT NULL,

	zaporedna_stevilka int(11) DEFAULT '0' NOT NULL,
	naziv varchar(255) DEFAULT '' NOT NULL,
	url varchar(255) DEFAULT '' NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_vlogapriloga'
#
CREATE TABLE tx_a3ekt_domain_model_vlogapriloga (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	dovoljenje int(11) unsigned DEFAULT '0' NOT NULL,
	cezmejnodovoljenje int(11) unsigned DEFAULT '0' NOT NULL,
	dovoljenjevtujini int(11) unsigned DEFAULT '0' NOT NULL,

	stevilka_vloge int(11) DEFAULT '0' NOT NULL,
	naziv varchar(255) DEFAULT '' NOT NULL,
	nacin_pridobitve int(11) DEFAULT '0' NOT NULL,
	url varchar(255) DEFAULT '' NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_stroskipostopka'
#
CREATE TABLE tx_a3ekt_domain_model_stroskipostopka (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	dovoljenje int(11) unsigned DEFAULT '0' NOT NULL,
	cezmejnodovoljenje int(11) unsigned DEFAULT '0' NOT NULL,
	dovoljenjevtujini int(11) unsigned DEFAULT '0' NOT NULL,

	naziv_stroska varchar(255) DEFAULT '' NOT NULL,
	znesek double(11,2) DEFAULT '0.00' NOT NULL,
	naziv_predpisa varchar(255) DEFAULT '' NOT NULL,
	drugo varchar(255) DEFAULT '' NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_korakipostopka'
#
CREATE TABLE tx_a3ekt_domain_model_korakipostopka (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	dovoljenje int(11) unsigned DEFAULT '0' NOT NULL,
	cezmejnodovoljenje int(11) unsigned DEFAULT '0' NOT NULL,
	dovoljenjevtujini int(11) unsigned DEFAULT '0' NOT NULL,

	naziv int(11) DEFAULT '0' NOT NULL,
	terminski_rok varchar(255) DEFAULT '' NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_drugipogoj'
#
CREATE TABLE tx_a3ekt_domain_model_drugipogoj (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	naziv varchar(255) DEFAULT '' NOT NULL,
	oznaka varchar(255) DEFAULT '' NOT NULL,
	opis text NOT NULL,
	dokazilo text NOT NULL,
	opomba text NOT NULL,
	zakonodajni_organ int(11) unsigned DEFAULT '0',
	zakonodajni_organ_kontakt int(11) unsigned DEFAULT '0',
	pravne_podlage varchar(255) DEFAULT '' NOT NULL,
	pristojni_organ int(11) unsigned DEFAULT '0',
	pristojni_organ_kontakt int(11) unsigned DEFAULT '0',
	sorodne_povezave int(11) unsigned DEFAULT '0' NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_registerdovoljenj'
#
CREATE TABLE tx_a3ekt_domain_model_registerdovoljenj (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	dovoljenje int(11) unsigned DEFAULT '0' NOT NULL,

	naziv varchar(255) DEFAULT '' NOT NULL,
	organ int(11) unsigned DEFAULT '0' NOT NULL,
	url varchar(255) DEFAULT '' NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_cezmejnodovoljenje'
#
CREATE TABLE tx_a3ekt_domain_model_cezmejnodovoljenje (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	naziv varchar(255) DEFAULT '' NOT NULL,
        opis text NOT NULL,
	oznaka varchar(255) DEFAULT '' NOT NULL,
	pravno_sredstvo int(11) DEFAULT '0' NOT NULL,
	molk_organa text NOT NULL,
	opomba text NOT NULL,
	pristojni_organ int(11) unsigned DEFAULT '0',
	pristojni_organ_kontakt int(11) unsigned DEFAULT '0',
	vloge int(11) unsigned DEFAULT '0' NOT NULL,
	priloge_k_vlogi int(11) unsigned DEFAULT '0' NOT NULL,
	stroski_postopka int(11) unsigned DEFAULT '0' NOT NULL,
	koraki_postopka int(11) unsigned DEFAULT '0' NOT NULL,
	pritozbeni_organ int(11) unsigned DEFAULT '0',
	pravna_podlaga varchar(255) DEFAULT '' NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_cezmejnoopravljenjepoklica'
#
CREATE TABLE tx_a3ekt_domain_model_cezmejnoopravljenjepoklica (
	
  uid int(11) NOT NULL auto_increment,
  pid int(11) NOT NULL DEFAULT '0',
	
  naziv varchar(255) NOT NULL DEFAULT '',
  oznaka varchar(255) DEFAULT '' NOT NULL,
  opis text NOT NULL,
  oznaka varchar(255) NOT NULL DEFAULT '',
  pravno_sredstvo int(11) NOT NULL DEFAULT '0',
  molk_organa text NOT NULL,
  opomba text NOT NULL,
  pristojni_organ int(11) unsigned DEFAULT '0',
  pristojni_organ_kontakt int(11) unsigned DEFAULT '0',
  vloge int(11) unsigned NOT NULL DEFAULT '0',
  priloge_k_vlogi int(11) unsigned NOT NULL DEFAULT '0',
  stroski_postopka int(11) unsigned NOT NULL DEFAULT '0',
  koraki_postopka int(11) unsigned NOT NULL DEFAULT '0',
  pritozbeni_organ int(11) unsigned DEFAULT '0',
  pravna_podlaga varchar(255) DEFAULT '',
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)
);

#
# Table structure for table 'tx_a3ekt_domain_model_povezavepogojev'
#
CREATE TABLE tx_a3ekt_domain_model_povezavepogojev (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	povezavepogojev int(11) unsigned DEFAULT '0' NOT NULL,
	ektpovezavepogoj int(11) unsigned DEFAULT '0' NOT NULL,

	tip int(11) DEFAULT '0' NOT NULL,
	poklic int(11) unsigned DEFAULT '0',
	dovoljenje int(11) unsigned DEFAULT '0',
	cezmejno_dovoljenje int(11) unsigned DEFAULT '0',
	drugi_pogoj int(11) unsigned DEFAULT '0',
	pogoj int(11) unsigned DEFAULT '0' NOT NULL,
	dejavnost int(11) unsigned DEFAULT '0',
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_ektpovezavepogoj'
#
CREATE TABLE tx_a3ekt_domain_model_ektpovezavepogoj (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	opomba text NOT NULL,
	pogoj int(11) unsigned DEFAULT '0' NOT NULL,
	dejavnost int(11) unsigned DEFAULT '0' NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_povezavepoklicev'
#
CREATE TABLE tx_a3ekt_domain_model_povezavepoklicev (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	povezavepoklicev int(11) unsigned DEFAULT '0' NOT NULL,
	ektpovezavepoklicev int(11) unsigned DEFAULT '0' NOT NULL,

	tip int(11) DEFAULT '0' NOT NULL,
	dovoljenje int(11) unsigned DEFAULT '0',
	cezmejno_dovoljenje int(11) unsigned DEFAULT '0',
	drugi_pogoj int(11) unsigned DEFAULT '0',
	pogoj int(11) unsigned DEFAULT '0' NOT NULL,
	dovoljenje_v_tujini int(11) unsigned DEFAULT '0',
	poklic int(11) unsigned DEFAULT '0',
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_ektpovezavepoklicev'
#
CREATE TABLE tx_a3ekt_domain_model_ektpovezavepoklicev (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	opomba text NOT NULL,
	poklic int(11) unsigned DEFAULT '0' NOT NULL,
	pogoj int(11) unsigned DEFAULT '0' NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_domain_model_dovoljenjevtujini'
#
CREATE TABLE tx_a3ekt_domain_model_dovoljenjevtujini (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	naziv_dovoljenja text NOT NULL,
	oznaka varchar(255) DEFAULT '' NOT NULL,
        opis text NOT NULL,
	nacin_priznavanja int(11) DEFAULT '0' NOT NULL,
	obvezno_clanstvo int(11) DEFAULT '0' NOT NULL,
	obnova int(11) DEFAULT '0' NOT NULL,
	termin_obnove varchar(255) DEFAULT '' NOT NULL,
	opis_obnove text NOT NULL,
	drugo text NOT NULL,
	pristojni_organ int(11) unsigned DEFAULT '0',
	pristojni_organ_kontakt int(11) unsigned DEFAULT '0',
	vloge int(11) unsigned DEFAULT '0' NOT NULL,
	priloge_k_vlogi int(11) unsigned DEFAULT '0' NOT NULL,
	stroski_postopka int(11) unsigned DEFAULT '0' NOT NULL,
	koraki_postopka int(11) unsigned DEFAULT '0' NOT NULL,
	pritozbeni_organ int(11) unsigned DEFAULT '0',
	pravne_podlage_tujina varchar(255) DEFAULT '' NOT NULL,
	pravno_sredstvo int(11) DEFAULT '0' NOT NULL,
	molk_organa text NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_a3ekt_ektdejavnost_skddejavnost_mm'
#
CREATE TABLE tx_a3ekt_ektdejavnost_skddejavnost_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_a3ekt_domain_model_pravnapodlaga'
#
CREATE TABLE tx_a3ekt_domain_model_pravnapodlaga (

	dejavnost  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_sorodnepovezave'
#
CREATE TABLE tx_a3ekt_domain_model_sorodnepovezave (

	dejavnost  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_vloga'
#
CREATE TABLE tx_a3ekt_domain_model_vloga (

	dovoljenje  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_vlogapriloga'
#
CREATE TABLE tx_a3ekt_domain_model_vlogapriloga (

	dovoljenje  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_stroskipostopka'
#
CREATE TABLE tx_a3ekt_domain_model_stroskipostopka (

	dovoljenje  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_korakipostopka'
#
CREATE TABLE tx_a3ekt_domain_model_korakipostopka (

	dovoljenje  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_pravnapodlaga'
#
CREATE TABLE tx_a3ekt_domain_model_pravnapodlaga (

	dovoljenje5  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_registerdovoljenj'
#
CREATE TABLE tx_a3ekt_domain_model_registerdovoljenj (

	dovoljenje  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_sorodnepovezave'
#
CREATE TABLE tx_a3ekt_domain_model_sorodnepovezave (

	poklic  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_pravnapodlaga'
#
CREATE TABLE tx_a3ekt_domain_model_pravnapodlaga (

	poklic  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_pravnapodlaga'
#
CREATE TABLE tx_a3ekt_domain_model_pravnapodlaga (

	drugipogoj  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_sorodnepovezave'
#
CREATE TABLE tx_a3ekt_domain_model_sorodnepovezave (

	drugipogoj  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_vloga'
#
CREATE TABLE tx_a3ekt_domain_model_vloga (

	cezmejnodovoljenje  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_vlogapriloga'
#
CREATE TABLE tx_a3ekt_domain_model_vlogapriloga (

	cezmejnodovoljenje  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_stroskipostopka'
#
CREATE TABLE tx_a3ekt_domain_model_stroskipostopka (

	cezmejnodovoljenje  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_korakipostopka'
#
CREATE TABLE tx_a3ekt_domain_model_korakipostopka (

	cezmejnodovoljenje  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_pravnapodlaga'
#
CREATE TABLE tx_a3ekt_domain_model_pravnapodlaga (

	cezmejnodovoljenje  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_povezavepogojev'
#
CREATE TABLE tx_a3ekt_domain_model_povezavepogojev (

	povezavepogojev  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_povezavepogojev'
#
CREATE TABLE tx_a3ekt_domain_model_povezavepogojev (

	ektpovezavepogoj  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_dejavnost'
#
CREATE TABLE tx_a3ekt_domain_model_dejavnost (

	ektpovezavepogoj  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_povezavepoklicev'
#
CREATE TABLE tx_a3ekt_domain_model_povezavepoklicev (

	povezavepoklicev  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_poklic'
#
CREATE TABLE tx_a3ekt_domain_model_poklic (

	ektpovezavepoklicev  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_povezavepoklicev'
#
CREATE TABLE tx_a3ekt_domain_model_povezavepoklicev (

	ektpovezavepoklicev  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_vloga'
#
CREATE TABLE tx_a3ekt_domain_model_vloga (

	dovoljenjevtujini  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_vlogapriloga'
#
CREATE TABLE tx_a3ekt_domain_model_vlogapriloga (

	dovoljenjevtujini  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_stroskipostopka'
#
CREATE TABLE tx_a3ekt_domain_model_stroskipostopka (

	dovoljenjevtujini  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_korakipostopka'
#
CREATE TABLE tx_a3ekt_domain_model_korakipostopka (

	dovoljenjevtujini  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_a3ekt_domain_model_pravnapodlaga'
#
CREATE TABLE tx_a3ekt_domain_model_pravnapodlaga (

	dovoljenjevtujini  int(11) unsigned DEFAULT '0' NOT NULL,

);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

CREATE TABLE tt_content (
    tx_a3ekt_sekundarne_vsebine_tip tinytext
);

#
# Table structure for table 'tx_a3ekt_domain_model_registerdovoljenj'
#
CREATE TABLE tx_a3ekt_domain_model_korak (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	naziv varchar(255) DEFAULT '' NOT NULL,
	tx_externalimporttut_externalid varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);